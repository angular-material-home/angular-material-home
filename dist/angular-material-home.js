/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome', [
	'mblowfish-core',
	'ui.tree'
]);

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/**
 * @ngdoc module
 * @name ngDonate
 * @description Defines icons to use every where.
 *
 */
angular.module('ngMaterialHome')
.config(function(wbIconServiceProvider) {
	wbIconServiceProvider
	// widget move icon
	.addShapes({
		'widget_move_first': '<path d="M 17.160156 5.9628906 L 11.160156 11.962891 L 17.160156 17.962891 L 18.570312 16.552734 L 13.990234 11.962891 L 18.570312 7.3730469 L 17.160156 5.9628906 z M 12 6 L 6 12 L 12 18 L 13.410156 16.589844 L 8.8300781 12 L 13.410156 7.4101562 L 12 6 z " />',
		'widget_move_left': '<path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/>',
		'widget_move_right': '<path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"/>',
		'widget_move_last': '<path d="m 7.410156,5.9628906 6,6.0000004 -6,6 L 6,16.552734 10.580078,11.962891 6,7.3730469 Z M 12.570312,6 l 6,6 -6,6 L 11.160156,16.589844 15.740234,12 11.160156,7.4101562 Z" />',
		
		'widget_move_top': '<path d="m 18.303711,16.856445 -6.000001,-6 -5.9999997,6 1.410157,1.410156 4.5898427,-4.580078 4.589844,4.580078 z m -0.03711,-5.160156 -6,-5.9999997 -5.9999997,5.9999997 1.410156,1.410156 4.5898437,-4.5800777 4.589844,4.5800777 z" />',
		'widget_move_up': '<path d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z"/>',
		'widget_move_down': '<path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"/>',
		'widget_move_button': '<path d="M 18.303711,7.1064453 12.30371,13.106445 6.3037103,7.1064453 7.7138673,5.6962893 12.30371,10.276367 16.893554,5.6962893 Z m -0.03711,5.1601557 -6,6 -5.9999997,-6 1.410156,-1.410156 4.5898437,4.580078 4.589844,-4.580078 z" />',
	});
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
	/*
	 * State machine of the system
	 * 
	 */
	.config(function ($routeProvider) {
		var defaultSidenavs = ['amh.cms.pages.sidenav',
			'amh.workbench.weburger.widgets',
			'amh.workbench.weburger.settings',
			'amh.workbench.weburger.templates',
			'amh.workbench.weburger.navigator',
			'amh.workbench.content',
			// 'amh.workbench.contentMetadata',
			// 'amh.workbench.termTaxonomies'
		];

		$routeProvider //
			.otherwise({
				redirectTo: '/'
			})
			/**
			 * @ngdoc ngRoute
			 * @name /
			 * @description Main page of the site
			 */
			.when('/', {
				templateUrl: 'views/amh-content-editor.html',
				helpId: 'amh-content',
				groups: ['workbench'],
				sidenavs: defaultSidenavs,
			})
			/**
			 * @ngdoc ngRoute
			 * @name /home/:language
			 * @description Main page of the site
			 */
			.when('/home/:language', {
				templateUrl: 'views/amh-content-editor.html',
				helpId: 'amh-content',
				groups: ['workbench'],
				sidenavs: defaultSidenavs,
			})
			/**
			 * @ngdoc ngRoute
			 * @name /home/:language
			 * @description Main page of the site
			 */
			.when('/home', {
				templateUrl: 'views/amh-content-editor.html',
				helpId: 'amh-content',
				groups: ['workbench'],
				sidenavs: defaultSidenavs,
			})

			/**
			 * @ngdoc ngRoute
			 * @name /content/:name
			 * @description Display a content value
			 */
			.when('/content/:name', {
				templateUrl: 'views/amh-content-editor.html',
				helpId: 'amh-content',
				groups: ['workbench'],
				sidenavs: defaultSidenavs,
			});
	});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('ngMaterialHome')

/**
 * @ngdoc Controllers
 * @name AmhContentEditorCtrl
 * @description Controller of a content
 * 
 * Shows a content and prepares all tools to allow users to manage the content. ##
 * Language
 * 
 * NOTE: Language is just supported in home page.
 * 
 * If the content is in the main page, language may be used to to load content.
 * Here is the list of language params: - $routeParams.language -
 * setting.language - config.language
 * 
 * @property {Object} $scope Shared data with view
 * @property {Object} $scope.error current error from controller
 * @property {boolean} $scope.working is true if controller is doing some
 *           process
 * @property {string} getModelMeta meta data of the content
 * @property {string} $scope.model data model and page design if is WB page.
 */
.controller('AmhContentEditorCtrl', function (
		/* AMH      */ WbObservableObject, $amhEditorService,
		/* angular  */ $scope, $element, $rootScope,
		/* wb       */ $widget
) {
	

	/**
	 * Sets root widget
	 * 
	 * @param rootWidget {Widget} to set as root
	 */
	this.setRootWidget = function (rootWidget) {
		this.rootWidget = rootWidget;
		this.fire('rootWidgetChanged', {
			value: this.rootWidget
		})
	};

	/**
	 * Gets root widget
	 * 
	 * @return the root widget of the editor
	 */
	this.getRootWidget = function () {
		return this.rootWidget;
	};

	/**
	 * Sets selected widgets
	 * 
	 * @param widgets {Array} of widgets
	 */
	this.setSelectedWidgets = function (widgets) {
		var oldValue = this.SelectedWidgets;
		this.selectedWidgets = widgets || [];
		$rootScope.workbenchSelectedWidgets = widgets;
		if($scope.workbench){
			$scope.workbench.fire('selecteWidgetsChanged', {
				oldValue: oldValue,
				value: this.selectedWidgets
			});
		}
	};

	/**
	 * Gets selected widgets
	 * 
	 * @return selected widgets
	 */
	this.getSelectedWidgets = function () {
		return this.selectedWidgets || [];
	};

	this.getState = function () {
		return this.state;
	};

	this.setState = function (state) {
		var oldValue = this.state;
		this.state = state;
		this.fire('stateChanged', {
			oldValue: oldValue,
			value: state
		});
	};

	this.isEditable = function(){
		return this.state === 'edit';
	};

	this.getElement = function(){
		return $element;
	};

	this.getScope = function(){
		return $scope;
	};

	this.init = function(){
		// load observable
		angular.extend(this, WbObservableObject.prototype);
		WbObservableObject.apply(this);
		this.setState('ready');
		this.readOnly = true;
		$amhEditorService.connectEditor(this);
		// add edtor to 
		var ctrl = this;
		$scope.$watch('workbench', function(workbench){
			if(workbench){
				workbench.addEditor(ctrl);
			}
		});
	};

	this.init();
});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('ngMaterialHome')

/**
 * @ngdoc Controllers
 * @name AmhContentWorkbenchCtrl
 * @description Manage environment of editors
 * 
 * Workbench manages a list of jobs.
 * 
 * Following information are load in the workbench:
 *  - Content 
 *  - Content Meta 
 *  - Content term-taxonomis 
 *  - Content value
 * 
 */
.controller('AmhContentWorkbenchCtrl', function (
		/* amh */ WbObservableObject, $amhEditorService, AmhWorkbenchContentType,
		/* material */ $mdSidenav, $mdMedia, 
		/* weburger */ $wbUtil,
		/* angular */ $q, $window, $routeParams, $rootScope,
		/* mblowfish */ $actions, $app,
		/* cms */ CmsContent, CmsContentMetadata, $cms,
		/* controller */ $scope, $element
) {
	

	/*
	 * States are:
	 *  
	 * - start: the controller start its work 
	 * - ready: controller is in read only mode 
	 * - edit: controller load editor environment
	 */
	this.state = 'start';

	/*
	 * If the editor is loaded for then this true.
	 */
	this.editorLoaded = false;

	/*
	 * Store list of editors
	 * 
	 * In the current version there is just a single content editor.
	 */
	this.editors = [];

	/*
	 * Put media service into the scope
	 */
	$scope.$mdMedia = $mdMedia;


	/***************************************************************************
	 * Editors * *
	 **************************************************************************/
	this.addEditor = function(editor){
		// put job
		var editors = _.concat(this.editors, editor);
		this.setEditors(editors);
	};
	
	this.getEditors = function(){
		return this.editors;
	};
	
	this.getActiveEditor = function(){
		return this.activeEditor;
	};
	
	this.setActiveEditor = function(editor){
		var oldEditor = this.activeEditor;
		this.activeEditor = editor;
		this.fire('activeEditorChanged', {
			values: this.activeEditor,
			oldValues: oldEditor
		});
	};

	this.setEditors = function(editors){
		var oldEditors = this.editors;
		this.editors = editors;
		this.fire('editorsChanged', {
			values: this.editors,
			oldValues: oldEditors
		});
		if(!this.activeEditor) {
			this.setActiveEditor(editors[0]);
		}
	};

	/***************************************************************************
	 * jobs * *
	 **************************************************************************/
	this.addJob = function(job){
		// listen to job
		var ctrl = this;
		job.finally(function(){
			ctrl.removeJob(job);
		});
		// put job
		var jobs = _.concat(this.jobs, job);
		this.setJobs(jobs);
	};

	/**
	 * 
	 * NOTE: do not use directly
	 */
	this.setJobs = function(jobs){
		var oldJobs = this.jobs;
		this.jobs = jobs;
		this.fire('jobsChanged', {
			values: this.jobs,
			oldValues: oldJobs
		});
	};

	this.getJobs = function(){
		return this.jobs;
	};

	this.removeJob = function(job){
		// TODO: maso, 2019: use set job to change list
		var index = this.jobs.indexOf(job);
		if(index >= 0){
			this.jobs.splice(index, 1);
		}
	};

	/***************************************************************************
	 * environment * *
	 **************************************************************************/
	this.getElement = function(){
		return $element;
	};

	this.getScope = function(){
		return $scope;
	}

	/***************************************************************************
	 * State * *
	 **************************************************************************/
	this.setState = function(state){
		if(this.state === state){
			return;
		}
		var event = {
				source: this,
				value: state,
				oldValue: this.state
		};
		this.state = state;
		if(state === 'edit'){
			this.setEditorLoaded(true);
		}
		this.fire('stateChanged', event);
	};

	this.getState = function(){
		return this.state;
	};

	this.isEditorLoaded = function(){
		return this.editorLoaded;
	};

	this.setEditorLoaded = function(editorLoaded){
		$rootScope.workbenchEditorLoaded = editorLoaded;
		this.editorLoaded = editorLoaded;
	};

	this.toggleEditMode = function(){
		var newState = this.getState() === 'edit' ? 'ready' : 'edit';
		this.setState(newState);
		_.forEach(this.editors, function(editor){
			editor.setState(newState);
		});
		// support old sidenave model
		$rootScope.showWorkbenchSettingPanel = this.getState() === 'edit';
	};


	/***************************************************************************
	 * Sidenavs *
	 **************************************************************************/

	/**
	 * Check if sidenave is open
	 * 
	 * @memberof AmhContentCtrl
	 * @params {string} id of the sidenav
	 */
	this.isSidenavOpen = function (id) {
		return $mdSidenav(id).isOpen();
	};


	/**
	 * Toggles the sidenav with the given id
	 * 
	 * @memberof AmhContentCtrl
	 * @params {string} componentId of the sidenav
	 */
	this.toggleSidenav = function (componentId) {
		if (angular.isDefined(this.oldComponentId) && $mdSidenav(this.oldComponentId).isOpen()) {
			$mdSidenav(this.oldComponentId).toggle();
			if (this.oldComponentId == componentId) {
				return;
			}
		}
		this.oldComponentId = componentId;
		$mdSidenav(componentId).toggle();
	};

	// Open general help in right sidenav.
	this.openHelp = function () {
		// TODO: maso, 2018: replace with help service
		$rootScope.showHelp = !$rootScope.showHelp;
	};

	this.clean = function(){
		this.setContent(undefined);
		this.setContentValue(undefined);
		this.setContentMetadata(undefined);
		this.setTermTaxonomies(undefined);
	};

	/***************************************************************************
	 * Content *
	 **************************************************************************/
	/**
	 * Return the model id
	 * 
	 * The model ID will be fetch from the model.
	 */
	this.getContentId = function () {
		var meta = this.getContent();
		if (!angular.isDefined(meta)) {
			return;
		}
		return meta.id;
	};

	this.setContentType = function (contentType) {
		this.contentType = contentType;
	};

	this.getContentType = function () {
		return this.contentType;
	};

	this.setContent = function(content){
		// set content
		if(this.content === content) {
			return;
		}
		var oldContent = this.content;
		this.content = content;

		// find content type
		var contentType = 'weburger';
		_.forEach(this.supportedTypes, function(type){
			if(type.match(content.mime_type)){
				contentType = type.key;
			}
		})
		this.setContentType(contentType);

		// fire change
		this.fire('contentChanged', {
			value: content,
			oldValue: oldContent
		});
	};

	this.getContent = function(){
		return this.content;
	};

	this.isContentDeletable = function () {
		var permissions = $app.getProperty('account.permissions');
		return (permissions.tenant_owner || permissions.cms_author || permissions.cma_editor);
	};

	this.isContentEditable = function () {
		var permissions = $app.getProperty('account.permissions');
		var accountId = $app.getProperty('account.id');
		var content = this.getContent();
		if(!content){
			return false;
		}
		// TODO: maso, 2019: check if the content is wb
		return permissions.tenant_owner || permissions.cms_editor ||
		(permissions.cms_author && (angular.equals(Number(content.author_id), Number(accountId)))) ||
		false;
	};

	this.canCreateContent = function(){
		var permissions = $app.getProperty('account.permissions');
		return permissions.tenant_owner || permissions.cms_author || permissions.cma_editor;
	};

	/***************************************************************************
	 * Content Value *
	 **************************************************************************/
	this.setContentValue = function (contentValue) {
		if (!contentValue) {
			this.contentValue = contentValue;
			return;
		}
		if(!angular.isObject(contentValue)){
			throw {
				message: 'content must be an object'
			};
		}
		var oldContentValue = this.contentValue;
		this.contentValue = $wbUtil.clean(contentValue);
		this.fire('contentValueChanged', {
			values: contentValue,
			oldValue: oldContentValue
		});
	};

	this.getContentValue = function () {
		return this.contentValue;
	};

	this.isContentValueLoaded = function(){ 
		return this.getContentValue();
	};

	/***************************************************************************
	 * Content Metadata *
	 **************************************************************************/
	this.setContentMetadata = function (list) {
		var oldList = this.contentMetadata;
		this.contentMetadata = list;
		this.fire('contentMetadataChanged', {
			values: list,
			oldValue: oldList
		});
	};

	this.getContentMetadata = function () {
		return this.contentMetadata;
	};



	/***************************************************************************
	 * term taxonomies *
	 **************************************************************************/
	this.setTermTaxonomies = function (termTaxonomies) {
		var oldList = this.termTaxonomies;
		this.termTaxonomies = termTaxonomies;
		this.fire('termTaxonomiesChanged', {
			values: termTaxonomies,
			oldValue: oldList
		});
	};

	this.getTermTaxonomies = function () {
		return this.termTaxonomies;
	};

	/***************************************************************************
	 * modules
	 **************************************************************************/
	this.setContentModules = function (contentModules) {
		var oldList = this.contentModules;
		this.contentModules = contentModules;
		this.fire('contentModulesChanged', {
			values: contentModules,
			oldValue: oldList
		});
	};

	this.getContentModules = function () {
		return this.contentModules;
	};

	/***************************************************************************
	 * Basic *
	 **************************************************************************/
	/**
	 * Load and initializ the workbench
	 */
	this.init = function(){
		// load observable
		angular.extend(this, WbObservableObject.prototype);
		WbObservableObject.apply(this);

		this.jobs = [];	
		this.supportedTypes = [];
		this.editors = [];

		var ctrl = this;
		$scope.$on('$destroy', function () {
			$amhEditorService.disconnectWorkbench(ctrl);
		});
		$amhEditorService.connectWorkbench(ctrl);

		this.supportedTypes.push(new AmhWorkbenchContentType('weburger', 'application/weburger+json'));

		this.setState('ready');
	};

	this.init();
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


//Test controller
angular.module('ngMaterialHome')
.controller('AmhContentMyPagesCtrl', function ($scope, $rootScope, $controller, $cms, $dispatcher, $q) {
	// Extends Items controller
	angular.extend(this, $controller('MbSeenCmsContentsCtrl',{
		$scope: $scope
	}));


	// get accounts
	this.getModels = function(queryParams) {
		// Check if user is login
		if($rootScope.__account.anonymous){
			return $q.resolve({
				items:[]
			});
		}

		// Getting user pages
		queryParams.setFilter('media_type', 'page');
		queryParams.setFilter('author_id', $rootScope.__account.id);
		return $cms.getContents(queryParams)
		.then(function(list){
			for(var i = 0; i < list.items.length; i++){
				var item  = list.items[i];
				var map = {};
				var metas = item.metas || [];
				for(var j = 0; j < metas.length; j++){
					var key = metas[j].key.replace('.', '_');
					map[key] = metas[j].value;
				}
				item.metasMap = map;
			}
			return list;
		});
	};

	/*
	 * Add store listener
	 * 
	 * This is an internal function, is overried to handle my-page condition
	 */
	this.eventHandlerCallBack = function(){
		if(this._eventHandlerCallBack){
			return this._eventHandlerCallBack ;
		}
		var ctrl = this;
		this._eventHandlerCallBack = function($event){
			var contents = $event.values || [];
			contents = _.filter(contents, {'author_id': Number($rootScope.__account.id)});
			if(!contents){
				return;
			}
			switch ($event.key) {
			case 'created':
				ctrl.pushViewItems(contents);
				break;
			case 'updated':
				ctrl.updateViewItems(contents);
				break;
			case 'removed':
				ctrl.removeViewItems(contents);
				break;
			default:
				break;
			}
		};
		return this._eventHandlerCallBack;
	};

	this.init
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


//Test controller
angular.module('ngMaterialHome')
.controller('AmhContentTemplatesCtrl', function ($scope, $controller, $cms, $http, $navigator, $wbUtil) {

    /*
     * Extends collection controller
     */
    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope : $scope
    }));

    // Override the schema function
    this.getModelSchema = function () {
        return $cms.contentSchema();
    };

    // get contents
    this.getModels = function (parameterQuery) {
    	parameterQuery.setFilter('taxonomy', 'template');
    	parameterQuery.put('graphql', '{current_page,page_number,items{term{id,name},contents{id, name, title, description, media_type}}}');
        return $cms.getTermTaxonomies(parameterQuery);
    };

    // get a content
    this.getModel = function (id) {
        return $cms.getContent(id);
    };
    

	/**
	 * Shows a preview of a template
	 * 
	 * @memberof AmhMainPageTmplCtrl
	 * @param {type} template
	 * @return {promiss} to load preview
	 */
	this.showPreviewOf = function(page){
		// load content
		if ($scope.loadingTemplatePreview) {
			return $scope.loadingTemplatePreview;
		}
		var responseData;
		var ctrl = this;
		$scope.loadingTemplatePreview = $http.get('/api/v2/cms/contents/'+page.id+'/content') //
		.then(function(response) {
			responseData = $wbUtil.clean(response.data);
			return $navigator.openDialog({
				templateUrl: 'views/dialogs/amh-template-preview.html',
				config: {
					model: responseData,
					page: page
				}
			});
		})//
		.then(function(){
			if(angular.isFunction(ctrl.applyTemplate)){
				ctrl.applyTemplate(responseData);
			}
		})
		.finally(function(){
			delete $scope.loadingTemplatePreview;
		});
		return $scope.loadingTemplatePreview;
	};
	
    this.init({
    	eventType: '/cms/contents'
    });
    

    /*
     * Add store listener
     * 
     * This is an internal function, is overried to handle my-page condition
     */
    this.eventHandlerCallBack = function(){
        if(this._eventHandlerCallBack){
            return this._eventHandlerCallBack ;
        }
        var ctrl = this;
        this._eventHandlerCallBack = function($event){
        	// check values are a template. Then update the list
        };
        return this._eventHandlerCallBack;
    };
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

//Test controller
angular.module('ngMaterialHome')
.controller('AmhContentPagesCtrl', function ($scope, $controller, $cms, $dispatcher) {
	
	// Extends Items controller
	angular.extend(this, $controller('MbSeenCmsContentsCtrl',{
		$scope: $scope
	}));

	// get accounts
	this.getModels = function(queryParams) {
		queryParams.setFilter('media_type', 'page');
		return $cms.getContents(queryParams)
		.then(function(list){
			for(var i = 0; i < list.items.length; i++){
				var item  = list.items[i];
			    var map = {};
			    var metas = item.metas || [];
			    for(var j = 0; j < metas.length; j++){
			        var key = metas[j].key.replace('.', '_');
			        map[key] = metas[j].value;
			    }
			    item.metasMap = map;
			}
			return list;
		});
	};

	this.title = 'Pages';
	this.init({
		eventType: '/cms/contents'
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
/**
 * @ngdoc controllers
 * @name AmhOwnerToolbarCtrl
 * @description Controller for toolbar which is shown for owners only.
 * 
 * This toolbar uses following groups of actions
 * 
 * - amh.owner-toolbar.public
 * - amh.owner-toolbar.location
 * - amh.owner-toolbar.scope
 * - mb.user
 */
.controller("AmhOwnerToolbarCtrl", function ($scope, $actions, $app, $mdSidenav, $monitor) {
	$scope.userMenu = $actions.group('mb.user');

	this.logout = function () {
		$app.logout();
	};


	this.toggleMessageSidenav = function(){
		$mdSidenav('messages').toggle();
	};

	this.getMessageCount = function() {
		var ctrl = this;
		return $monitor.getMetric('message.count')
		.then(function (metric) {
			ctrl.messageCount = metric.value;
		});
	};

	this.showLocalSettings = function(){
		return $mdSidenav('settings').toggle();
	};

	this.getMessageCount();
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


//Test controller
angular.module('ngMaterialHome')

/**
 * @ngdoc Controllers
 * @name AmhPageNewDialogCtrl
 * @description Manage the dialog used to create new page.
 * 
 * This controller is used to manage an steper to create a page.
 */
.controller('AmhPageNewDialogCtrl', function ($scope, $controller, $mdStepper, $cms, $q, $wbUtil,
		$dispatcher, $navigator, $http, $translate, uuid4, config, QueryParameter, $resource, $window, $mdDialog) {
	// Extends Items controller
	angular.extend(this, $controller('AmdNavigatorDialogCtrl', {
		$scope: $scope,
		config: config
	}));
	/*
	 * ID of the stepper
	 */
	var parameterQuery = new QueryParameter;
	var _stepper_id = 'page-stepper';
	this.steps = [];

	//Content base structure
	this.contentModel = {
			info: {
				media_type: 'page',
				mime_type: 'application/weburger+json',
			},
			template: {},
			termTaxonomies: [],
			metas: []
	};

	/**
	 * List of supported page types
	 * 
	 * @memberof AmhPageNewDialogCtrl
	 */
	this.supportedPageTypes = [{
		mime_type: 'application/weburger+json',
		title: 'Weburger',
		enable: true,
		icon: 'resources/images/page-type-wb.svg',
	}, {
		mime_type: 'text/html',
		title: 'HTML',
		enable: false,
		icon: 'resources/images/page-type-html.svg',
	}, {
		mime_type: 'image/svg+xml',
		title: 'SVG',
		enable: false,
		icon: 'resources/images/page-type-svg.svg',
	}, {
		mime_type: 'text/markdown',
		title: 'Markdown',
		enable: false,
		icon: 'resources/images/page-type-md.svg',
	}];

	/**
	 * Sets page type and go to the next step
	 * 
	 * @memberof AmhPageNewDialogCtrl
	 */
	this.setPageType = function (type) {
		this.contentModel.info.mime_type = type.mime_type;
		this.nextStep();
	};

	/*
	 * 
	 * @param {type} key
	 * @param {type} href
	 * @returns {String}
	 * @description get the name of page and the current route of browser (full url) and 
	 * constract a relative canonical link
	 */
	this.fetchRoute = function (key, href) {
		var baseUrl;
		var url = new URL(href);
		if (url.port) {
			baseUrl = url.protocol + '//' + url.hostname + ':' + url.port + '/';
		} else {
			baseUrl = url.protocol + '//' + url.hostname + '/';
		}

		// href without baseUrl
		var remainedUrl = url.href.replace(baseUrl, '');
		//if baseUrl contain 'localhost' 
		if (baseUrl.indexOf('localhost') > -1 || remainedUrl.length === 0) {
			return '/content/' + key;
		} else {
			return '/' + remainedUrl.split('/')[0] + '/content/' + key;
		}
	};

	/**
	 * Updates a info field
	 * 
	 * @memberof AmhPageNewDialogCtrl
	 */
	this.setPageInfo = function (key, value) {
		// update info
		this.contentModel.info[key] = value;

		// update meta
		var metaKey;
		switch (key) {
		case 'name':
			metaKey = 'link.canonical';
			var href = $window.location.href;
			value = this.fetchRoute(value, href);
			break;
		case 'title':
			metaKey = 'title';
			break;
		case 'description':
			metaKey = 'meta.description';
			break;
		case 'cover':
			metaKey = 'link.cover';
			break;
		default:
			metaKey = key;
		}
		this.setPageMeta(metaKey, value);
	};

	/**
	 * Sets meta of the page
	 * 
	 * @memberof AmhPageNewDialogCtrl
	 */
	this.setPageMeta = function (key, value) {
		var meta = this.getPageMeta(key);
		if (!meta) {
			meta = {
					key: key
			};
			this.contentModel.metas.push(meta);
		}
		meta.value = value;
	};

	/**
	 * FIndes meta of the page
	 * 
	 * @memberof AmhPageNewDialogCtrl
	 */
	this.getPageMeta = function (key) {
		var metas = this.contentModel.metas;
		for (var i = 0; i < metas.length; i++) {
			var item = metas[i];
			if (item.key === key) {
				return item;
			}
		}
	};

	/**
	 * Sets cover of the page from resources
	 * 
	 * @memberof AmhPageNewDialogCtrl
	 */
	this.selectCoverFromResources = function () {
		var ctrl = this;
		return $resource.get('image', {
			config: {
				title: 'Page cover'
			},
			name: 'Page cover'
		}).then(function (cover) {
			ctrl.setPageInfo('cover', cover);
		});
	};

	/**
	 * Sets page name with random value
	 * 
	 * @memberof AmhPageNewDialogCtrl
	 */
	this.generateRandomName = function () {
		this.setPageInfo('name', uuid4.generate());
	};

	/**
	 * Sets template 
	 */
	this.setTemplate = function (template) {
		this.contentModel.template = template || {};
		this.nextStep();
	};

	/**
	 * Shows a preview of a template
	 * 
	 * @memberof AmhMainPageTmplCtrl
	 * @param {type} template
	 * @return {promiss} to load preview
	 */
	this.showPreviewOf = function (template) {
		// load content
		if (this.loadingTemplatePreview) {
			return;
		}
		this.loadingTemplatePreview = true;
		var ctrl = this;
		return $http.get('/api/v2/cms/contents/' + template.id + '/content') //
		.then(function (response) {
			var responseData = $wbUtil.clean(response.data);
			return $navigator.openDialog({
				templateUrl: 'views/dialogs/amh-template-preview.html',
				config: {
					model: responseData,
					page: template
				}
			});
		})//
		.finally(function () {
			ctrl.loadingTemplatePreview = false;
		});
	};

	/**
	 * Loads settings with the index
	 * 
	 * @memberof MbInitialCtrl
	 * @param {integer}
	 *            index of the setting
	 */
	this.goToStep = function (index) {
		$mdStepper(_stepper_id)//
		.goto(index);
	};

	/**
	 * Loads the next setting page
	 * 
	 * @memberof MbInitialCtrl
	 */
	this.nextStep = function () {
		$mdStepper(_stepper_id)//
		.next();
	};

	/**
	 * Loads the previous setting page
	 * 
	 * @memberof MbInitialCtrl
	 */
	this.prevStep = function () {
		$mdStepper(_stepper_id)//
		.back();
	};

	/**
	 * Create the model
	 * 
	 * The editor can create a page if you add createModel function.
	 * By default, the function dose not implemented.
	 */
	this.createModel = function (newContentModel) {
		var ctrl = this;
		var contentOrg;
		if (this.creatingContent) {
			return;
		}
		this.creatingContent = true;
		return $cms.putContent(newContentModel.info)
		.then(function (content) {
			contentOrg = content;
			var list = [];
			//load template to content
			list.push($http.get('/api/v2/cms/contents/' + newContentModel.template.id + '/content') //
					.then(function (response) {
						content.uploadValue(response.data);
					}));
			// metas
			angular.forEach(newContentModel.metas, function (meta) {
				list.push(content.putMetadatum(meta));
			});
			return $q.all(list);
		})//
		.finally(function () {
			$dispatcher.dispatch('/cms/contents', {
				key: 'created',
				values: [contentOrg]
			});
			ctrl.creatingContent = false;
			ctrl.contentCreated = true;

			for (var i = 0; i < newContentModel.metas.length; i++) {
				if (newContentModel.metas[i].key === 'link.canonical') {
					ctrl.canonicalLink = newContentModel.metas[i].value;
					break;
				}
			}
		});
	};

	this.goTo = function () {
		$mdDialog.hide();
	};

	/*
	 * Load templates for view
	 */
	this._getTemplates = function () {
		parameterQuery.setFilter('taxonomy', 'template');
		parameterQuery.put('graphql', '{current_page,page_number,items{term{id,name},contents{id, name, title, description, media_type}}}');
		var ctrl = this;
		$cms.getTermTaxonomies(parameterQuery)
		.then(function (response) {
			ctrl.items = response.items;
		}, function () {
			alert($translate.instant('Fail to load template'));
		});
	};

	// add watch on setting stepper current step.
	$scope.$watch(function () {
		var current = $mdStepper(_stepper_id);
		if (current) {
			return current.currentStep;
		}
		return -1;
	}, function (index) {
		if (index >= 0 && $scope.steps && $scope.steps.length) {
			$scope.currentStep = $scope.steps[index];
		}
	});

	// TODO: mao, 2019: check a content with the same name exist
	this._getTemplates();
	this.setPageInfo('name', config.name || uuid4.generate());
//	this.loadCategories();
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */




//Test controller
angular.module('ngMaterialHome')
.controller('AmhSeenSelectPagesCtrl', function ($scope, $controller, $cms) {
	// Extends Items controller
	$scope.setValue = function (value) {
		$scope.value = value;
		$scope.$parent.setValue('content/' + value.id);
		$scope.$parent.answer();
	};
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('ngMaterialHome')

/**
 * @ngdoc controller
 * @name AmhMainPageTmplCtrl
 * @description Main page template setting controller
 * 
 * This controller is used to control setting main page template
 * 
 * Here is list of flags in scope:
 * 
 * <ul>
 * 	<li>loadingTemplatePreview: Show if control is about to load template preview</li>
 * 	<li>savingContent: True if control is about save content</li>
 * </ul>
 */
.controller('AmhUserWidgetCtrl', function($scope, $http) {
	
	// Load from resources
	function load(){
		$http.get('resources/widget-templates.json')
		.then(function(result){
			$scope.categories = result.data;
		});
	}
	
	function loadWidgets(selectedCategory){
		if(!selectedCategory.type || selectedCategory.type == 'embed'){
			$scope.widgets = selectedCategory.templates;
		}
		if(selectedCategory.type == 'url'){
			$http.get(selectedCategory.url)
			.then(function(result){
				$scope.widgets = result.data;
			});
		}
	}
	
	
	this.loadWidgets = loadWidgets;
	load();
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome').controller('AmhContentWorkbenchMetasCtrl', function (
    /* angularjs */ $scope, $window,
    /* angualr-material-home */ $amhEditorService, $actions) {

    var ctrl = this;
    var CONTENT_METADATA_CHANGE_EVENT = 'contentMetadataChanged';
    var WORKBENCH_CHANGE_EVENT = 'workbenchChanged';

    function contentMetadataChanged() {
        if (ctrl.workbench) {
            ctrl.contentMetadata = ctrl.workbench.getContentMetadata();
        }
    }

    function setWorkbench(workbenc) {
        if (ctrl.workbench) {
            ctrl.workbench.off(CONTENT_METADATA_CHANGE_EVENT, contentMetadataChanged);
        }
        ctrl.workbench = workbenc;
        if (ctrl.workbench) {
            ctrl.workbench.on(CONTENT_METADATA_CHANGE_EVENT, contentMetadataChanged);
        }
        contentMetadataChanged();
    }

    function handleWorkbench(event) {
        setWorkbench(event.value);
    }

    this.deleteMetadatum = function (meta, $event) {
        $window.confirm('Delete metadata?')
            .then(function () {
                $event.metadata = [meta];
                return $actions.exec('amh.workbench.content.metadata.delete', $event);
            });
    };

    this.editMetadatum = function (meta, $event) {
        $event.metadata = [meta];
        return $actions.exec('amh.workbench.content.metadata.update', $event);
    };

    this.addMetadatum = function ($event) {
        return $actions.exec('amh.workbench.content.metadata.create', $event);
    };

    setWorkbench($amhEditorService.getWorkbench());
    $amhEditorService.on(WORKBENCH_CHANGE_EVENT, handleWorkbench);
    $scope.$on('destory', function () {
        $amhEditorService.off(WORKBENCH_CHANGE_EVENT, handleWorkbench);
        if (ctrl.workbench) {
            ctrl.workbench.on(CONTENT_METADATA_CHANGE_EVENT, contentMetadataChanged);
        }
    });

});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome').controller('AmhContentWorkbenchTermTaxonomiesCtrl', function (
    /* angularjs             */ $scope,
    /* angualr-material-home */ $amhEditorService, $actions) {
	/*
	 * Allow user to change current content settings such as title, description
	 * and ..
	 */
    var ctrl = this;
    this.termTaxonomiesMap = {};

    function termTaxonomiesChanged() {
        ctrl.termTaxonomies = ctrl.workbench ? ctrl.workbench.getTermTaxonomies() : [];
        ctrl.termTaxonomiesMap = {};
        _.forEach(ctrl.termTaxonomies, function (termTaxonomies) {
            var list = ctrl.termTaxonomiesMap[termTaxonomies.taxonomy];
            if (!_.isArray(list)) {
                list = [];
                ctrl.termTaxonomiesMap[termTaxonomies.taxonomy] = list;
            }
            list.push(termTaxonomies);
        });
    }

    function setWorkbench(workbenc) {
        if (ctrl.workbench) {
            ctrl.workbench.off('termTaxonomiesChanged', termTaxonomiesChanged);
        }
        ctrl.workbench = workbenc;
        if (ctrl.workbench) {
            ctrl.workbench.on('termTaxonomiesChanged', termTaxonomiesChanged);
        }
        termTaxonomiesChanged();
    }

    function handleWorkbench(event) {
        setWorkbench(event.value);
    }

    this.addTermTaxonomies = function ($event) {
        $actions.exec('amh.workbench.content.termTaxonomies.create', $event);
    };

    this.deleteTermTaxonomy = function (item, $event) {
        $event.items = [item];
        $actions.exec('amh.workbench.content.termTaxonomies.delete', $event);
    };


    setWorkbench($amhEditorService.getWorkbench());
    $amhEditorService.on('workbenchChanged', handleWorkbench);
    $scope.$on('destory', function () {
        $amhEditorService.off('workbenchChanged', handleWorkbench);
        if (ctrl.workbench) {
            ctrl.workbench.on('contentChanged', contentChanged);
        }
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
.controller('AmhContentWorkbenchModuleCtrl', function($scope, $amhEditorService, $actions, $window){
	
	var ctrl = this;
	var COTNTENT_CHANGE_EVENT = 'contentChanged';
	var WORKBENCH_CHANGE_EVENT = 'workbenchChanged';

	function modulesChanged(){
		ctrl.modules = ctrl.workbench.getContentModules();
	}

	function setWorkbench(workbenc){
		if(ctrl.workbench){
			ctrl.workbench.off(COTNTENT_CHANGE_EVENT, modulesChanged);
		}
		ctrl.workbench = workbenc;
		if(ctrl.workbench){
			ctrl.workbench.on(COTNTENT_CHANGE_EVENT, modulesChanged);
		}
		modulesChanged();
	}

	function handleWorkbench(event){
		setWorkbench(event.value);
	}

	this.addModule = function($event){
		$actions.exec('amh.workbench.content.module.add', $event);
	};

	this.deleteModule = function(module, $event){
		$window.confirm('Delete modeule?')
		.then(function(){
			$event.modules = [module];
			$actions.exec('amh.workbench.content.module.remove', $event);
		});
	};

	this.editModule = function(module, $event){
		$event.modules = [module];
		$actions.exec('amh.workbench.content.module.edit', $event);
	};

	/*
	 * connect to environment
	 */
	setWorkbench($amhEditorService.getWorkbench());
	$amhEditorService.on(WORKBENCH_CHANGE_EVENT, handleWorkbench);
	$scope.$on('destory', function(){
		$amhEditorService.off(WORKBENCH_CHANGE_EVENT, handleWorkbench);
		if(ctrl.workbench){
			ctrl.workbench.on(COTNTENT_CHANGE_EVENT, contentChanged);
		}
	});
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('am-wb-core')//

/**
 * @ngdoc Converter
 * @name WbConverter
 * @description Widget converter
 * 
 * A converter are responsible to encode and decode a widget.
 * 
 */
.factory('WbConverterAbstract', function () {

    /**
     * Creates new instance of the converter
     * 
     * @memberof WbConverter
     */
    function Converter(mimetype){
        this.mimetype = mimetype || 'text/plain';
    }
    
    /**
     * Convert widgets into data
     * 
     * @param widget {Widget} to convert
     * @return string of data
     * @memberof WbConverter
     */
    Converter.prototype.encode = function(){};
    
    /**
     * Converts the input data into list of widgets
     * 
     * @param data {string} to convert
     * @returns list of widgets
     * @memberof WbConverter
     */
    Converter.prototype.decode = function(){};
    
    /**
     * Get the data mimetype
     * 
     * When widgets are converted into data, then the data can transfer anywhere, but
     * data type is very important to recover. 
     * 
     * @memberof WbConverter
     */
    Converter.prototype.getMimetype = function(){
        return this.mimetype;
    };

    /**
     * Sets the data mimetype
     * 
     * NOTE: this is not global
     * 
     * @memberof WbConverter
     */
    Converter.prototype.setMimetype = function(mimetype){
        this.mimetype = mimetype;
    };
    
    return Converter;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Converter
 * @name WbConverterDom
 * @description Widget converter
 * 
 * A converter are responsible to encode and decode a widget.
 * 
 */
.factory('WbConverterDom', function (WbConverterAbstract, $widget) {
	function cssNameToJsName(name)
	{
		var split = name.split('-');
		var output = '';
		for(var i = 0; i < split.length; i++)
		{
			if (i > 0 && split[i].length > 0 && !(i === 1 && split[i] === 'ms'))
			{
				split[i] = split[i].substr(0, 1).toUpperCase() + split[i].substr(1);
			}
			output += split[i];
		}
		return output;
	}

	function convertElementToModel(element){
		var name = element.tagName;
		if(!name){
			return null;
		}
		name = name.toLowerCase();
		if(!$widget.hasWidget(name)){
			return null;
		}
		var model = {
				style:{}
		};
		model.type = name;
		// attributes
		_.forEach(element.attributes, function(attr){
			if(attr.name == 'style'){
				return;
			}
			if(attr.name == 'type'){
				model[model.type+'Type'] = attr.value;
				return;
			}
			model[attr.name] = attr.value;
		});
		//style
		for(var i = 0; i < element.style.length; i++){
			var sname = element.style.item(i);
			model.style[cssNameToJsName(sname)] = element.style.getPropertyValue(sname);
		}
		if($widget.isWidgetLeaf(name)){
			// html
			model.html = element.innerHTML;
			if(model.type === 'pre'){
				model.text = element.innerText;
			}
		} else {
			model.children = [];
			_.forEach(element.children, function(childelement){
				var childWidget = convertElementToModel(childelement);
				if(childWidget){
					model.children.push(childWidget);
				}
			});
			if(model.type === 'li' && model.children.length === 0){
				model.children.push({
					type: 'p',
					html: element.innerText
				});
			}
		}
		return model;
	}

	function Converter(){
		WbConverterAbstract.apply(this, ['text/html']);
	}
	Converter.prototype = new WbConverterAbstract();

	Converter.prototype.encode = function(){
		var widgets = Array.prototype.slice.call(arguments) || [];
		var data = '';
		while(widgets.length){
			var widget = widgets.pop();
			data += widget.getElement().prop('outerHTML') + '\n';
		}
		return data;
	};

	Converter.prototype.decode = function(data){
		var widgets = [];
		try{
			var element = angular.element(data);
			for(var i = 0; i < element.length; i++){
				var model = convertElementToModel(element[i]);
				if(model){
					widgets.push(model);
				}
			}
		} catch(ex){
//			console.error(ex);
		}
		return widgets;
	};


	return Converter;
});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Converter
 * @name WbConverter
 * @description Widget converter
 * 
 * A converter are responsible to encode and decode a widget.
 * 
 */
.factory('WbConverterText', function (WbConverterAbstract) {

    function Converter(){
        WbConverterAbstract.apply(this, ['text/plain']);
    }
    Converter.prototype = new WbConverterAbstract();

    Converter.prototype.encode = function(){
        var widgets = Array.prototype.slice.call(arguments) || [];
        var data = '';
        while(widgets.length){
            var widget = widgets.pop();
            if(widget.isLeaf()){
                if(widget.html){
                    data += widget.html() + '\n';
                }
            } else {
                widgets.push.apply(widgets, widget.getChildren());
            }
        }
        return data;
    };

    Converter.prototype.decode = function(data){
        var widgets = [];
        data = data.split('\n');
        _.forEach(data, function(item){
            item = _.trim(item);
            if(item.length){ 
                widgets.push({
                    type: 'p',
                    html: item
                });
            }
        });
        return widgets;
    };


    return Converter;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Converter
 * @name WbConverterWeburger
 * @description Widget converter
 * 
 * A converter are responsible to encode and decode a widget.
 * 
 */
.factory('WbConverterWeburger', function (WbConverterAbstract) {

    function Converter(){
        WbConverterAbstract.apply(this, ['application/json']);
    }
    Converter.prototype = new WbConverterAbstract();

    Converter.prototype.encode = function(){
        var widgets = Array.prototype.slice.call(arguments) || [];
        if(widgets.length === 1){
            return JSON.stringify(widgets[0].getModel());
        }
        var models = [];
        _.forEach(widgets, function(widget){
            models.push(widget.getModel());
        });
        return JSON.stringify(models);
    };

    Converter.prototype.decode = function(data){
        var widgets = [];
        try{
            var model = JSON.parse(data);
            if(angular.isArray(model)){
                widgets = model;
            } else {
                widgets = [];
                widgets.push(model);
            }
            // TODO: clean each item
        } catch(ex){
            // TODO:
        }
        return widgets;
    };


    return Converter;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')

/**
 * @ngdoc Directives
 * @name amh-cms-content-setting
 * @description Show and manage content meta datas.
 */
.directive('amhCmsContentSetting', function ($parse) {

	/*
	 * Link widget view
	 */
	function postLink($scope, $element, $attrs, $ctrls) {
		var ctrl = $ctrls[0];
		var ngModelCtrl = $ctrls[1];

		// Load ngModel
		ngModelCtrl.$render = function () {
			if(!ngModelCtrl.$viewValue){
				// TODO: maso, 2019: clean and lock settings
				return;
			}
			ctrl.loadContent(ngModelCtrl.$viewValue);
		};
	}

	return {
		templateUrl: 'views/directives/amh-cms-content-setting.html',
		restrict: 'E',
		replace: true,
		scope: {},
		link: postLink,
		controller: 'AmhCmsContentCtrl',
		controllerAs: 'ctrl',
		require: ['amhCmsContentSetting', 'ngModel']
	};
});

///* 
// * The MIT License (MIT)
// * 
// * Copyright (c) 2016 weburger
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//
//
//angular.module('ngMaterialHome')
///**
// * @ngdoc Directives
// * @name amh-contents
// * @description Shows the list of pages
// */
//.directive('amhContents', function () {
//
//	/*
//	 * Link widget view
//	 */
//	function postLink($scope, $element, $attrs, $ctrl) {
//		$ctrl.title = $attrs.amhTitle;
//		$scope.$watch($attrs.amhFilter, function(filters){
//			if(!angular.isArray(filters)){
//				return;
//			}
//			var queryParameter = $ctrl.getQueryParameter();
//			queryParameter.clearFilters();
//			for(var i = 0; i < filters.length; i++){
//				var filter = filters[i];
//				queryParameter.addFilter(filter.key, filter.value);
//			}
//			$ctrl.reload();
//		});
//	}
//
//	return {
//		templateUrl: 'views/amh-contents-list.html',
//		restrict: 'E',
//		replace: true,
//		scope: {},
//		link: postLink,
//		require: 'amhContents', 
//		controller: 'AmhContentPagesCtrl',
//		controllerAs: 'ctrl'
//	};
//});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
/**
 * @ngdoc Directives
 * @name mb-mini-action-anchor
 * @description produces top toolbar for the module in editing mode.
 */
.directive('mbMiniActionAnchor', function ($actions) {

	return {
		templateUrl: 'views/directives/amh-mini-action-anchor.html',
		restrict: 'E',
		replace: true,
		scope: {
			mbActionGroup: '@?',
			mbDivider: '<?',
			mbSize: '<?'
		},
		/*
		 * @ngInject
		 */
		controller: function($scope){
			var ctrl = this;
			
			function updateItems(){
				var value = $scope.mbActionGroup;
				if(value){
					ctrl.group = $actions.group($scope.mbActionGroup);
				} else {
					ctrl.group = {};
				}
			}
			
			$scope.$watch('mbActionGroup', updateItems);
			$scope.$on('destroy', function(){
				$actions.off('actionsChanged', updateItems);
				$actions.off('groupsChanged', updateItems);
			});
		},
		controllerAs: 'ctrl'
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
/**
 * @ngdoc Directives
 * @name amh-contents
 * @description Shows the list of pages
 */
.directive('amhTemplateContents', function () {

	/*
	 * Link widget view
	 */
	function postLink($scope, $element, $attrs, $ctrls) {
		var $ctrl = $ctrls[0];
		var ngModelCtrl = $ctrls[1];
		
		$ctrl.applyTemplate = function(value){
            ngModelCtrl.$setViewValue(value);
		};
		
		ngModelCtrl.$render = function () {
			$scope.template = ngModelCtrl.$viewValue;
		};

	}

	return {
		templateUrl: 'views/directives/amh-template-contents.html',
		restrict: 'E',
		replace: true,
		scope: {},
		link: postLink,
		require: ['amhTemplateContents', 'ngModel'], 
		controller: 'AmhContentTemplatesCtrl',
		controllerAs: 'ctrl'
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')

/**
 * @ngdoc Directives
 * @name amh-user-widgets
 * @description List all user widgets and let user drag and drop them
 * 
 * It is used to design the page with pre designed items
 */
.directive('amhUserWidgets', function(QueryParameter) {
	return {
		restrict : 'E',
		replace: true,
		templateUrl: 'views/directives/amh-user-widgets.html',
		scope: {},
		controller: 'AmhUserWidgetCtrl',
		controllerAs: 'ctrl'
	};
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
/**
 * @ngdoc Directives
 * @name amh-widget-path
 * @description Shows the path of a widget from its first parent to the widget itself.
 */
.directive('amhWidgetPath', function ($actions, $help) {

	/*
	 * Link widget view
	 */
	function postLink($scope, $element, $attrs, $ctrls) {
		$scope.widgets = [{id:1}];
		var workbench;
		// Load ngModel
		var ngModelCtrl = $ctrls[0];
		ngModelCtrl.$render = function () {
			if(workbench){
				workbench.off('selecteWidgetsChanged', eventHandler);
			}
			workbench = ngModelCtrl.$viewValue;
			if(workbench){
				workbench.on('selecteWidgetsChanged', eventHandler);
			}
		};

		function eventHandler($event){
			loadPath($event.value || []);
		}


		function loadPath(list) {
			if (list.length === 0) {
				$scope.widgets = [];
				return;
			}
			// Now list is an array of length 1 which contains just a widget.
			var widget = list[0];
			var newPath = [];
			while (widget) {
				newPath.push(widget);
				widget = widget.getParent();
			}
			newPath = newPath.reverse();
			if (!isPrefix(newPath, $scope.widgets)) {
				$scope.widgets = newPath;
			}
			$scope.prefixIndex = newPath.length;
		}

		/**
		 * The function:
		 * return 'false' if the newPath is not a prefix of oldPath
		 * return 'newPath.length' as index (which will be used for changing the color of path in view)
		 */
		function isPrefix(newPath, oldPath) {
			if (oldPath.length < newPath.length) {
				return false;
			}
			for (var i = 0; i < newPath.length; i++) {
				if (newPath[i] !== oldPath[i]) {
					return false;
				}
			}
			return true;
		}
	}

	return {
		templateUrl: 'views/directives/amh-widget-path.html',
		restrict: 'E',
		replace: true,
		scope: {},
		link: postLink,
		require: ['ngModel'],
		/**
		 * @ngInject
		 * @ngdoc Controllers
		 * @name amhWidgetPathCtrl
		 * @description Controls the widget path
		 * 
		 * There are many things which are controlled by the widget path controller
		 * such as opening help. 
		 */
		controller: function(){
			/**
			 * Opens help for a widget
			 * 
			 * @memberof amhWidgetPathCtrl
			 * @param widget {Widget} to display help for
			 */
			this.openHelp = function(widget){
				return $help.openHelp(widget);
			};
			
			this.selectChildren = function(widget, $event){
				$event.items = [widget];
				$actions.exec('amh.workbench.widget.selectChildren', $event);
			}
		},
		controllerAs: 'ctrl'
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
/**
 * @ngdoc Directives
 * @name amh-workbench-navigator
 * @description Show basic tools of work bench
 */
.directive('amhWorkbenchEditorToolbar', function ($rootScope) {
	return {
		templateUrl: 'views/directives/amh-workbench-editor-toolbar.html',
		restrict: 'E',
		replace: true,
		scope: true,
//		link: function ($scope, $element, $attrs, $ctrls) {
//		},
		require: ['ngModel'],
		/**
		 * @ngInject
		 * @ngdoc Controllers
		 * @name amhWidgetPathCtrl
		 * @description Controls the widget path
		 * 
		 * There are many things which are controlled by the widget path controller
		 * such as opening help. 
		 */
		controller: function(){
			this.toggleSettingPanel = function(){
				$rootScope.showWorkbenchSettingPanel = !$rootScope.showWorkbenchSettingPanel;
			};
		},
		controllerAs: 'ctrl'
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
/**
 * @ngdoc Directives
 * @name amh-workbench-navigator
 * @description Show basic tools of work bench
 */
.directive('amhWorkbenchNavigator', function (/*$actions*/) {

	return {
		templateUrl: 'views/directives/amh-workbench-navigator.html',
		restrict: 'E',
		replace: true,
		scope: {
			
		},
//		require: ['ngModel'],
//		/**
//		 * @ngInject
//		 * @ngdoc Controllers
//		 * @name amhWidgetPathCtrl
//		 * @description Controls the widget path
//		 * 
//		 * There are many things which are controlled by the widget path controller
//		 * such as opening help. 
//		 */
//		controller: function(/*$scope*/){
////			$scope.tools = $actions.group('amh.workbench.toolbar');
////			$scope.advanceTools = $actions.group('amh.workbench.toolbar#advanced');
//		},
//		controllerAs: 'ctrl'
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * Binds a TinyMCE widget to <textarea> elements.
 */
angular.module('ui.tinymce', [])
.value('uiTinymceConfig', {})
.directive('uiTinymce', function($rootScope, $compile, $timeout, $window, $sce, uiTinymceConfig, uiTinymceService) {
	
	uiTinymceConfig = uiTinymceConfig || {};

	if (uiTinymceConfig.baseUrl) {
		tinymce.baseURL = uiTinymceConfig.baseUrl;
	}

	return {
		require: ['ngModel', '^?form'],
		priority: 599,
		link: function(scope, element, attrs, ctrls) {
			if (!$window.tinymce) {
				return;
			}

			var ngModel = ctrls[0],
			form = ctrls[1] || null;

			var expression, options = {
					debounce: true
			}, tinyInstance,
			updateView = function(editor) {
				var content = editor.getContent({format: options.format}).trim();
				content = $sce.trustAsHtml(content);

				ngModel.$setViewValue(content);
				if (!$rootScope.$$phase) {
					scope.$digest();
				}
			};

			function toggleDisable(disabled) {
				if (disabled) {
					ensureInstance();

					if (tinyInstance) {
						tinyInstance.getBody().setAttribute('contenteditable', false);
					}
				} else {
					ensureInstance();

					if (tinyInstance && !tinyInstance.settings.readonly && tinyInstance.getDoc()) {
						tinyInstance.getBody().setAttribute('contenteditable', true);
					}
				}
			}

			// fetch a unique ID from the service
			var uniqueId = uiTinymceService.getUniqueId();
			attrs.$set('id', uniqueId);

			expression = {};

			angular.extend(expression, scope.$eval(attrs.uiTinymce));

			//Debounce update and save action
			var debouncedUpdate = (function(debouncedUpdateDelay) {
				var debouncedUpdateTimer;
				return function(ed) {
					$timeout.cancel(debouncedUpdateTimer);
					debouncedUpdateTimer = $timeout(function() {
						return (function(ed) {
							if (ed.isDirty()) {
								ed.save();
								updateView(ed);
							}
						})(ed);
					}, debouncedUpdateDelay);
				};
			})(400);

			var setupOptions = {
					// Update model when calling setContent
					// (such as from the source editor popup)
					setup: function(ed) {
						ed.on('init', function() {
							ngModel.$render();
							ngModel.$setPristine();
							ngModel.$setUntouched();
							if (form) {
								form.$setPristine();
							}
						});

						// Update model when:
						// - a button has been clicked [ExecCommand]
						// - the editor content has been modified [change]
						// - the node has changed [NodeChange]
						// - an object has been resized (table, image) [ObjectResized]
						ed.on('ExecCommand change NodeChange ObjectResized', function() {
							if (!options.debounce) {
								ed.save();
								updateView(ed);
								return;
							}
							debouncedUpdate(ed);
						});

						ed.on('blur', function() {
							element[0].blur();
							ngModel.$setTouched();
							if (!$rootScope.$$phase) {
								scope.$digest();
							}
						});

						ed.on('remove', function() {
							element.remove();
						});

						if (uiTinymceConfig.setup) {
							uiTinymceConfig.setup(ed, {
								updateView: updateView
							});
						}

						if (expression.setup) {
							expression.setup(ed, {
								updateView: updateView
							});
						}
					},
					format: expression.format || 'html',
					selector: '#' + attrs.id
			};
			// extend options with initial uiTinymceConfig and
			// options from directive attribute value
			angular.extend(options, uiTinymceConfig, expression, setupOptions);
			// Wrapped in $timeout due to $tinymce:refresh implementation, requires
			// element to be present in DOM before instantiating editor when
			// re-rendering directive
			$timeout(function() {
				if (options.baseURL){
					tinymce.baseURL = options.baseURL;
				}
				var maybeInitPromise = tinymce.init(options);
				if(maybeInitPromise && typeof maybeInitPromise.then === 'function') {
					maybeInitPromise.then(function() {
						toggleDisable(scope.$eval(attrs.ngDisabled));
					});
				} else {
					toggleDisable(scope.$eval(attrs.ngDisabled));
				}
			});

			ngModel.$formatters.unshift(function(modelValue) {
				return modelValue ? $sce.trustAsHtml(modelValue) : '';
			});

			ngModel.$parsers.unshift(function(viewValue) {
				return viewValue ? $sce.getTrustedHtml(viewValue) : '';
			});

			ngModel.$render = function() {
				ensureInstance();

				var viewValue = ngModel.$viewValue ?
						$sce.getTrustedHtml(ngModel.$viewValue) : '';

						// instance.getDoc() check is a guard against null value
						// when destruction & recreation of instances happen
						if (tinyInstance &&
								tinyInstance.getDoc()
						) {
							tinyInstance.setContent(viewValue);
							// Triggering change event due to TinyMCE not firing event &
							// becoming out of sync for change callbacks
							tinyInstance.fire('change');
						}
			};

			attrs.$observe('disabled', toggleDisable);

			// This block is because of TinyMCE not playing well with removal and
			// recreation of instances, requiring instances to have different
			// selectors in order to render new instances properly
			var unbindEventListener = scope.$on('$tinymce:refresh', function(e, id) {
				var eid = attrs.id;
				if (angular.isUndefined(id) || id === eid) {
					var parentElement = element.parent();
					var clonedElement = element.clone();
					clonedElement.removeAttr('id');
					clonedElement.removeAttr('style');
					clonedElement.removeAttr('aria-hidden');
					tinymce.execCommand('mceRemoveEditor', false, eid);
					parentElement.append($compile(clonedElement)(scope));
					unbindEventListener();
				}
			});

			scope.$on('$destroy', function() {
				ensureInstance();

				if (tinyInstance) {
					tinyInstance.remove();
					tinyInstance = null;
				}
			});

			function ensureInstance() {
				if (!tinyInstance) {
					tinyInstance = tinymce.get(attrs.id);
				}
			}
		}
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')
/**
 * @ngdoc Directives
 * @name wb-content
 * @description Render a list of widget
 *  ## wbOnModelSelect
 * 
 * If a widget select with in the group then the wbOnModelSelect parameter will
 * be evaluated with the following attributes:
 *  - $event : the related event - $ctrl : the widget (to support legacy) -
 * $model: the model (to support legace)
 * 
 * NOTE: The root widget will be passed as first selected item. The function
 * will be evaluated in non edit mode.
 */
.directive('wbContent', function($widget) {
	/*
	 * Link widget view
	 */
	function wbGroupLink($scope, $element, $attrs, ctrls) {
		var rootWidget;
		var ngModelCtrl = ctrls[0];
		// Load ngModel
		ngModelCtrl.$render = function() {
			var model = ngModelCtrl.$viewValue;
			if (!model) {
				return;
			}

			// 0- remove old
			var editable = false;
			if(rootWidget){
				editable = rootWidget.isEditable();
				rootWidget.destroy();
			}
			$element.empty();

			// 1- create widget
			$widget.compile(model, null, $element)
			.then(function(widget){
				rootWidget = widget;
				rootWidget.setEditable(editable);
				$element.trigger('load', widget);
			}, function(error){
				$element.error(error);
			});
		};
	}

	return {
		restrict : 'EA',
		scope : true,
		link : wbGroupLink,
		require: ['?ngModel']
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')

/**
 * @ngdoc Directives
 * @name wb-event-panel
 * @description Widgets settings
 * 
 * Loads list of settings.
 * 
 */
.directive('wbEventPanel', function () {
    /**
     * Init settings
     */
    function postLink($scope, $element, $attrs, $ctrls) {
        // Load ngModel
        var ngModelCtrl = $ctrls[0];
        var widget = null;
        var eventTypes = [{
            key: 'init',
            title: 'Initialization'
        }, {
            key: 'click',
            title: 'Click'
        }, {
            key: 'dblclick',
            title: 'Double click'
        }, {
            key: 'mouseout',
            title: 'Mouse out'
        }, {
            key: 'mouseover',
            title: 'Mouse over'
        }, {
            key: 'mousedown',
            title: 'Mouse down'
        }, {
            key: 'mouseup',
            title: 'Mouse up'
        }, {
            key: 'mouseenter',
            title: 'Mouse enter'
        }, {
            key: 'mouseleave',
            title: 'Mouse leave'
        }, {
            key: 'resize',
            title: 'Resize'
        }, {
            key: 'intersection',
            title: 'Intersection'
        }, {
            key: 'success',
            title: 'Success'
        }, {
            key: 'error',
            title: 'Failure'
        }, {
            key: 'load',
            title: 'Load'
        }, {
            key: 'load',
            title: 'Load'
        }];

        ngModelCtrl.$render = function () {
            if (ngModelCtrl.$viewValue) {
                cleanEvents();
                widget = ngModelCtrl.$viewValue;
                if (angular.isArray(widget)) {
                    if(widget.length > 0){
                        widget = widget[0];
                    }else {
                        widget = null;
                    }
                }
                loadEvents();
            }
        };

        function cleanEvents() {
            $scope.events = [];
        }

        function loadEvents() {
            cleanEvents();
            if(!widget){
                return;
            }
            for (var i = 0; i < eventTypes.length; i++) {
                var event = eventTypes[i];
                event.code = widget.getModelProperty('on.' + event.key);
                $scope.events.push(event);
            }
        }

        function saveEvents() {
            if(!widget){
                return;
            }
            for (var i = 0; i < $scope.events.length; i++) {
                var event = $scope.events[i];
                if (event.code) {
                    widget.setModelProperty('on.' + event.key, event.code);
                } else {
                    widget.setModelProperty('on.' + event.key, undefined);
                }
            }
        }

        /**
         * Save events into the model
         */
        $scope.saveEvents = saveEvents;

        $element.on('keypress keyup keydown paste copy', function(event){
            event.stopPropagation();
        });
    }

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'views/directives/wb-event-panel.html',
        scope: {},
        link: postLink,
        require: ['ngModel'],
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function ($scope, $resource) {

            var defaultLanguages = [{
                text: 'JavaScript',
                value: 'javascript'
            }];
            this.editEvent = function (event, $evn) {
                $evn.stopPropagation();
                $resource.get('script', {
                    data: {
                        language: 'javascript',
                        languages: defaultLanguages,
                        code: event.code
                    }
                }).then(function (value) {
                    event.code = value.code;
                    if (!value) {
                        delete event.code;
                    }
                    $scope.saveEvents();
                });
            };

            this.deleteEvent = function (event, $evn) {
                $evn.stopPropagation();
                delete event.code;
                $scope.saveEvents();
            };
        }
    };
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')

/**
 * @ngdoc Directives
 * @name wb-setting-page
 * @description Display a setting of a model
 * 
 */
.directive('wbSettingPage', function ($widget, $settings, $wbUtil, $controller, $compile, $mdTheming) {

	function postLink($scope, $element, $attrs, $ctrls) {
		var widgets = [];
		var settingCtrl = null;

		function loadSetting(page) {
			return $wbUtil.getTemplateFor(page)
			.then(function (templateSrc) {
				var element = angular.element(templateSrc);
				var scope = $scope.$new();
				var controller = $controller('WbSettingPageCtrl',{
					$scope: scope,
					$element: element
				});
				if (angular.isDefined(page.controller)) {
					controller = angular.extend(controller, $controller(page.controller, {
						$scope: scope,
						$element: element
					}));
					if (page.controllerAs) {
						scope[page.controllerAs] = controller;
					}
					element.data('$ngControllerController', controller);
				}
				$compile(element)(scope);
				$mdTheming(element);
				$element.empty();
				$element.append(element);
				try{
					if(_.isFunction(controller.init)){
						controller.init();
					}
				} catch(ex){
					// TODO:
				}
				return controller;
			});
		}

		$scope.$watch('type', function (type) {
			if (!type) {
				return;
			}
			var setting = $settings.getPage(type);
			loadSetting(setting)//
			.then(function(ctrl){
				settingCtrl = ctrl;
				ctrl.setWidget(widgets);
			});
		});

		// Load ngModel
		var ngModelCtrl = $ctrls[0];
		ngModelCtrl.$render = function () {
			widgets = ngModelCtrl.$viewValue;
			if(settingCtrl) {
				settingCtrl.setWidget(widgets);
			}
		};
	}

	// create directive
	return {
		restrict: 'E',
		replace: true,
		template: '<div layout="column"></div>',
		link: postLink,
		scope: {
			type: '@wbType'
		},
		require: ['ngModel']
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')

/**
 * @ngdoc Directives
 * @name wb-setting-panel-group
 * @description Widgets settings
 * 
 * Loads list of settings.
 * 
 */
.directive('wbSettingPanelGroup', function($settings, $widget) {

	/**
	 * Init settings
	 */
	function postLink($scope, $element, $attrs, $ctrls) {

		// Load ngModel
		var ngModelCtrl = $ctrls[0];
		var settingMap = [];
		$scope.settings = [];

		/**
		 * تنظیمات را به عنوان تنظیم‌های جاری سیستم لود می‌کند.
		 * 
		 * @returns
		 */
		function loadSetting(widgets) {

			// hide all settings
			var i;
			for(i = 0; i < $scope.settings.length; i++){
				$scope.settings[i].visible = false;
			}

			if(_.isEmpty(widgets)){
				$scope.wbModel = null;
				return;
			}

			// load pages
			var settingKeys = [];
			_.forEach(widgets, function(widget){
				var widgetDef = $widget.getWidget(widget.getModel());
				var settingKeysOfWidget = $settings.getSettingsFor(widgetDef);
				if(_.isEmpty(settingKeys)){
					settingKeys = settingKeysOfWidget;
				} else {
					settingKeys = _.intersection(settingKeys, settingKeysOfWidget);
				}
			});

			// visible new ones
			for(i = 0; i < settingKeys.length; i++){
				var key = settingKeys[i].type;
				if(!settingMap[key]){
					var setting = settingKeys[i];
					settingMap[key] = angular.copy(setting);
					$scope.settings.push(settingMap[key]);
				}
				settingMap[key].visible = true;
			}

			// set model in view
			$scope.wbModel = widgets;
		}

		ngModelCtrl.$render = function() {
			if(ngModelCtrl.$viewValue) {
				var widgets = ngModelCtrl.$viewValue;
				loadSetting(widgets);
			}
		};


		$element.on('keypress keyup keydown paste copy', function(event){
			event.stopPropagation();
		});
	}

	return {
		restrict : 'E',
		replace: true,
		templateUrl: function($element, $attr){
			var link = 'views/directives/wb-setting-panel-';
			if(angular.isDefined($attr.wbTabMode)){
				link += 'tabs.html';
			} else {
				link += 'expansion.html';
			}
			return link;
		},
		scope : {},
		link : postLink,
		require:['ngModel']
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * link setting widgets
 */
function wbUiSettingLinkFunction($scope, $element, $attrs, ctrls) {
	var ngModel = ctrls[0];

	$scope.wbActionClean = ! _.isUndefined($attrs.wbActionClean);

	ngModel.$render = function () {
		$scope.value = ngModel.$modelValue;
	};

	$scope.cleanValue = function () {
		setValue(undefined);
	};

	$scope.setValue = setValue;

	function setValue(value){
		$scope.value = value;
		// TODO: validate and set
		ngModel.$setViewValue(value);
	}
}

angular.module('am-wb-core')

/**
 * @ngdoc Directives
 * @name wb-ui-settingn-boolean
 * @description a setting section for on/off switch.
 *
 */
.directive('wbUiSettingBoolean', function () {
	return {
		templateUrl: 'views/directives/wb-ui-setting-boolean.html',
		restrict: 'E',
		replace: true,
		scope: {
			title: '@wbTitle',
			description: '@wbDescription',
		},
		require: ['ngModel'],
		link: wbUiSettingLinkFunction
	};
})

/**
 * @ngdoc Directives
 * @name wb-ui-setting-text
 * @description Setting for a text
 */
.directive('wbUiSettingText', function () {
	return {
		templateUrl: 'views/directives/wb-ui-setting-text.html',
		restrict: 'E',
		scope: {
			title: '@wbTitle',
			description: '@wbDescription',
			resourceType: '@?wbResourceType'
		},
		require: ['ngModel'],
		link: wbUiSettingLinkFunction,
		/*
		 * @ngInject
		 */
		controller: function($scope, $resource){
			function openResourcePage(type){
				return $resource.get(type, {
					data: $scope.value,
					style: {
						title: $scope.title,
						description: $scope.description
					}
				})
				.then(function(newValue){
					$scope.setValue(newValue);
				});
			}
			$scope.openResource = function(){
				openResourcePage($scope.resourceType);
			};
		}
	};
})


/**
 * @ngdoc Directives
 * @name wbUiSettingSelect
 * @description a setting section for choosing values.
 */
.directive('wbUiSettingSelect', function () {
	return {
		templateUrl: 'views/directives/wb-ui-setting-select.html',
		restrict: 'E',
		scope: {
			title: '@wbTitle',
			description: '@wbDescription',
		},
		require: ['ngModel'],
		link: wbUiSettingLinkFunction
	};
})


/**
 * @ngdoc Directives
 * @name wbUiSettingNumber
 * @description a setting section to set a number.
 *
 */
.directive('wbUiSettingNumber', function () {
	return {
		templateUrl: 'views/directives/wb-ui-setting-number.html',
		restrict: 'E',
		scope: {
			title: '@wbTitle',
			description: '@wbDescription',
		},
		require: ['ngModel'],
		link: wbUiSettingLinkFunction
	};
})


/**
 * @ngdoc Directives
 * @name wbUiSettingLength
 */
.directive('wbUiSettingLength', function () {
	return {
		templateUrl: 'views/directives/wb-ui-setting-length.html',
		restrict: 'E',
		replace: true,
		scope: {
			title: '@wbTitle',
			description: '@wbDescription',
		},
		require: ['ngModel'],
		link: wbUiSettingLinkFunction
	};
})

/**
 * @ngdoc Directives
 * @name wbUiSettingColor
 * @description a setting section to set color.
 *
 */
.directive('wbUiSettingColor', function ($mdColorPicker){
	return {
		templateUrl: 'views/directives/wb-ui-setting-color.html',
		restrict: 'E',
		scope: {
			title: '@wbTitle',
			description: '@wbDescription',


			options: '=wbColorPicker',

			// Input options
			type: '@wbType',
			label: '@?wbLabel',
			icon: '@?wbIcon',
			random: '@?wbRandom',
		default: '@?wbDefault',

		// Dialog Options
		openOnInput: '=?wbOpenOnInput',
		hasBackdrop: '=?wbHasBackdrop',
		clickOutsideToClose: '=?wbClickOutsideToClose',
		skipHide: '=?wbSkipHide',
		preserveScope: '=?wbPreserveScope',

		// Advanced options
		wbColorClearButton: '=?wbColorClearButton',
		wbColorPreview: '=?wbColorPreview',

		wbColorAlphaChannel: '=?wbColorAlphaChannel',
		wbColorSpectrum: '=?wbColorSpectrum',
		wbColorSliders: '=?wbColorSliders',
		wbColorGenericPalette: '=?wbColorGenericPalette',
		wbColorMaterialPalette: '=?wbColorMaterialPalette',
		wbColorHistory: '=?wbColorHistory',
		wbColorHex: '=?wbColorHex',
		wbColorRgb: '=?wbColorRgb',
		wbColorHsl: '=?wbColorHsl',
		wbColorDefaultTab: '=?wbColorDefaultTab'
		},
		require: ['ngModel'],
		link: wbUiSettingLinkFunction,
		/*
		 * @ngInject
		 */
		controller: function($scope, $element) {
			var preview = $element.find('.preview');
			$scope.$watch('value', function(color){
				preview.css({background: color});
			});

			$scope.selectColor = function($event){

				$mdColorPicker.show({
					value: $scope.value || 'red',
					defaultValue: $scope.default,
					random: $scope.random,
					clickOutsideToClose: $scope.clickOutsideToClose,
					hasBackdrop: $scope.hasBackdrop,
					skipHide: $scope.skipHide,
					preserveScope: $scope.preserveScope,

					mdColorAlphaChannel: $scope.wbColorAlphaChannel,
					mdColorSpectrum: $scope.wbColorSpectrum,
					mdColorSliders: $scope.wbColorSliders,
					mdColorGenericPalette: $scope.wbColorGenericPalette,
					mdColorMaterialPalette: $scope.wbColorMaterialPalette,
					mdColorHistory: $scope.wbColorHistory,
					mdColorHex: $scope.wbColorHex,
					mdColorRgb: $scope.wbColorRgb,
					mdColorHsl: $scope.wbColorHsl,
					mdColorDefaultTab: $scope.wbColorDefaultTab,

					$event: $event

				}).then(function( color ) {
					$scope.setValue(color);
				});
			};
		},
		controllerAs: 'ctrl'
	};
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')

/**
 * @ngdoc Directives
 * @name wb-widgets-explorer
 * @description Widgets explorers
 * 
 * This is widgets explorer list.
 * 
 */
.directive('wbWidgetsExplorer', function($widget, $rootScope) {
	/*
	 * link function
	 */
	function postLink(scope, element, attrs, ctrls) {

		var ngModel = ctrls[0];
		var widgets = null;

		if($rootScope.app && $rootScope.app.setting) {
			// save setting in root scope
			if(!$rootScope.app.setting.wbWidgetExplorer){
				$rootScope.app.setting.wbWidgetExplorer = {};
			}
			scope.wbWidgetExplorer = $rootScope.app.setting.wbWidgetExplorer;
		} else {
			scope.wbWidgetExplorer = {};
		}

		/*
		 * Filter widgets width the query
		 */
		function _loadQuery(query, widgets){
			if(query) {
				var q = query.trim().toLowerCase();
				return widgets.filter(function(w){
					return w.title.toLowerCase().indexOf(q) > -1 || w.description.indexOf(q) > -1;
				});
			}
			return widgets;
		}

		/*
		 * Load widgets in groups
		 */
		function _loadGroups(widgets){
			var groups = [];
			var tmp = {};
			for(var i = 0; i < widgets.length; i++){
				var gl = widgets[i].groups || [];
				for(var j = 0; j < gl.length; j++){
					var gid = gl[j];
					if(!angular.isDefined(tmp[gid])){
						tmp[gid] = angular.copy($widget.group(gid));
						tmp[gid].widgets = [];
						groups.push(tmp[gid]);
					}
					tmp[gid].widgets.push(widgets[i]);
				}
			}
			return groups;
		}

		function _runQuery(/*query, $event*/){
			scope.widgets = _loadQuery(scope.query, widgets);
			scope.groups = _loadGroups(scope.widgets);
		}

		function _load(){
			if(!widgets){
				scope.widgets = [];
				return;
			}
			// maso, 2018: clear configs
			scope.query = '';
			scope.widgets = _loadQuery(scope.query, widgets);
			scope.groups = _loadGroups(scope.widgets);
		}

		// Load models
		ngModel.$render = function(){
			widgets = ngModel.$modelValue;
			_load();
		};

		scope.runQuery = _runQuery;
	}

	return {
		templateUrl : 'views/directives/wb-widgets-explorer.html',
		restrict : 'E',
		replace : true,
		scope: {},
		require: ['ngModel'],
		link : postLink
	};
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')

/**
 * @ngdoc Directives
 * @name wb-widgets-list
 * @description Widgets explorers
 * 
 * This is widgets explorer list.
 * 
 */
.directive('wbWidgetsList', function($window) {

    return {
        templateUrl : 'views/directives/wb-widgets-list.html',
        restrict : 'E',
        replace : true,
        scope : {
            widgets : '<'
        },
        /*
         * @ngInject
         */
        controller : function($scope) {
            if (angular.isFunction($window.openHelp)) {
                $scope.openHelp = function(widget, $event) {
                    $window.openHelp(widget, $event);
                };
            }

            $scope.putDragdata = function(event, widget){
                event = event.originalEvent || event;
                event.dataTransfer.setData('application/json', JSON.stringify(widget.model));
            };
        }
    };
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')

/**
 * @ngdoc Directives
 * @name wb-widgets-module
 * @description Widgets explorers
 * 
 * This is widgets explorer list.
 * 
 */
.directive('wbWidgetsModule', function($window) {

    return {
        templateUrl : 'views/directives/wb-widgets-module.html',
        restrict : 'E',
        replace : true,
        scope: {
            widgets: '<'
        },
        /*
         * @ngInject
         */
        controller: function($scope){
            if(angular.isFunction($window.openHelp)){
                $scope.openHelp = function(widget, $event){
                    $window.openHelp(widget, $event);
                };
            }

            $scope.putDragdata = function(event, widget){
                event = event.originalEvent || event;
                event.dataTransfer.setData('application/json', JSON.stringify(widget.model));
            };
        }
    };
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
/**
 * 
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/copy_event
 * @param AmhEditorProcessor
 * @param $window
 * @returns
 */
.factory('AmhEditorProcessorClipboard', function(
		AmhEditorProcessor, $widget, $actions) {
	

	var EditorProcessor = function (editor, options) {
		options = options || {};
		AmhEditorProcessor.apply(this, [editor, options]);
	};

	EditorProcessor.prototype = new AmhEditorProcessor();


	EditorProcessor.prototype.connect = function(){
		if(angular.isFunction(this.editModeListener)){
			this.disconnect();
		}
		var ctrl = this;
		var editor = this.editor;
		// add actions
		$actions.newAction({
			id: 'amh.workbench.editor.copy',
			priority: 15,
			icon: 'content_copy',
			title: 'Copy',
			description: 'Copy selected widgets',
			visible: function () {
				return editor.getSelectedWidgets().length && editor.isEditable();
			},
			/*
			 * @ngInject
			 */
			action: function () {
				return ctrl.copy();
			},
			groups: ['amh.workbench.editor.weburger.toolbar#clipboard'],
			scope: this.editor.getScope()
		});
		$actions.newAction({
			id: 'amh.workbench.editor.past',
			priority: 15,
			icon: 'content_paste',
			title: 'Past',
			description: 'Past widgets from clipboard',
			visible: function () {
				return editor.getSelectedWidgets().length && editor.isEditable();
			},
			/*
			 * @ngInject
			 */
			action: function () {
				return ctrl.pastInternal();
			},
			groups: ['amh.workbench.editor.weburger.toolbar#clipboard'],
			scope: this.editor.getScope()
		});
		$actions.newAction({
			id: 'amh.workbench.editor.cut',
			priority: 15,
			icon: 'content_cut',
			title: 'Cut',
			description: 'Cut widgets into the clipboard',
			visible: function () {
				return editor.getSelectedWidgets().length && editor.isEditable();
			},
			/*
			 * @ngInject
			 */
			action: function () {
				return ctrl.cut();
			},
			groups: ['amh.workbench.editor.weburger.toolbar#clipboard'],
			scope: this.editor.getScope()
		});
		/*
		 * handle past
		 */
		this.copyListener = function(event){
			ctrl.copy(event);
		};

		/*
		 * handle past
		 */
		this.pastListener = function(event){
			ctrl.past(event);
		}
		
		/*
		 * handle cut
		 */
		this.cutListener = function(event){
			ctrl.cut(event);
		}

		/*
		 * Check editor state
		 */
		this.editModeListener = function() {
			var element = ctrl.editor.getElement();
			if(ctrl.editor.isEditable()){
				element.attr('tabindex', '1');
				element.focus();
				element[0].addEventListener('copy', ctrl.copyListener);
				element[0].addEventListener('paste', ctrl.pastListener);
				element[0].addEventListener('cut', ctrl.cutListener);
			} else {
				element.removeAttr('tabindex');
				element[0].removeEventListener('copy', ctrl.copyListener);
				element[0].removeEventListener('paste', ctrl.pastListener);
				element[0].removeEventListener('cut', ctrl.cutListener);
			}
		};
		this.editor.on('stateChanged', this.editModeListener);
	};

	/**
	 * Disconnect form editor
	 */
	EditorProcessor.prototype.disconnect = function(){
		var element = this.editor.getElement();
		this.editor.off('stateChanged', this.editModeListener);
		element[0].removeEventListener('copy', this.copyListener);
		element[0].removeEventListener('paste', this.pastListener);
		delete this.stateListener;
		delete this.pastListener;
		delete this.copyListener;
	};

	EditorProcessor.prototype.copy = function(event){
		// use chrom event
		var selectedWidgets = this.editor.getSelectedWidgets();
		if (selectedWidgets.length === 0) {
			return;
		} 
		var data;
		if (selectedWidgets.length === 1) {
			data = selectedWidgets[0].getModel();
		} else {
			data = [];
			angular.forEach(selectedWidgets, function (widget) {
				data.push(widget.getModel());
			});
		}
		if(event) {
			// NOTE: plain text is supported with common browsers
			event.clipboardData.setData('text/plain', JSON.stringify(data));
			event.preventDefault();
		} else {
			navigator.clipboard.writeText(JSON.stringify(data));
		}
	}

	EditorProcessor.prototype.past = function(event){
		var clipboardData = (event.clipboardData || window.clipboardData);
		var mimeType = 'text/plain';
		var data =  clipboardData.getData('text');
		var converter = $widget.getConverter('text/plain');

		// Browsers that support the 'text/html' type in the Clipboard API (Chrome, Firefox 22+)
		if (clipboardData && clipboardData.types) {
			var converters = $widget.getConverters();
			for(var i = 0; i < converters.length; i++){
				var tc = converters[i];
				mimeType = tc.getMimetype();
				if(_.includes(clipboardData.types, mimeType)){
					converter = tc;
					data =  clipboardData.getData(mimeType);
					break;
				}
			}
		}
		addWidgets(this.editor, converter.decode(data));
		// Stop the data from actually being pasted
		event.stopPropagation();
		event.preventDefault();
		return false;
	};

	EditorProcessor.prototype.pastInternal = function(){
		var workbench = this.editor;
		navigator.clipboard.readText()
		.then(function(data) {
			var converters = $widget.getConverters();
			for(var i = 0; i < converters.length; i++){
				var converter = converters[i];
				var widgets = converter.decode(data);
				if(widgets.length){
					addWidgets(workbench, widgets);
					return;
				}
			}
		});
	};
	
	EditorProcessor.prototype.cut = function(){
		this.copy();
		var selectedWidgets = this.editor.getSelectedWidgets();
		_.forEach(selectedWidgets, function(widget){
			widget.delete();
		});
	};

	function addWidgets(workbench, widgets){
		var group = workbench.getRootWidget();
		var index = 0;
		var selectedWidgets = workbench.getSelectedWidgets();
		if (selectedWidgets.length !== 0) {
			var item = selectedWidgets[selectedWidgets.length - 1];
			if(!item.isLeaf() && selectedWidgets.length === 1){
				group = item;
			} else {
				group = item.getParent() || item;
				index = group.indexOfChild(item);
			}
		}
		group.addChildrenModel(index, widgets);
	}

	return EditorProcessor;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
.factory('AmhEditorProcessorCommon', function(
		/* MBlowFish */ $help, $actions,
		AmhEditorProcessor) {
	

	var COMMON_ACTION_GROUPS = ['amh.workbench.editor.weburger.toolbar#common'];

	function Processor (editor, options) {
		options = options || {};
		AmhEditorProcessor.apply(this, [editor, options]);
	};
	Processor.prototype = new AmhEditorProcessor();


	Processor.prototype.connect = function(){
		if(angular.isFunction(this.editModeListener)){
			this.disconnect();
		}
		var ctrl = this;
		var editor = this.editor;

		function commonActionVisible() {
			return editor.getSelectedWidgets().length && editor.isEditable();
		}

		function singleWidgetActionVisible() {
			return editor.getSelectedWidgets().length === 1 && editor.isEditable();
		}

		// add actions
		$actions.newAction({
			id: 'amh.workbench.editor.delete',
			priority: 15,
			icon: 'delete',
			title: 'Delete',
			description: 'Delete selected widgets',
			visible: commonActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.deleteWidgets($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});
		$actions.newAction({
			id: 'amh.workbench.editor.clone',
			priority: 15,
			icon: 'view_stream',
			title: 'Clone',
			description: 'Clones selected widgets',
			visible: commonActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.cloneWidgets($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});
		$actions.newAction({
			id: 'amh.workbench.editor.help',
			priority: 15,
			icon: 'help',
			title: 'Help',
			description: 'Show help for the selected widget',
			visible: singleWidgetActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.openHelpOfWidgets($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});

		$actions.newAction({
			id: 'amh.workbench.editor.moveFirst',
			priority: 103,
			icon: 'first_page',
			title: 'First',
			description: 'Move widgets to the first postion',
			visible: commonActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.moveFirstWidgets($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});
		$actions.newAction({
			id: 'amh.workbench.editor.movePrevious',
			priority: 102,
			icon: 'chevron_left',
			title: 'Previous',
			description: 'Move widget to the previous postion',
			visible: commonActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.moveBeforeWidgets($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});
		$actions.newAction({
			id: 'amh.workbench.editor.moveNext',
			priority: 101,
			icon: 'chevron_right',
			title: 'Next',
			description: 'Move the widget to the next position',
			visible: commonActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.moveNextWidgets($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});
		$actions.newAction({
			id: 'amh.workbench.editor.moveLast',
			priority: 100,
			icon: 'last_page',
			title: 'Last',
			description: 'Move widgets to the last postion',
			visible: commonActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.moveLastWidgets($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});



		/*
		 * handle past
		 */
		this.keyEventListener = function(event){
			if (event.code == 'Delete') {
				ctrl.deleteWidgets();
			}
		}
		/*
		 * Check editor state
		 */
		this.editModeListener = function() {
			var element = ctrl.editor.getElement();
			if(ctrl.editor.isEditable()){
				element[0].addEventListener('keyup', ctrl.keyEventListener);
			} else {
				element[0].removeEventListener('keyup', ctrl.keyEventListener);
			}
		};
		this.editor.on('stateChanged', this.editModeListener);
	};

	/**
	 * 
	 * @memberof AmhEditorProcessorCommon
	 */
	Processor.prototype.disconnect = function(){
		var element = this.editor.getElement();
		this.editor.off('stateChanged', this.editModeListener);
		element[0].removeEventListener('keyup', this.keyEventListener);
		delete this.stateListener;
		delete this.keyEventListener;
	};

	/**
	 * 
	 * @memberof AmhEditorProcessorCommon
	 */
	Processor.prototype.deleteWidgets = function(){
		//NOTE: The list may be changed after the first delete. Therefore I clone the original
		//selected widgets and do the delete action over it.
		var widgets = _.clone(this.editor.getSelectedWidgets());
		var newSelection = [];
		widgets.forEach(function (widget) {
			var parent = widget.getParent();
			widget.delete();
			if(parent){
				parent.setSelected(true);
				newSelection.push(parent);
			}
		});
		this.editor.setSelectedWidgets(newSelection);
	};

	/**
	 * Clones selected widgets
	 * 
	 * @memberof AmhEditorProcessorCommon
	 */
	Processor.prototype.cloneWidgets = function(){
		var widgets = this.editor.getSelectedWidgets();
		widgets.forEach(function (widget) {
			widget.clone();
		});
	};

	/**
	 * Move selected widgets to the first position
	 * 
	 * @memberof AmhEditorProcessorCommon
	 */
	Processor.prototype.moveFirstWidgets = function(){
		var widgets = this.editor.getSelectedWidgets();
		widgets.forEach(function (widget) {
			widget.moveFirst();
		});
	};

	/**
	 * Move selected widgets to the privieus postion
	 * 
	 * @memberof AmhEditorProcessorCommon
	 */
	Processor.prototype.moveBeforeWidgets = function(){
		var widgets = this.editor.getSelectedWidgets();
		widgets.forEach(function (widget) {
			widget.moveBefore();
		});
	};

	/**
	 * Moves selected widgets to the next position
	 * 
	 * @memberof AmhEditorProcessorCommon
	 */
	Processor.prototype.moveNextWidgets = function(){
		var widgets = this.editor.getSelectedWidgets();
		widgets.forEach(function (widget) {
			widget.moveNext();
		});
	};

	/**
	 * Moves selected widgets to the last postions
	 * 
	 * @memberof AmhEditorProcessorCommon
	 */
	Processor.prototype.moveLastWidgets = function(){
		var widgets = this.editor.getSelectedWidgets();
		widgets.forEach(function (widget) {
			widget.moveLast();
		});
	};

	/**
	 * Open help for first selected widgets
	 * 
	 * @memberof AmhEditorProcessorCommon
	 */
	Processor.prototype.openHelpOfWidgets = function(){
		var widgets = this.editor.getSelectedWidgets();
		return $help.openHelp(widgets[0]);
	};

	return Processor;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
/**
 * @ngdoc Processor
 * @name AmhEditorProcessorUtils
 * 
 */
.factory('AmhEditorProcessorUtils', function(
		/* amh       */ AmhEditorProcessor, 
		/* weburger  */ $widget, WbProcessorLocator, WbProcessorSelect, WbProcessorDnd,
		/* mblowfish */ $actions,
		/* angularjs */ $rootScope) {
	

	var COMMON_ACTION_GROUPS = ['amh.workbench.editor.weburger.toolbar#common'];
	
	function Processor(editor, options) {
		options = options || {};
		AmhEditorProcessor.apply(this, [editor, options]);


		this.locatorProcessor = new WbProcessorLocator();
		this.selectProcessor = new WbProcessorSelect();
		this.dndProcessor = new WbProcessorDnd();

		var ctrl = this;
		this.selectProcessor.on('selectionChange', function(){
			// XXX:
			var selectedWidgets = ctrl.selectProcessor.getSelectedWidgets();
			ctrl.editor.setSelectedWidgets(selectedWidgets || []);
			ctrl.editor.getScope().$digest();
		});
	};
	Processor.prototype = new AmhEditorProcessor();


	
	/*
	 * Connect to the editor
	 * @see AmhEditorProcessor#connect
	 */
	Processor.prototype.connect = function(){
		if(angular.isFunction(this.editModeListener)){
			this.disconnect();
		}
		var ctrl = this;
		var editor = ctrl.editor;
		function singleContainerActionVisible() {
			var widgets = editor.getSelectedWidgets();
			return widgets.length === 1 && 
			!widgets[0].isLeaf() &&
			editor.isEditable();
		}
		
		/*
		 * Check editor state
		 */
		this.editModeListener = function() {
			if(ctrl.editor.isEditable()){
				ctrl.connectUtilities();
			} else {
				ctrl.disConnectUtilities();
			}
			editor.getRootWidget().setEditable(editor.isEditable());
		};
		this.editor.on('stateChanged', this.editModeListener);
		
		$actions.newAction({
			id: 'amh.workbench.widget.selectChildren',
			priority: 15,
			icon: 'select_all',
			title: 'Select Children',
			description: 'Select all children of the selected widget',
			visible: singleContainerActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.selectChildrenWidget($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});
		$actions.newAction({
			id: 'amh.workbench.widget.click',
			priority: 15,
			icon: 'select',
			title: 'Select the widget',
			description: 'Select the widget',
			visible: singleContainerActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.clickWidget($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});
		$actions.newAction({
			id: 'amh.workbench.widget.dblclick',
			priority: 15,
			icon: 'eidt',
			title: 'Edit the widget',
			description: 'Edit the widget',
			visible: singleContainerActionVisible,
			/*
			 * @ngInject
			 */
			action: function ($event) {
				return ctrl.dblclickWidget($event);
			},
			groups: COMMON_ACTION_GROUPS,
			scope: this.editor.getScope()
		});
	};

	/*
	 * Disconnect form editor
	 * @see AmhEditorProcessor#disconnect
	 */
	Processor.prototype.disconnect = function(){
		this.disConnectUtilities();
	};

	Processor.prototype.connectUtilities = function(){
		$widget.setProcessor('locator', this.locatorProcessor);
		$widget.setProcessor ('select', this.selectProcessor);
		$widget.setProcessor('dnd', this.dndProcessor);

		this.locatorProcessor.setEnable(true);
		this.selectProcessor.setEnable(true);
	};

	Processor.prototype.disConnectUtilities = function(){
		this.locatorProcessor.setEnable(false);
		this.selectProcessor.setEnable(false);
	};

	Processor.prototype.selectChildrenWidget = function ($event) {
		var widgets = [];
		var items = $event.items || this.selectProcessor.getSelectedWidgets();
		_.forEach(items || [] , function(item){
			if(item.getChildren){
				widgets = _.concat(widgets, item.getChildren());
			}
		});
		return this.selectProcessor.setSelectedWidgets(widgets);
	};
	
	Processor.prototype.clickWidget = function ($event) {
		$event.source = $event.items[0];
		return this.selectProcessor.clickListener($event);
	};
	
	Processor.prototype.dblclickWidget = function ($event) {
		$event.source = $event.items[0];
		return this.selectProcessor.dblclickListener($event);
	};
	
	return Processor;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
.factory('AmhEditorProcessor', function() {
	

	var Processor = function (editor) {
		this.editor = editor;
		this.connect();
	};
	
	Processor.prototype.destroy = function(){
		this.disconnect();
		delete this.editor;
	};
	
	
	Processor.prototype.connect = function(){};
	Processor.prototype.disconnect = function(){};
	
	
	return Processor;
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Factories
 * @name WidgetEditorFake
 * @description Editor of a widget
 * 
 */

.factory('WidgetEditorCode', function ($resource, WidgetEditor) {

    /**
     * TODO: maso, 2019: extends WidgetEditorFake
     * 
     * Creates new instace of an editor
     */
    function editor(widget, options) {
        options = options || {};
        WidgetEditor.apply(this, [widget, options]);
    }
    
    editor.prototype = new WidgetEditor();


    editor.prototype.setActive = function(){}; 
    editor.prototype.isActive = function(){};
    editor.prototype.save = function(){};
    editor.prototype.hide = function(){};
    editor.prototype.show = function(){
        var ctrl = this;
        $resource.get('code', {
            data: {
                code: ctrl.widget.text(),
                languages: [{
                    text: 'HTML/XML',
                    value: 'markup'
                },
                {
                    text: 'JavaScript',
                    value: 'javascript'
                },
                {
                    text: 'CSS',
                    value: 'css'
                }]
            }
        })
        .then(function(value){
            ctrl.widget.setModelProperty('text', value.code);
        });
    };
    editor.prototype.isHidden = function(){};
    
//  the editor type
    return editor;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Factories
 * @name WidgetEditorFake
 * @description Editor of a widget
 * 
 */

.factory('WidgetEditorDeprecated', function ($window, WidgetEditor) {

    /**
     * TODO: maso, 2019: extends WidgetEditorFake
     * 
     * Creates new instace of an editor
     */
    function Editor(widget, options) {
        options = options || {};
        WidgetEditor.apply(this, [widget, options]);
    }
    Editor.prototype = Object.create(WidgetEditor.prototype);


    Editor.prototype.setActive = function(){}; 
    Editor.prototype.isActive = function(){};
    Editor.prototype.save = function(){};
    Editor.prototype.hide = function(){};
    Editor.prototype.show = function(){
        $window.alert('This widget type is deprecated. This will be removed in the next major version.');
    };
    Editor.prototype.isHidden = function(){};

//  the editor type
    return Editor;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Factories
 * @name WidgetEditorFake
 * @description Editor of a widget
 * 
 */



.factory('WidgetEditorFake', function () {

    /**
     * TODO: maso, 2019: extends WidgetEditorFake
     * 
     * Creates new instace of an editor
     */
    function editor() {}
    
    editor.prototype.destroy = function(){};
    editor.prototype.fire = function(){}; // internal
    editor.prototype.setActive = function(){}; // focus|skipFocuse
    editor.prototype.isActive = function(){};
    editor.prototype.getWidget = function(){};
    editor.prototype.setDirty = function(){};
    editor.prototype.isDirty = function(){};
    editor.prototype.save = function(){};
    editor.prototype.hide = function(){};
    editor.prototype.show = function(){};
    editor.prototype.isHidden = function(){};
    editor.prototype.Off = function(){};
    editor.prototype.On = function(){};

    // the editor type
    return editor;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Editor
 * @name WidgetEditorTinymceSection
 * @description Editor of a section
 * 
 *  Section is list of html widgets such as a, p, pre, and h. This editor allow
 * you to edit a section as a simple text. All entered text converted into a 
 * common widgets and stored into the section.
 */

.factory('WidgetEditorTinymceSection', function ($widget, WidgetEditor) {

	/**
	 * TODO: maso, 2019: extends WidgetEditorFake
	 * 
	 * Creates new instace of an editor
	 */
	function Editor(widget, options) {
		options = options || {};
		WidgetEditor.apply(this, [widget, options]);
	}

	Editor.prototype = new WidgetEditor();

	/**
	 * remove all resources
	 * 
	 * @memberof WidgetEditorTinymce
	 */
	Editor.prototype.destroy = function () {
		WidgetEditor.prototype.destroy.call(this);
		this.hide();
	};

	/**
	 * Remove editor
	 */
	Editor.prototype.hide = function () {
		// remove all tinymce editor
		for (var i = tinymce.editors.length - 1 ; i > -1 ; i--) {
			var ed_id = tinymce.editors[i].id;
			tinyMCE.execCommand('mceRemoveEditor', true, ed_id);
		}

		// set hidern
		if (this.isHidden()) {
			return;
		}
		this._hide = true;

		// TODO: fire state changed
	};

	/**
	 * Run and display editor for the current widget
	 */
	Editor.prototype.show = function () {
		this._hide = false;
		var ctrl = this;
		var widget = this.getWidget();
		var element = widget.getElement();
		var selectorPath = element.getPath();
		tinymce.init(_.merge(this.options, {
			selector : selectorPath,
			themes : 'modern',
			setup: function (editor) {

				// Save button to save and close the editor
				editor.ui.registry.addButton('save', {
//					text: 'save',
					icon: 'save',
					tooltip: 'Save current changes and close the editor',
					onAction: function() {
						ctrl.saveAndClose();
					}
				});
				// close button
				editor.ui.registry.addButton('close', {
//					text: 'close',
					icon: 'close',
					tooltip: 'Close and discards changes',
					onAction: function() {
						ctrl.closeWithoutSave();
					}
				});

//				editor.on('focusout', function(){
//				ctrl.closeWithoutSave();
//				});

				editor.on('keydown', function(e) {
					if (e.keyCode === 27) { // escape
						ctrl.closeWithoutSave();
						return false;
					}
				});

				editor.on('KeyDown KeyUp KeyPress Paste Copy', function(event){
					event.stopPropagation();
					editor.save();
				});

				// Update model when:
				// - a button has been clicked [ExecCommand]
				// - the editor content has been modified [change]
				// - the node has changed [NodeChange]
				// - an object has been resized (table, image) [ObjectResized]
				editor.on('ExecCommand change NodeChange ObjectResized', function() {
					editor.save();
					ctrl.updateView(editor);
				});
			}
		}))
		.then(function () {
			element.focus();
		});
	};

	Editor.prototype.isHidden = function () {
		return this._hide;
	};

	/**
	 * Read value from element and set into the element
	 */
	Editor.prototype.updateView = function (editor) {
		var content = editor.getContent({
			format : this.options.format || 'html'
		}).trim();
		this._content = content;
		this.setDirty(true);
	};


	Editor.prototype.closeWithoutSave = function(){
		this.setDirty(false);
		this.hide();
		// reset old value
		var widget = this.widget;
		widget.loadWidgets();
	};

	Editor.prototype.saveAndClose = function(){
		this.hide();
		if(this.isDirty()){
			var widget = this.widget;
			var converter = $widget.getConverter('text/html');
			var widgets = converter.decode(this._content);
			widget.removeChildren();
			widget.getElement().empty();
			widget.addChildrenModel(0, widgets);
			this.setDirty(false);
		}
	};

//	the editor type
	return Editor;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Widget Editor
 * @name WidgetEditorTinymceSingleLine
 * @description A single line text editor
 * 
 *  In a single line editor you are allowed to set a text property into a widget. For
 * example you can change text of a header. Some key and actions are reserved:
 * 
 * - Enter: save and close the editor
 * - ESC: close editor without save
 * 
 * 
 */

.factory('WidgetEditorTinymceSingleLine', function ($sce, WidgetEditor) {

	/**
	 * TODO: maso, 2019: extends WidgetEditorFake
	 * 
	 * Creates new instace of an editor
	 */
	function Editor(widget, options) {
		options = options || {};
		WidgetEditor.apply(this, [widget, options]);
	}

	Editor.prototype = new WidgetEditor();

	/**
	 * remove all resources
	 * 
	 * @memberof WidgetEditorTinymceSingleLine
	 */
	Editor.prototype.destroy = function () {
		this.hide();
		WidgetEditor.prototype.destroy.call(this);
	};

	/**
	 * Remove editor
	 * 
	 * @memberof WidgetEditorTinymceSingleLine
	 */
	Editor.prototype.hide = function () {
		// remove all tinymce editor
		for (var i = tinymce.editors.length - 1 ; i > -1 ; i--) {
			var ed_id = tinymce.editors[i].id;
			tinyMCE.execCommand('mceRemoveEditor', true, ed_id);
		}

		// check current editor
		if (this.isHidden()) {
			return;
		}
		this._hide = true;
		// TODO: fire state changed
		if(this.tinyEditor){
			this.tinyEditor.remove();
			delete this.tinyEditor;
		}
	};

	/**
	 * Run and display editor for the current widget
	 * 
	 * @memberof WidgetEditorTinymceSingleLine
	 */
	Editor.prototype.show = function () {
		this._hide = false;
		var ctrl = this;
		var widget = this.getWidget();
		var element = widget.getElement();
		tinymce.init(_.merge(this.options, {
			selector : element.getPath(),
			themes : 'modern',
			setup: function (editor) {
				ctrl.tinyEditor = editor;
				editor.on('keydown', function(e) {
					if (e.keyCode === 27) { // escape
						ctrl.closeWithoutSave();
						return false;
					}
					if (e.keyCode === 13){
						ctrl.saveAndClose();
						return false;
					}
				});

//				editor.on('focusout', function(){
//				ctrl.closeWithoutSave();
//				});

				editor.on('KeyDown KeyUp KeyPress Paste Copy', function(event){
					event.stopPropagation();
					editor.save();
				});

				// Update model when:
				// - a button has been clicked [ExecCommand]
				// - the editor content has been modified [change]
				// - the node has changed [NodeChange]
				// - an object has been resized (table, image) [ObjectResized]
				editor.on('ExecCommand change NodeChange ObjectResized', function() {
					editor.save();
					ctrl.updateView(editor);
				});

				//
				// Adding custom actions
				//
				// Save button to save and close the editor
				editor.ui.registry.addButton('save', {
//					text: 'save',
					icon: 'save',
					tooltip: 'Save current changes and close the editor',
					onAction: function() {
						ctrl.saveAndClose();
					}
				});
				// close button
				editor.ui.registry.addButton('close', {
//					text: 'close',
					icon: 'close',
					tooltip: 'Close and discards changes',
					onAction: function() {
						ctrl.closeWithoutSave();
					}
				});

//				alignleft aligncenter alignjustify alignright alignfull
				// style.textAlign: left, center, right, justify;
				_.forEach(['widgetalignleft', 'widgetaligncenter', 'widgetalignjustify', 'widgetalignright'], function(action){
					editor.ui.registry.addButton(action, {
						icon: 'align-' + action.substring(11),
						onAction: function() {
							ctrl.widget.setModelProperty('style.textAlign', action.substring(11));
						}
					});
				});
			}
		}))
		.then(function () {
			element.focus();
		});
	};

	/**
	 * Checks if the editor is hiden
	 * 
	 * @memberof WidgetEditorTinymceSingleLine
	 */
	Editor.prototype.isHidden = function () {
		return this._hide;
	};

	/**
	 * Read value from element and set into the element
	 * 
	 * @memberof WidgetEditorTinymceSingleLine
	 */
	Editor.prototype.updateView = function (editor) {
		var content = editor.getContent({
			format : this.options.format || 'html'
		}).trim();
		this._content = content;
		this.setDirty(true);
	};

	/**
	 * Close editor and discards changes
	 * 
	 * @memberof WidgetEditorTinymceSingleLine
	 */
	Editor.prototype.closeWithoutSave = function(){
		this.setDirty(false);
		this.hide();
		// reset old value
		var widget = this.widget;
		widget.fire('modelUpdated', {
			key: this.options.property,
			oldValue: '',
			value: this.widget.getModelProperty(this.options.property)
		});
	};

	/**
	 * Save and close the editor
	 * 
	 * @memberof WidgetEditorTinymceSingleLine
	 */
	Editor.prototype.saveAndClose = function(){
		// remove editor
		if(this.isDirty()){
			this.widget.setModelProperty(this.options.property, this._content);
		}
		this.hide();
	};

//	the editor type
	return Editor;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')
.factory('WbDialogWindow', function($wbWindow, $document, $wbFloat) {
    


    // Utils
    function covertToFloadConfig(dialogWindow) {
        var options = {
                closeOnEscape: dialogWindow.closeOnEscape,
                header: dialogWindow.isTitleVisible(),
                headerTitle: dialogWindow.getTitle(),
                headerLogo: '',
                headerControls: {
//                  close: 'remove',
//                  maximize: 'remove',
//                  normalize: 'remove',
//                  minimize: 'remove',
//                  smallify: 'remove',
//                  smallifyrev: 'remove',
                }
        };

        if(angular.isDefined(dialogWindow.x)){
            options.position = {
                    type: 'fixed',
                    my: 'left-top',
                    at: 'left-top',
                    of: 'body',
                    container: 'body',
                    offsetX: dialogWindow.x,
                    offsetY: dialogWindow.y
            };
        }
        if(angular.isDefined(dialogWindow.width)){
            options.panelSize = {
                    width: dialogWindow.width, 
                    height: dialogWindow.width
            };
        }

        return options;
    }

    /**
     * @ngdoc Factory
     * @name WbDialogWindow
     * @description WbDialogWindow a dialog manager
     * 
     */
    var wbWindow = function(parent){
        this.parent = parent || $wbWindow;
        this.floatDialogElement = null;
        this.setTitleVisible(true);
    };

    /**
     * Gets parent of the window
     * 
     * @memberof WbDialogWindow
     */
    wbWindow.prototype.getParent = function(){
        return this.parent;
    };

    /**
     * Sets title of the window
     * 
     * @memberof WbDialogWindow
     * @params title {string} the window title
     */
    wbWindow.prototype.setTitle = function(title){
        this.title = title;
        if(this.isVisible()){
            // TODO: maso, 2019: set title of the current dialog
        }
    };

    /**
     * Sets title of the window
     * 
     * @memberof WbDialogWindow
     * @return {string} the window title
     */
    wbWindow.prototype.getTitle = function(){
        return this.title;
    };


    /**
     * Sets language of the window
     * 
     * @memberof WbDialogWindow
     * @params language {string} the window language
     */
    wbWindow.prototype.setLanguage = function(language){
        this.language = language;
        if(this.isVisible()){
            // TODO: maso, 2019: set title of the current dialog
        }
    };

    /**
     * Sets title of the window
     * 
     * @memberof WbDialogWindow
     * @return {string} the window language
     */
    wbWindow.prototype.getLanguage = function(){
        return this.language;
    };

    /**
     * 
     * The open() method opens a new browser window, or a new tab, depending 
     * on your browser settings.
     * 
     * Tip: Use the close() method to close the window.
     * 
     * @memberof WbDialogWindow
     * @return window object
     */
    wbWindow.prototype.open = function(url, name, options, replace){
        return $wbWindow.open(url, name, options, replace);
    };

    /**
     * Close current window
     * 
     * 
     * @memberof WbDialogWindow
     * @params visible {boolean} of the window
     */
    wbWindow.prototype.close = function(){
        this.setVisible(false);
        // TODO: maso, 2019: remove dome and destroy scope.
    };

    /**
     * Sets visible of the window
     * 
     * 
     * @memberof WbDialogWindow
     * @params visible {boolean} of the window
     */
    wbWindow.prototype.setVisible = function(visible){
        if(!this.floatDialogElement) {
            this.floatDialogElement = $wbFloat.create(covertToFloadConfig(this));
        } else if(this.floatDialogElement.isVisible() === visible) {
            return;
        }

        this.floatDialogElement.setVisible(visible);
    };

    /**
     * Gets visible of the window
     * 
     * 
     * @memberof WbDialogWindow
     * @returns true if the window is visible
     */
    wbWindow.prototype.isVisible = function(){
        if(! this.floatDialogElement){
            return false;
        }
        return this.floatDialogElement.isVisible();
    };

    /**
     * Sets position of the window
     * 
     * 
     * @memberof WbDialogWindow
     * @params x {string|int} absolute position
     * @params y {string|int} absolute position
     */
    wbWindow.prototype.setPosition = function(x, y) {
        this.x = x;
        this.y = y;
        if(this.floatDialogElement){
            // TODO: reload the window position
        }
    };

    /**
     * Gets current position of the window
     * 
     * @memberof WbDialogWindow
     * @return position
     */
    wbWindow.prototype.getPosition = function() {
        return {
            x: this.x,
            y:this.y,
        };
    };



    /**
     * Close window on Escape
     * 
     * @memberof WbDialogWindow
     * @params x {string|int} absolute position
     * @params y {string|int} absolute position
     */
    wbWindow.prototype.setCloseOnEscape = function(closeOnEscape) {
        this.closeOnEscape = closeOnEscape;
        if(this.floatDialogElement){
            // TODO: reload the window close
        }
    };

    /**
     * Sets size of the window
     * 
     * @memberof WbDialogWindow
     * @params width {string|int} absolute position
     * @params height {string|int} absolute position
     */
    wbWindow.prototype.setSize = function(width, height) {
        this.width = width;
        this.height = height;
        if(this.floatDialogElement){
            // TODO: reload the window size
        }
    };

    /**
     * Loads a library
     * 
     * @memberof WbDialogWindow
     * @path path of library
     * @return promise to load the library
     */
    wbWindow.prototype.loadLibrary = function(path){
        return $wbWindow.loadLibrary(path);
    };

    /**
     * Check if the library is loaded
     * 
     * @memberof WbDialogWindow
     * @return true if the library is loaded
     */
    wbWindow.prototype.isLibraryLoaded = function(path){
        return $wbWindow.isLibraryLoaded(path);
    };

    /**
     * Loads a library
     * 
     * @memberof WbDialogWindow
     * @path path of library
     * @return promise to load the library
     */
    wbWindow.prototype.loadStyle = function(path){
        return $wbWindow.loadStyle(path);
    };

    /**
     * Check if the library is loaded
     * 
     * @memberof WbDialogWindow
     * @return true if the library is loaded
     */
    wbWindow.prototype.isStyleLoaded = function(path){
        return $wbWindow.isStyleLoaded(path);
    };


    /**
     * Set meta
     * 
     * @memberof WbDialogWindow
     * @params key {string} the key of meta
     * @params value {string} the value of meta
     */
    wbWindow.prototype.setMeta = function (key, value){
        var parent = this.getParent();
        if(parent) {
            parent.setMeta(key, value);
        }
    };

    /**
     * Set link
     * 
     * @memberof WbDialogWindow
     * @params key {string} the key of link
     * @params data {string} the value of link
     */
    wbWindow.prototype.setLink = function (key, data){
        var parent = this.getParent();
        if(parent) {
            parent.setLink(key, data);
        }
    };


    /**
     * Write the body
     * 
     * @memberof WbDialogWindow
     * @params data {string} the value
     */
    wbWindow.prototype.write = function (data){
        this.floatDialogElement.getElement()
        .then(function(parentElement){
            // string
            var element = angular.element(data);
            parentElement.empty();
            parentElement.append(element);
        });
    };

    /**
     * Set view the body
     * 
     * @memberof WbDialogWindow
     * @params data {Object} the view
     */
    wbWindow.prototype.setView = function (view){
        return this.floatDialogElement.setView(view);
    };

    wbWindow.prototype.setWidth = function(width){
        this.resizeTo(width, this.getHeight());
    };

    wbWindow.prototype.getWidth = function(){
        return this.width;
    };

    wbWindow.prototype.setHeight = function(height){
        this.resizeTo(this.getWidth(), height);
    };

    wbWindow.prototype.getHeight = function(){
        return this.height;
    };

    wbWindow.prototype.resizeTo = function(width, height) {
        this.width = width;
        this.height = height;
        if(this.floatDialogElement){
            this.floatDialogElement.resize(width, height);
        }
    };

    wbWindow.prototype.setTitleVisible = function(visible){
        this._titleVisible = visible;
        if(this.floatDialogElement){
            // TODO: maso, 2019: Check if the JPanel supports title visibility online.
        }
    };

    wbWindow.prototype.isTitleVisible = function(){
        return this._titleVisible;
    };

    return wbWindow;
});

///* 
// * The MIT License (MIT)
// * 
// * Copyright (c) 2016 weburger
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//
//angular.module('am-wb-core')//
//
///**
// * @ngdoc Factories
// * @name TinymcePluginCodesample
// * @description Adding code sample to tinymce
// * 
// * 
// * ## options
// * 
// * codesample_languages: array of languages
// * 
// */
//.factory('TinymcePluginCodesample', function ($resource) {
//	
//	var languages;
//	var defaultLanguages = [{
//		text: 'HTML/XML',
//		value: 'markup'
//	},
//	{
//		text: 'JavaScript',
//		value: 'javascript'
//	},
//	{
//		text: 'CSS',
//		value: 'css'
//	},
//	{
//		text: 'PHP',
//		value: 'php'
//	},
//	{
//		text: 'Ruby',
//		value: 'ruby'
//	},
//	{
//		text: 'Python',
//		value: 'python'
//	},
//	{
//		text: 'Java',
//		value: 'java'
//	},
//	{
//		text: 'C',
//		value: 'c'
//	},
//	{
//		text: 'C#',
//		value: 'csharp'
//	},
//	{
//		text: 'C++',
//		value: 'cpp'
//	}];
//
//	/*
//	 * dom utils
//	 */
//	var tinymceDomeUtils = tinymce.util.Tools.resolve('tinymce.dom.DOMUtils');
//
//	function isCodeSample(elm) {
//		return elm && elm.nodeName === 'PRE' && elm.className.indexOf('language-') !== -1;
//	}
//
//	function trimArg(predicateFn) {
//		return function (arg1, arg2) {
//			return predicateFn(arg2);
//		};
//	}
//
//	/*
//	 * Insert new code sample into the cell
//	 */
//	function insertCodeSample(editor, language, code, node) {
//		editor.undoManager.transact(function () {
////			var node = getSelectedCodeSample(editor);
//			code = tinymceDomeUtils.DOM.encode(code);
//			if (node) {
//				editor.dom.setAttrib(node, 'class', 'language-' + language);
//				node.innerHTML = code;
//				Prism.highlightElement(node);
//				editor.selection.select(node);
//			} else {
//				editor.insertContent('<pre id="__new" class="language-' + language + '">' + code + '</pre>');
//				editor.selection.select(editor.$('#__new').removeAttr('id')[0]);
//			}
//		});
//	}
//
//	/*
//	 * Add to plugin manager
//	 */
//
//	var tinymcePluginCodesample = function (editor, pluginUrl) {
//		this.setEditor(editor, pluginUrl);
//	};
//
//	/**
//	 * Set editor and load the plugin
//	 */
//	tinymcePluginCodesample.prototype.setEditor = function(editor) {
//		this._editor = editor;
//		languages = editor.settings.codesample_languages || defaultLanguages;
//		this.setup();
//		this.register();
//	};
//
//	/**
//	 * Gets current editor
//	 */
//	tinymcePluginCodesample.prototype.getEditor = function(){
//		return this._editor;
//	};
//
//
//
//	/**
//	 * Setups the environments and events
//	 */
//	tinymcePluginCodesample.prototype.setup = function () {
//		var facotry = this;
//		var editor = this.getEditor();
//		var $ = editor.$;
//		editor.on('PreProcess', function (e) {
//			$('pre[contenteditable=false]', e.node).filter(trimArg(isCodeSample)).each(function (idx, elm) {
//				var $elm = $(elm), code = elm.textContent;
//				$elm.attr('class', $.trim($elm.attr('class')));
//				$elm.removeAttr('contentEditable');
//				$elm.empty().append($('<code></code>').each(function () {
//					this.textContent = code;
//				}));
//			});
//		});
//		editor.on('SetContent', function () {
//			var unprocessedCodeSamples = $('pre').filter(trimArg(isCodeSample)).filter(function (idx, elm) {
//				return elm.contentEditable !== 'false';
//			});
//			if (unprocessedCodeSamples.length) {
//				editor.undoManager.transact(function () {
//					unprocessedCodeSamples.each(function (idx, elm) {
//						$(elm).find('br').each(function (idx, elm) {
//							elm.parentNode.replaceChild(editor.getDoc().createTextNode('\n'), elm);
//						});
//						elm.contentEditable = false;
//						elm.innerHTML = editor.dom.encode(elm.textContent);
//						Prism.highlightElement(elm);
//						elm.className = $.trim(elm.className);
//					});
//				});
//			}
//		});
//
//		editor.on('dblclick', function (ev) {
//			if (isCodeSample(ev.target)) {
//				facotry.openEditor();
//			}
//		});
//	};
//
//	/**
//	 * Register the plugin with the editor
//	 * 
//	 */
//	tinymcePluginCodesample.prototype.register = function () {
//		var facotry = this;
//		var editor = this.getEditor();
//		editor.addCommand('codesample', function () {
//			var node = editor.selection.getNode();
//			if (editor.selection.isCollapsed() || isCodeSample(node)) {
//				facotry.openEditor();
//			} else {
//				editor.formatter.toggle('code');
//			}
//		});
//		editor.addButton('codesample', {
//			cmd: 'codesample',
//			title: 'Insert/Edit code sample'
//		});
//		editor.addMenuItem('codesample', {
//			cmd: 'codesample',
//			text: 'Code sample',
//			icon: 'codesample'
//		});
//	};
//
//	/*
//	 * Get selected code sample from the editor
//	 */
//	tinymcePluginCodesample.prototype.getSelectedCodeSample = function () {
//		var editor = this.getEditor();
//		var node = editor.selection.getNode();
//		if (isCodeSample(node)) {
//			return node;
//		}
//		return null;
//	};
//
//	/*
//	 * Get current code.
//	 * 
//	 * If the code sample is empty then an empty text is returned as
//	 * result.
//	 */
//	tinymcePluginCodesample.prototype.getCurrentCode = function () {
//		var node = this.getSelectedCodeSample();
//		if (node) {
//			return node.textContent;
//		}
//		return '';
//	};
//
//
//	/*
//	 * Gets current language of the code sampler
//	 */
//	tinymcePluginCodesample.prototype.getCurrentLanguage = function (editor) {
//		var matches;
//		var node = this.getSelectedCodeSample();
//		if (node) {
//			matches = node.className.match(/language-(\w+)/);
//			return matches ? matches[1] : '';
//		}
//		return '';
//	};
//
//	/*
//	 * Open editor to edit a code sample
//	 */
//	tinymcePluginCodesample.prototype.openEditor = function () { 
//		var editor = this.getEditor();
//		var node = this.getSelectedCodeSample();
//		var factory = this;
//		$resource.get('script', {
//			data : {
//				language: this.getCurrentLanguage(editor),
//				languages: languages,
//				// TODO: maso, 2019: get code
//				code: this.getCurrentCode()
//			}
//		})
//		.then(function(script){
//			insertCodeSample(editor, script.language, script.code, node);
//		});
//	};
//
//
//	return tinymcePluginCodesample;
//});
//
//
//
//
//
//
//
//
//
//
//
//

///* 
// * The MIT License (MIT)
// * 
// * Copyright (c) 2016 weburger
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//angular.module('am-wb-core')//
//
///**
// * @ngdoc Factories
// * @name TinymcePluginImageToolxcx
// * @description Adding image plugin
// * 
// */
//.factory('TinymcePluginImageTool', function ($resource) {
//	
//
//	var tinymcePluginImageTool = function (editor/*, pluginUrl*/) {
//		var factory = this;
//		this.setEditor(editor);
//		editor.addButton('image', {
//			icon: 'image',
//			tooltip: 'Insert/edit image',
//			onclick: function(url){
//				editor.insertContent('<img src="' + url + '" >');
//			},
//			stateSelector: 'img:not([data-mce-object],[data-mce-placeholder]),figure.image'
//		});
//
//		editor.addMenuItem('image', {
//			icon: 'image',
//			text: 'Image',
//			onclick: function(){
//				factory.insertImage();
//			},
//			context: 'insert',
//			prependToContext: true
//		});
//
//		editor.addCommand('mceImage', function(){
//			factory.insertImage();
//		});
//	}
//
//	tinymcePluginImageTool.prototype.insertImage = function() {
//		var editor = this.getEditor();
//		$resource.get('image')//
//		.then(function(value){
//			editor.insertContent('<img src="' + value + '" >');
//		});
//	};
//	
//	tinymcePluginImageTool.prototype.setEditor = function(editor) {
//		this._editor = editor;
//	};
//	
//	tinymcePluginImageTool.prototype.getEditor = function() {
//		return this._editor;
//	};
//
//	return tinymcePluginImageTool;
//});
//
//
//
//
//
//
//
//
//
//
//
//

///* 
// * The MIT License (MIT)
// * 
// * Copyright (c) 2016 weburger
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//angular.module('am-wb-core')//
//
///**
// * @ngdoc Factories
// * @name TinymcePluginLink
// * @description Adding image plugin
// * 
// * 
// * ## Options
// * 
// * link_assume_external_targets: 
// * link_context_toolbar:
// * link_list:
// * default_link_target: 
// */
//.factory('TinymcePluginLink', function ($resource) {
//	
//	
//
//	var attachState = {};
//
//
//	var global$1 = tinymce.util.Tools.resolve('tinymce.util.VK');
//	var global$2 = tinymce.util.Tools.resolve('tinymce.dom.DOMUtils');
//	var global$3 = tinymce.util.Tools.resolve('tinymce.Env');
//	var global$4 = tinymce.util.Tools.resolve('tinymce.util.Tools');
//	var global$5 = tinymce.util.Tools.resolve('tinymce.util.Delay');
//	var global$6 = tinymce.util.Tools.resolve('tinymce.util.XHR');
//
//
//
//	//-----------------------------------------------------------
//	// utility
//	//-----------------------------------------------------------
//	var assumeExternalTargets = function (editorSettings) {
//		return typeof editorSettings.link_assume_external_targets === 'boolean' ? editorSettings.link_assume_external_targets : false;
//	};
//
//	var hasContextToolbar = function (editorSettings) {
//		return typeof editorSettings.link_context_toolbar === 'boolean' ? editorSettings.link_context_toolbar : false;
//	};
//
//	var getLinkList = function (editorSettings) {
//		return editorSettings.link_list;
//	};
//
//	function hasDefaultLinkTarget(editorSettings) {
//		return typeof editorSettings.default_link_target === 'string';
//	}
//
//	function getDefaultLinkTarget(editorSettings) {
//		return editorSettings.default_link_target;
//	};
//
//	var getTargetList = function (editorSettings) {
//		return editorSettings.target_list;
//	};
//
//	var setTargetList = function (editor, list) {
//		editor.settings.target_list = list;
//	};
//
//	var shouldShowTargetList = function (editorSettings) {
//		return getTargetList(editorSettings) !== false;
//	};
//
//	var getRelList = function (editorSettings) {
//		return editorSettings.rel_list;
//	};
//
//	var hasRelList = function (editorSettings) {
//		return getRelList(editorSettings) !== undefined;
//	};
//
//	var getLinkClassList = function (editorSettings) {
//		return editorSettings.link_class_list;
//	};
//
//	var hasLinkClassList = function (editorSettings) {
//		return getLinkClassList(editorSettings) !== undefined;
//	};
//
//	var shouldShowLinkTitle = function (editorSettings) {
//		return editorSettings.link_title !== false;
//	};
//
//	var allowUnsafeLinkTarget = function (editorSettings) {
//		return typeof editorSettings.allow_unsafe_link_target === 'boolean' ? editorSettings.allow_unsafe_link_target : false;
//	};
//
//	var isImageFigure = function (node) {
//		return node && node.nodeName === 'FIGURE' && /\bimage\b/i.test(node.className);
//	};
//
//	var appendClickRemove = function (link, evt) {
//		document.body.appendChild(link);
//		link.dispatchEvent(evt);
//		document.body.removeChild(link);
//	};
//
//	var isLink = function (elm) {
//		return elm && elm.nodeName === 'A' && elm.href;
//	};
//	
//	var hasLinks = function (elements) {
//		return global$4.grep(elements, isLink).length > 0;
//	};
//	
//	var getLink = function (editor, elm) {
//		return editor.dom.getParent(elm, 'a[href]');
//	};
//
//	var getSelectedLink = function (editor) {
//		return getLink(editor, editor.selection.getStart());
//	};
//	
//	var getHref = function (elm) {
//		var href = elm.getAttribute('data-mce-href');
//		return href ? href : elm.getAttribute('href');
//	};
//	
//	var isContextMenuVisible = function (editor) {
//		var contextmenu = editor.plugins.contextmenu;
//		return contextmenu ? contextmenu.isContextMenuVisible() : false;
//	};
//	
//	var hasOnlyAltModifier = function (e) {
//		return e.altKey === true && e.shiftKey === false && e.ctrlKey === false && e.metaKey === false;
//	};
//	
//	var trimCaretContainers = function (text) {
//		return text.replace(/\uFEFF/g, '');
//	};
//
//
//	function getAnchorElement(editor, selectedElm) {
//		selectedElm = selectedElm || editor.selection.getNode();
//		if (isImageFigure(selectedElm)) {
//			return editor.dom.select('a[href]', selectedElm)[0];
//		} else {
//			return editor.dom.getParent(selectedElm, 'a[href]');
//		}
//	}
//	
//	function getAnchorText(selection, anchorElm) {
//		var text = anchorElm ? anchorElm.innerText || anchorElm.textContent : selection.getContent({ format: 'text' });
//		return trimCaretContainers(text);
//	}
//	
//	function isOnlyTextSelected(html) {
//		if (/</.test(html) && (!/^<a [^>]+>[^<]+<\/a>$/.test(html) || html.indexOf('href=') === -1)) {
//			return false;
//		}
//		return true;
//	}
//	
//	function buildListItems(inputList, itemCallback, startItems) {
//		var appendItems = function (values, output) {
//			output = output || [];
//			angular.forEach(values, function (item) {
//				var menuItem = { 
//						text: item.text || item.title 
//				};
//				if (item.menu) {
//					menuItem.menu = appendItems(item.menu);
//				} else {
//					menuItem.value = item.value;
//					if (itemCallback) {
//						itemCallback(menuItem);
//					}
//				}
//				output.push(menuItem);
//			});
//			return output;
//		};
//		return appendItems(inputList, startItems || []);
//	};
//	
//
//	var toggleTargetRules = function (rel, isUnsafe) {
//		var rules = ['noopener'];
//		var newRel = rel ? rel.split(/\s+/) : [];
//		var toString = function (rel) {
//			return global$4.trim(rel.sort().join(' '));
//		};
//		var addTargetRules = function (rel) {
//			rel = removeTargetRules(rel);
//			return rel.length ? rel.concat(rules) : rules;
//		};
//		var removeTargetRules = function (rel) {
//			return rel.filter(function (val) {
//				return global$4.inArray(rules, val) === -1;
//			});
//		};
//		newRel = isUnsafe ? addTargetRules(newRel) : removeTargetRules(newRel);
//		return newRel.length ? toString(newRel) : null;
//	};
//
//	var link = function (editor, attachState) {
//		return function (data) {
//			editor.undoManager.transact(function () {
//				var selectedElm = editor.selection.getNode();
//				var anchorElm = getAnchorElement(editor, selectedElm);
//				var linkAttrs = {
//						href: data.href,
//						target: data.target ? data.target : null,
//								rel: data.rel ? data.rel : null,
//										class: data.class ? data.class : null,
//												title: data.title ? data.title : null
//				};
//				if (!hasRelList(editor.settings) && allowUnsafeLinkTarget(editor.settings) === false) {
//					linkAttrs.rel = toggleTargetRules(linkAttrs.rel, linkAttrs.target === '_blank');
//				}
//				if (data.href === attachState.href) {
//					attachState.attach();
//					attachState = {};
//				}
//				if (anchorElm) {
//					editor.focus();
//					if (data.hasOwnProperty('text')) {
//						if ('innerText' in anchorElm) {
//							anchorElm.innerText = data.text;
//						} else {
//							anchorElm.textContent = data.text;
//						}
//					}
//					editor.dom.setAttribs(anchorElm, linkAttrs);
//					editor.selection.select(anchorElm);
//					editor.undoManager.add();
//				} else {
//					if (isImageFigure(selectedElm)) {
//						linkImageFigure(editor, selectedElm, linkAttrs);
//					} else if (data.hasOwnProperty('text')) {
//						editor.insertContent(editor.dom.createHTML('a', linkAttrs, editor.dom.encode(data.text)));
//					} else {
//						editor.execCommand('mceInsertLink', false, linkAttrs);
//					}
//				}
//			});
//		};
//	};
//	
//	var unlink = function (editor) {
//		return function () {
//			editor.undoManager.transact(function () {
//				var node = editor.selection.getNode();
//				if (isImageFigure(node)) {
//					unlinkImageFigure(editor, node);
//				} else {
//					editor.execCommand('unlink');
//				}
//			});
//		};
//	};
//	
//	var unlinkImageFigure = function (editor, fig) {
//		var a, img;
//		img = editor.dom.select('img', fig)[0];
//		if (img) {
//			a = editor.dom.getParents(img, 'a[href]', fig)[0];
//			if (a) {
//				a.parentNode.insertBefore(img, a);
//				editor.dom.remove(a);
//			}
//		}
//	};
//	var linkImageFigure = function (editor, fig, attrs) {
//		var a, img;
//		img = editor.dom.select('img', fig)[0];
//		if (img) {
//			a = editor.dom.create('a', attrs);
//			img.parentNode.insertBefore(a, img);
//			a.appendChild(img);
//		}
//	};
//
//	var delayedConfirm = function (editor, message, callback) {
//		var rng = editor.selection.getRng();
//		global$5.setEditorTimeout(editor, function () {
//			editor.windowManager.confirm(message, function (state) {
//				editor.selection.setRng(rng);
//				callback(state);
//			});
//		});
//	};
//
//	
//	//-----------------------------------------------------------
//	// Factory
//	//
//	//
//	//-----------------------------------------------------------
//	var tinymcePluginLink = function (editor/*, pluginUrl*/) {
//		this.setEditor(editor);
//
//		this.setupButtons();
//		this.setupMenuItems();
//		this.setupContextToolbars();
//		this.setupGotoLinks();
//		this.register();
//	};
//
//	tinymcePluginLink.prototype.setEditor = function(editor) {
//		this._editor = editor;
//	};
//
//	tinymcePluginLink.prototype.getEditor = function() {
//		return this._editor;
//	};
//
//	tinymcePluginLink.prototype.setupButtons = function () {
//		var editor = this.getEditor();
//		var factory = this;
//		editor.ui.registry.addSplitButton('link', {
//			active: false,
//			icon: 'link',
//			tooltip: 'Insert/edit link',
//			onclick: function(){
//				factory.openDialog();
//			},
//			onpostrender: function(){
//				factory.toggleActiveState();
//			}
//		});
//		editor.ui.registry.addSplitButton('unlink', {
//			active: false,
//			icon: 'unlink',
//			tooltip: 'Remove link',
//			onclick: function(){
//				factory.unlink();
//			},
//			onpostrender: function(){
//				factory.toggleActiveState();
//			}
//		});
//		if (editor.ui.registry.addContextToolbar) {
//			editor.ui.registry.addSplitButton('openlink', {
//				icon: 'newtab',
//				tooltip: 'Open link',
//				onclick: function () {
//					gotoLink(editor, getSelectedLink(editor));
//				}
//			});
//		}
//	};
//
//	tinymcePluginLink.prototype.setupMenuItems = function () {
//		var editor = this.getEditor();
//		var factory = this;
//		editor.ui.registry.addMenuItem('openlink', {
//			text: 'Open link',
//			icon: 'newtab',
//			onclick: function () {
//				factory.gotoLink(editor, getSelectedLink(editor));
//			},
//			onPostRender: function () {
//				var self = this;
//				var toggleVisibility = function (e) {
//					if (hasLinks(e.parents)) {
//						self.show();
//					} else {
//						self.hide();
//					}
//				};
//				if (!hasLinks(editor.dom.getParents(editor.selection.getStart()))) {
//					self.hide();
//				}
//				editor.on('nodechange', toggleVisibility);
//				self.on('remove', function () {
//					editor.off('nodechange', toggleVisibility);
//				});
//			},
//			prependToContext: true
//		});
//		editor.ui.registry.addMenuItem('link', {
//			icon: 'link',
//			text: 'Link',
//			shortcut: 'Meta+K',
//			onclick: function(){
//				factory.openDialog();
//			},
//			stateSelector: 'a[href]',
//			context: 'insert',
//			prependToContext: true
//		});
//		editor.ui.registry.addMenuItem('unlink', {
//			icon: 'unlink',
//			text: 'Remove link',
//			onclick: function(){
//				factory.unlink();
//			},
//			stateSelector: 'a[href]'
//		});
//	};
//
//	tinymcePluginLink.prototype.setupContextToolbars = function () {
//		var editor = this.getEditor();
//		if (editor.ui.registry.addContextToolbar) {
//			editor.ui.registry.addContextToolbar(function (elm) {
//				var sel, rng, node;
//				if (hasContextToolbar(editor.settings) && !isContextMenuVisible(editor) && isLink(elm)) {
//					sel = editor.selection;
//					rng = sel.getRng();
//					node = rng.startContainer;
//					if (node.nodeType === 3 && sel.isCollapsed() && rng.startOffset > 0 && rng.startOffset < node.data.length) {
//						return true;
//					}
//				}
//				return false;
//			}, 'openlink | link unlink');
//		}
//	};
//
//	tinymcePluginLink.prototype.setupGotoLinks = function () {
//		var editor = this.getEditor();
//		editor.on('click', function (e) {
//			var link = getLink(editor, e.target);
//			if (link && global$1.metaKeyPressed(e)) {
//				e.preventDefault();
//				gotoLink(editor, link);
//			}
//		});
//		editor.on('keydown', function (e) {
//			var link = getSelectedLink(editor);
//			if (link && e.keyCode === 13 && hasOnlyAltModifier(e)) {
//				e.preventDefault();
//				gotoLink(editor, link);
//			}
//		});
//	};
//	
//	tinymcePluginLink.prototype.toggleActiveState = function () {
//		var editor = this.getEditor();
//		return function () {
//			var self = this;
//			editor.on('nodechange', function (e) {
//				self.active(!editor.readonly && !!$_ft5004fzjm0o6bvs.getAnchorElement(editor, e.element));
//			});
//		};
//	};
//	
//	tinymcePluginLink.prototype.register = function () {
//		var factory = this;
//		var editor = this.getEditor();
//		// commands
//		editor.addCommand('mceLink', function(){
//			factory.openDialog();
//		});
//		// shortcuts
//		editor.addShortcut('Meta+K', '', function(){
//			factory.openDialog();
//		});
//	};
//	
//
//	tinymcePluginLink.prototype.openDialog = function (editor) {
//		var editor = this.getEditor();
//		var linkList = getLinkList(editor.settings);
//		var factory = this;
//		if (typeof linkList === 'string') {
//			global$6.send({
//				url: linkList,
//				success: function (text) {
//					factory.showDialog(JSON.parse(text));
//				}
//			});
//		} else if (typeof linkList === 'function') {
//			linkList(function (list) {
//				factory.showDialog(list);
//			});
//		} else {
//			factory.showDialog(linkList);
//		}
//	};
//	
//	tinymcePluginLink.prototype.unlink = function () {
//		var editor = this.getEditor();
//		editor.undoManager.transact(function () {
//			var node = editor.selection.getNode();
//			if (isImageFigure(node)) {
//				unlinkImageFigure(editor, node);
//			} else {
//				editor.execCommand('unlink');
//			}
//		});
//	};
//	
//
//	tinymcePluginLink.prototype.showDialog = function (linkList) {
//		var editor = this.getEditor();
//		var data = {};
//		var selection = editor.selection;
//		var dom = editor.dom;
//		var anchorElm, initialText;
//		
//		var win, onlyText, textListCtrl, linkListCtrl, relListCtrl, targetListCtrl, classListCtrl, linkTitleCtrl, value;
//		
//		var linkListChangeHandler = function (e) {
//			var textCtrl = win.find('#text');
//			if (!textCtrl.value() || e.lastControl && textCtrl.value() === e.lastControl.text()) {
//				textCtrl.value(e.control.text());
//			}
//			win.find('#href').value(e.control.value());
//		};
//		
//		var buildAnchorListControl = function (url) {
//			var anchorList = [];
//			global$4.each(editor.dom.select('a:not([href])'), function (anchor) {
//				var id = anchor.name || anchor.id;
//				if (id) {
//					anchorList.push({
//						text: id,
//						value: '#' + id,
//						selected: url.indexOf('#' + id) !== -1
//					});
//				}
//			});
//			if (anchorList.length) {
//				anchorList.unshift({
//					text: 'None',
//					value: ''
//				});
//				return {
//					name: 'anchor',
//					type: 'listbox',
//					label: 'Anchors',
//					values: anchorList,
//					onselect: linkListChangeHandler
//				};
//			}
//		};
//		
//		var updateText = function () {
//			if (!initialText && onlyText && !data.text) {
//				this.parent().parent().find('#text')[0].value(this.value());
//			}
//		};
//		
//		var urlChange = function (e) {
//			var meta = e.meta || {};
//			if (linkListCtrl) {
//				linkListCtrl.value(editor.convertURL(this.value(), 'href'));
//			}
//			global$4.each(e.meta, function (value, key) {
//				var inp = win.find('#' + key);
//				if (key === 'text') {
//					if (initialText.length === 0) {
//						inp.value(value);
//						data.text = value;
//					}
//				} else {
//					inp.value(value);
//				}
//			});
//			if (meta.attach) {
//				attachState = {
//						href: this.value(),
//						attach: meta.attach
//				};
//			}
//			if (!meta.text) {
//				updateText.call(this);
//			}
//		};
//		
//		var onBeforeCall = function (e) {
//			e.meta = win.toJSON();
//		};
//		
//		onlyText = isOnlyTextSelected(selection.getContent());
//		anchorElm = getAnchorElement(editor);
//		data.text = initialText = getAnchorText(editor.selection, anchorElm);
//		data.href = anchorElm ? dom.getAttrib(anchorElm, 'href') : '';
//		if (anchorElm) {
//			data.target = dom.getAttrib(anchorElm, 'target');
//		} else if (hasDefaultLinkTarget(editor.settings)) {
//			data.target = getDefaultLinkTarget(editor.settings);
//		}
//		if (value = dom.getAttrib(anchorElm, 'rel')) {
//			data.rel = value;
//		}
//		if (value = dom.getAttrib(anchorElm, 'class')) {
//			data.class = value;
//		}
//		if (value = dom.getAttrib(anchorElm, 'title')) {
//			data.title = value;
//		}
//		if (onlyText) {
//			textListCtrl = {
//					name: 'text',
//					type: 'textbox',
//					size: 40,
//					label: 'Text to display',
//					onchange: function () {
//						data.text = this.value();
//					}
//			};
//		}
//		if (linkList) {
//			linkListCtrl = {
//					type: 'listbox',
//					label: 'Link list',
//					values: buildListItems(linkList, function (item) {
//						item.value = editor.convertURL(item.value || item.url, 'href');
//					}, [{
//						text: 'None',
//						value: ''
//					}]),
//					onselect: linkListChangeHandler,
//					value: editor.convertURL(data.href, 'href'),
//					onPostRender: function () {
//						linkListCtrl = this;
//					}
//			};
//		}
//		if (shouldShowTargetList(editor.settings)) {
//			if (getTargetList(editor.settings) === undefined) {
//				setTargetList(editor, [{
//						text: 'None',
//						value: ''
//					},{
//						text: 'New window',
//						value: '_blank'
//					}]);
//			}
//			targetListCtrl = {
//					name: 'target',
//					type: 'listbox',
//					label: 'Target',
//					values: buildListItems(getTargetList(editor.settings))
//			};
//		}
//		if (hasRelList(editor.settings)) {
//			relListCtrl = {
//					name: 'rel',
//					type: 'listbox',
//					label: 'Rel',
//					values: buildListItems(getRelList(editor.settings), function (item) {
//						if (allowUnsafeLinkTarget(editor.settings) === false) {
//							item.value = toggleTargetRules(item.value, data.target === '_blank');
//						}
//					})
//			};
//		}
//		if (hasLinkClassList(editor.settings)) {
//			classListCtrl = {
//					name: 'class',
//					type: 'listbox',
//					label: 'Class',
//					values: buildListItems(getLinkClassList(editor.settings), function (item) {
//						if (item.value) {
//							item.textStyle = function () {
//								return editor.formatter.getCssText({
//									inline: 'a',
//									classes: [item.value]
//								});
//							};
//						}
//					})
//			};
//		}
//		if (shouldShowLinkTitle(editor.settings)) {
//			linkTitleCtrl = {
//					name: 'title',
//					type: 'textbox',
//					label: 'Title',
//					value: data.title
//			};
//		}
//		win = editor.windowManager.open({
//			title: 'Insert link',
//			data: data,
//			body: [
//				{
//					name: 'href',
//					type: 'filepicker',
//					filetype: 'file',
//					size: 40,
//					autofocus: true,
//					label: 'Url',
//					onchange: urlChange,
//					onkeyup: updateText,
//					onpaste: updateText,
//					onbeforecall: onBeforeCall
//				},
//				textListCtrl,
//				linkTitleCtrl,
//				buildAnchorListControl(data.href),
//				linkListCtrl,
//				relListCtrl,
//				targetListCtrl,
//				classListCtrl
//				],
//				onSubmit: function (e) {
//					var assumeExternalTargetsResult = assumeExternalTargets(editor.settings);
//					var insertLink = link(editor, attachState);
//					var removeLink = unlink(editor);
//					var resultData = global$4.extend({}, data, e.data);
//					var href = resultData.href;
//					if (!href) {
//						removeLink();
//						return;
//					}
//					if (!onlyText || resultData.text === initialText) {
//						delete resultData.text;
//					}
//					if (href.indexOf('@') > 0 && href.indexOf('//') === -1 && href.indexOf('mailto:') === -1) {
//						delayedConfirm(editor, 'The URL you entered seems to be an email address. Do you want to add the required mailto: prefix?', function (state) {
//							if (state) {
//								resultData.href = 'mailto:' + href;
//							}
//							insertLink(resultData);
//						});
//						return;
//					}
//					if (assumeExternalTargetsResult === true && !/^\w+:/i.test(href) || assumeExternalTargetsResult === false && /^\s*www[\.|\d\.]/i.test(href)) {
//						delayedConfirm(editor, 'The URL you entered seems to be an external link. Do you want to add the required http:// prefix?', function (state) {
//							if (state) {
//								resultData.href = 'http://' + href;
//							}
//							insertLink(resultData);
//						});
//						return;
//					}
//					insertLink(resultData);
//				}
//		});
//	};
//	
//
//
//	var gotoLink = function (a) {
//		var editor = this.getEditor();
//		if (a) {
//			var href = getHref(a);
//			if (/^#/.test(href)) {
//				var targetEl = editor.$(href);
//				if (targetEl.length) {
//					editor.selection.scrollIntoView(targetEl[0], true);
//				}
//			} else {
//				var url = a.href;
//				if (!global$3.ie || global$3.ie > 10) {
//					var link = document.createElement('a');
//					link.target = '_blank';
//					link.href = url;
//					link.rel = 'noreferrer noopener';
//					var evt = document.createEvent('MouseEvents');
//					evt.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
//					appendClickRemove(link, evt);
//				} else {
//					var win = window.open('', '_blank');
//					if (win) {
//						win.opener = null;
//						var doc = win.document;
//						doc.open();
//						doc.write('<meta http-equiv="refresh" content="0; url=' + global$2.DOM.encode(url) + '">');
//						doc.close();
//					}
//				}
//			}
//		}
//	};
//
//	return tinymcePluginLink;
//});
//
//
//
//
//
//
//
//
//
//

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')

.factory('AmhWorkbenchContentType', function() {
	

	function Type(key /* types */) {
		this.key = key;
		this.types = arguments;
	};

	Type.prototype.match = function(mimeType) {
		// TODO: maso, 2019: update
		for(var i = 0; i < this.types.length; i++){
			if(mimeType === this.types[i]){
				return true;
			}
		}
		return false;
	};

	return Type;
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')

.factory('AmhWorkbenchJob', function() {
	

	function Job(title, promise) {
		this.title = title;
		this.promise = promise;
	};
	
	Job.prototype.then = function(fn1, fn2) {
		var promise = this.promise.then(fn1, fn2);
		return new Job('', promise);
	};
	
	Job.prototype.finally = function(fn) {
		var promise = this.promise.finally(fn);
		return new Job('', promise);
	};
	
	return Job;
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Factories
 * @name AbstractWidgetLocator
 * @description Locates a widget on the view
 * 
 * It is used to display extra information about a widget on the screen. For
 * example it is used to show widget actions on the fly.
 * 
 */
.factory('AbstractWidgetLocator', function () {

    /**
     * Creates new instance of the widget locator
     * 
     * @memberof AbstractWidgetLocator
     */
    function Locator() {
        this.callbacks = [];
        this.elements = [];
        this.observedWidgets = [];

        // Creates listeners
        var ctrl = this;
        this.widgetListeners = {
                'select' : function (/*$event*/) {
                    ctrl.addClass('selected');
                    ctrl.removeClass('mouseover');
                },
                'unselect' : function (/*$event*/) {
                    ctrl.removeClass('selected');
                    if (ctrl.mouseover) {
                        ctrl.addClass('mouseover');
                    }
                },
                'mouseover' : function (/*$event*/) {
                    ctrl.addClass('mouseover');
                    ctrl.mouseover = true;
                },
                'mouseout' : function (/*$event*/) {
                    ctrl.removeClass('mouseover');
                    ctrl.mouseover = false;
                },
        };
    }

    /**
     * Defines anchor 
     */
    Locator.prototype.setAnchor = function (anchor) {
        this.anchor = anchor;
    };

    /**
     * Update the view
     */
    Locator.prototype.setAnchor = function (anchor) {
        this.anchor = anchor;
    };

    Locator.prototype.getAnchor = function (/*auncher*/) {
        // find custom anchor
//        if(this.anchor){
//            if(angular.isFunction(this.anchor)){
//                return this.anchor();
//            }
//            if(angular.isString(this.anchor)){
//                var list = $rootElement.find(this.anchor);
//                if(list){
//                    return list[0];
//                }
//            }
//        }
//        // find parent
        var widget = this.getWidget();
//        if(widget && widget.getParent()){
//            return widget.getParent().getElement();
//        }
        // return root
//        return $rootElement;
      return widget.getRoot().getElement();
    };


    /**
     * Sets new widget
     */
    Locator.prototype.setWidget = function (widget) {
        this.widget = widget;
    };

    Locator.prototype.getWidget = function () {
        return this.widget;
    };

    Locator.prototype.setElements = function (elements) {
        this.elements = elements;
    };

    Locator.prototype.getElements = function () {
        return this.elements;
    };

    Locator.prototype.addClass = function (value) {
        var elements = this.getElements();
        for (var i = 0; i < elements.length; i++) {
            elements[i].addClass(value);
        }
    };

    Locator.prototype.removeClass = function (value) {
        var elements = this.getElements();
        for (var i = 0; i < elements.length; i++) {
            elements[i].removeClass(value);
        }
    };

    /**
     * Remove connection the the current widget
     */
    Locator.prototype.disconnect = function () {
        this.connect(null);
        this.connected = false;
    };

    Locator.prototype.connect = function (widget) {
        this.connected = true;
        if (this.widget !==  widget) {
            var elements = this.getElements();
            if(this.widget){
                var oldWidget = this.widget;
                angular.forEach(this.widgetListeners, function (listener, type) {
                    oldWidget.off(type, listener);
                });
                for (var i = 0; i < elements.length; i++) {
                    elements[i].detach();
                }
            }
            this.setWidget(widget);
            if(widget){
                angular.forEach(this.widgetListeners, function (listener, type) {
                    widget.on(type, listener);
                });
                var anchor = this.getAnchor();
                angular.forEach(elements, function (element) {
                    anchor.append(element);
                });
            }
        }
        if(this.getWidget()){
            this.updateView();
        }
    };


    Locator.prototype.isConnected = function () {
        return this.connected;
    };

    return Locator;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')//

/**
 * @ngdoc Factories
 * @name BoundWidgetLocator
 * @description Locates a widget bound
 * 
 */
.factory('BoundWidgetLocator', function (AbstractWidgetLocator) {

    var boundWidgetLocator = function (options) {
        options = options || {};
        AbstractWidgetLocator.apply(this, options);

        // set anchor
        this.setAnchor(options.anchor);

        // load templates
        var template = options.template || '<div class="wb-widget-locator bound wb-layer-editor-locator"></div>';

        // load elements
        this.topElement = angular.element(template);
        this.topElement.attr('id', 'top');

        this.rightElement = angular.element(template);
        this.rightElement.attr('id', 'right');

        this.buttomElement = angular.element(template);
        this.buttomElement.attr('id', 'buttom');

        this.leftElement = angular.element(template);
        this.leftElement.attr('id', 'left');

        // init controller
        this.setElements([this.topElement, this.rightElement,
            this.buttomElement, this.leftElement]);
    };
    boundWidgetLocator.prototype = new AbstractWidgetLocator();

    boundWidgetLocator.prototype.updateView = function () {
        var widget = this.getWidget();
        var bound = widget.getBoundingClientRect();
        var space = 2;
        this.topElement.css({
            top: bound.top + space,
            left: bound.left + space,
            width: bound.width - 2*space
        });
        this.rightElement.css({
            top: bound.top + space,
            left: bound.left + bound.width - 2*space,
            height: bound.height - 2*space
        });
        this.buttomElement.css({
            top: bound.top + bound.height - space,
            left: bound.left + space,
            width: bound.width - 2*space
        });
        this.leftElement.css({
            top: bound.top + space,
            left: bound.left + space,
            height: bound.height - 2*space
        });

    };
    return boundWidgetLocator;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular
.module('am-wb-core')


/**
 * @ngdoc Factories
 * @name CursorWidgetLocator
 * @description Manages list of locators
 * 
 * 
 * There are two type of widgets locator: selection and bound.
 * 
 * For each widget a bound locator will be created.
 * 
 * For each item in selection a selection locator will be created.
 */
.factory('WidgetLocatorManager',function ($widget, BoundWidgetLocator, SelectionWidgetLocator) {

    /**
     * Creates new instance of the manager
     * 
     * @memberof CursorWidgetLocator
     */
    function WidgetLocatorManager(options) {
        var ctrl = this;
        options = options || {};

        this.boundLocatorMap = new Map();
        this.boundLocatorTrash = [];

        this.selectionLocatorMap = new Map();
        this.selectionLocatorTrash = [];


        // selection options
        this.selectionLocatorOption = options.selectionLocatorOption || {};
        this.selectionEnable = true;
        if (angular.isDefined(options.selectionEnable)) {
            this.selectionEnable = options.selectionEnable;
        }

        // bound options
        this.boundLocatorOption = options.boundLocatorOption || {};
        this.boundEnable = true;
        if (angular.isDefined(options.boundEnable)) {
            this.boundEnable = options.boundEnable;
        }

        this.widgetListeners = {
                'select': function($event){
                    ctrl.widgetSelectionChanged($event.source);
                },
                'unselect': function($event){
                    ctrl.widgetSelectionChanged($event.source);
                },
                'delete': function($event) {
                    ctrl.untrackWidget($event.source);
                },
                'newchild': function($event) {
                    _.forEach($event.widgets, function(widget){
                        ctrl.trackWidget(widget);
                    });
                },
                'loaded': function($event){
                    ctrl.disconnect();
                    ctrl.rootWidget = null;
                    ctrl.setRootWidget($event.source);
                },
        };
        this.rootListeners = {
                'newchild': function($event) {
                    _.forEach($event.widgets, function(widget){
                        ctrl.trackWidget(widget);
                    });
                },
                'loaded': function($event){
                    ctrl.disconnect();
                    ctrl.rootWidget = null;
                    ctrl.setRootWidget($event.source);
                },
        };
    }


    WidgetLocatorManager.prototype.untrackWidget = function(widget){
        // events
        var listener = widget.isRoot() ? this.rootListeners : this.widgetListeners;
        angular.forEach(listener, function (callback, type) {
            widget.off(type, callback);
        });
        this.removeBoundLocator(widget);
        this.removeSelectionLocator(widget);
    };

    WidgetLocatorManager.prototype.trackWidget = function(widget){
        // add events
        var listener = widget.isRoot() ? this.rootListeners : this.widgetListeners;
        angular.forEach(listener, function (callback, type) {
            widget.on(type, callback);
        });
        if(widget.isRoot()){
            return;
        }
        this.createBoundLocator(widget);
        if(widget.isSelected()){
            this.createSelectionLocator(widget);
        }
    };

    WidgetLocatorManager.prototype.disconnect = function(){
        var rootWidget = this.getRootWidget();
        if(!rootWidget) {
            return;
        }
        // Widgets
        var widgets = $widget.getChildren(rootWidget);
        this.untrackWidget(rootWidget);
        var ctrl = this;
        _.forEach(widgets, function(widget){
            ctrl.untrackWidget(widget);
        });
        // interval
        if(this._intervalCheck){
            clearInterval(this._intervalCheck);
            delete this._intervalCheck;
        }
    };

    WidgetLocatorManager.prototype.connect = function(){
        var rootWidget = this.getRootWidget();
        if(!rootWidget){
            return;
        }
        var ctrl = this;

        // widgets
        var widgets = $widget.getChildren(rootWidget);
        this.trackWidget(rootWidget);
        _.forEach(widgets, function(widget){
            ctrl.trackWidget(widget);
        });

        // interval
        if(!this._intervalCheck){
            this._intervalCheck = setInterval(function(){
                ctrl.updateLocators();
            }, 300);
        }
    };

    WidgetLocatorManager.prototype.setEnable = function (enable) {
        if (this.enable === enable) {
            return;
        }
        this.enable = enable;
        if(this.enable){
            this.connect();
        } else {
            this.disconnect();
        }
    };

    WidgetLocatorManager.prototype.isEnable = function () {
        return this.enable;
    };

    /**
     * Sets the root widget
     * 
     * @param rootWidget
     *            {WbAbstractWidget} root widget
     * @memberof WidgetLocatorManager
     */
    WidgetLocatorManager.prototype.setRootWidget = function (rootWidget) {
        if(this.rootWidget === rootWidget) {
            return;
        }
        var enable = this.isEnable();
        this.setEnable(false);
        this.rootWidget = rootWidget;
        this.setEnable(enable);
    };

    /**
     * Gets the root widget
     * 
     * @return the root widget
     * @memberof WidgetLocatorManager
     */
    WidgetLocatorManager.prototype.getRootWidget = function () {
        return this.rootWidget;
    };


    /**
     * Update all locators
     * 
     * @memberof WidgetLocatorManager
     */
    WidgetLocatorManager.prototype.updateLocators = function () {
        function handleWidget(bound, widget){
            if(widget.isIntersecting()){
                bound.connect(widget);
            } else {
                bound.disconnect();
            }
        }
        // locator
        this.boundLocatorMap.forEach(handleWidget);
        // selector
        this.selectionLocatorMap.forEach(handleWidget);
    };


    /**********************************************************
     * Selection
     */
    WidgetLocatorManager.prototype.hasBoundLocator = function(widget){
        return this.boundLocatorMap.has(widget);
    };

    WidgetLocatorManager.prototype.getBoundLocator = function(widget){
        return this.boundLocatorMap.get(widget);
    };

    WidgetLocatorManager.prototype.createBoundLocator = function(widget){
        var map = this.boundLocatorMap;
        if(!map.has(widget)) {
            var locator;
            if(this.boundLocatorTrash.length > 0){
                locator = this.boundLocatorTrash.pop();
            } else {
                locator = new BoundWidgetLocator(this.boundLocatorOption);
            }
            map.set(widget, locator);
        }
        return map.get(widget);
    };

    WidgetLocatorManager.prototype.removeBoundLocator = function(widget){
        if(!this.hasBoundLocator(widget)){
            return;
        }
        var locator = this.getBoundLocator(widget);
        locator.disconnect();
        this.boundLocatorMap.delete(widget);
        this.boundLocatorTrash.push(locator);
    };

    /**********************************************************
     * Selection
     */
    /**
     * Sets widgets which are selected
     * 
     */
    WidgetLocatorManager.prototype.widgetSelectionChanged = function (widgets) {
        if(!this.isEnable()){
            return;
        }
        if(!angular.isArray(widgets)){
            widgets = [widgets];
        }
        var ctrl = this;
        _.forEach(widgets, function(widget){
            if(widget.isSelected()){
                ctrl.createSelectionLocator(widget);
            } else {
                ctrl.removeSelectionLocator(widget);
            }
        });
    };

    WidgetLocatorManager.prototype.removeSelectionLocator = function(widget) {
        var map = this.selectionLocatorMap;
        if(map.has(widget)) {
            var locator = this.getSelectionLocator(widget);
            this.selectionLocatorTrash.push(locator);
            this.selectionLocatorMap.delete(widget);
            locator.disconnect();
        }
    };

    WidgetLocatorManager.prototype.createSelectionLocator = function(widget) {
        var map = this.selectionLocatorMap;
        if(!map.has(widget)) {
            var locator;
            if(this.selectionLocatorTrash.length > 0){
                locator = this.selectionLocatorTrash.pop();
            } else {
                locator = new SelectionWidgetLocator(this.selectionLocatorOption);
            }
            map.set(widget, locator);
        }
        return map.get(widget);
    };

    WidgetLocatorManager.prototype.getSelectionLocator = function(widget) {
        var map = this.selectionLocatorMap;
        return map.get(widget);
    };

    WidgetLocatorManager.prototype.hasSelectionLocator = function(widget){
        return this.selectionLocatorMap.has(widget);
    };


    return WidgetLocatorManager;
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')//

/**
 * @ngdoc Factories
 * @name AbstractWidgetLocator
 * @description Locates a widget on the view
 * 
 * It is used to display extra information about a widget on the screen. For
 * example it is used to show widget actions on the fly.
 * 
 */
.factory('SelectionWidgetLocator', function (AbstractWidgetLocator, $document) {

    var selectionWidgetLocator = function (options) {
        options = options || {};
        AbstractWidgetLocator.apply(this, options);

        // set anchor
        this.setAnchor(options.anchor);

        // load templates
        var template = options.template || '<div class="wb-widget-locator selection wb-layer-editor-selector"></div>';

        this.titleElement = angular.element(template);
        this.titleElement.attr('id', 'header');

        // load elements
        this.topElement = angular.element(template);
        this.topElement.attr('id', 'top');

        this.rightElement = angular.element(template);
        this.rightElement.attr('id', 'right');

        this.buttomElement = angular.element(template);
        this.buttomElement.attr('id', 'buttom');

        this.leftElement = angular.element(template);
        this.leftElement.attr('id', 'left');

        this.sizeElement = angular.element('<img class="wb-widget-locator selection" src="resources/corner-handle.png">');
        this.sizeElement.attr('id', 'size');

        // init controller
        this.setElements([this.titleElement, this.topElement, this.rightElement,
            this.buttomElement, this.leftElement, this.sizeElement]);
        
        
        
        var position = {};
//        var lock = false;
        var dimension = {};
        var ctrl = this;
        

        function mousemove($event) {
            var deltaWidth = dimension.width - (position.x - $event.clientX);
            var deltaHeight = dimension.height - (position.y - $event.clientY);
            var newDimensions = {
                    width: deltaWidth + 'px',
                    height: deltaHeight + 'px'
            };
            
            var widget = ctrl.getWidget();
            var model = widget.getModel();
            var $element = widget.getElement();
            var $scope = widget.getScope();
            
            if (model.style.size.height === 'auto') {
                newDimensions.height = 'auto';
            }
            $element.css(newDimensions);
            
            model.style.size.width = newDimensions.width;
            model.style.size.height = newDimensions.height;

            $scope.$apply();
            return false;
        }

        function mouseup() {
            $document.unbind('mousemove', mousemove);
            $document.unbind('mouseup', mouseup);
//            lock = false;
        }

        function mousedown($event) {
            $event.stopImmediatePropagation();
            position.x = $event.clientX;
            position.y = $event.clientY;
//            lock = true;
            var $element = ctrl.getWidget().getElement();
            dimension.width = $element.prop('offsetWidth');
            dimension.height = $element.prop('offsetHeight');
            $document.bind('mousemove', mousemove);
            $document.bind('mouseup', mouseup);
            return false;
        }


        this.sizeElement.on('mousedown', mousedown);
    };
    selectionWidgetLocator.prototype = new AbstractWidgetLocator();

    selectionWidgetLocator.prototype.updateView = function () {
        var widget = this.getWidget();
        var bound = widget.getBoundingClientRect();
        var space = 2;
        this.topElement.css({
            top: bound.top + space,
            left: bound.left + space,
            width: bound.width - 2*space
        });
        this.rightElement.css({
            top: bound.top + space,
            left: bound.left + bound.width - 2*space,
            height: bound.height - 2*space
        });
        this.buttomElement.css({
            top: bound.top + bound.height - space,
            left: bound.left + space,
            width: bound.width - 2*space
        });
        this.leftElement.css({
            top: bound.top + space,
            left: bound.left + space,
            height: bound.height - 2*space
        });
        if (bound.top < 32) {
            this.titleElement.css({
                top: bound.top + bound.height,
                left: bound.left + bound.width/2 - this.titleElement.width()/2
            });
        } else {
            this.titleElement.css({
                top: bound.top -  this.titleElement.height(),
                left: bound.left + bound.width/2 - this.titleElement.width()/2
            });
        }
        this.titleElement[0].innerHTML = '<span>' + 
            (widget.getTitle() || widget.getId() || widget.getType()) + 
            '</span>';
        
        
        this.sizeElement.css({
            top: bound.top + bound.height -13 ,
            left: bound.left + bound.width -15 ,
        });
    };
    return selectionWidgetLocator;
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @ngdoc Services
 * @name $WbProviderTimeout
 * @description UI utilities management
 * 
 */
angular.module('am-wb-core')
.factory('$WbProviderTimeout', function($timeout) {

    var promisses = [];

    /*
     * remove promise from list
     */
    function remove(promise){
        _.remove(promisses, function(prom){
            return prom == promise;
        });
    }

    /*
     * Simulates $timeout for widgets
     */
    function $WbProviderTimeout(fn, delay, invokeApply, pass){
        var promiss = $timeout(fn, delay, invokeApply, pass)
        .finally(function(){
            remove(promiss);
        });
        return promiss;
    }    
    /**
     * Clean the provider
     * 
     * @memberof $WbProviderTimeout
     */
    $WbProviderTimeout.clean = function(){
        _.forEach(promisses, function(promiss){
            $timeout.cancel(promiss);
        });
        promisses = [];
    };
    
    return $WbProviderTimeout;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function(
		/* mblowfish */ $app, $actions, $sidenav, $toolbar,
		/* angularjs */ $rootScope, $window,
		/* mateiral  */ $mdSidenav,
		/* weburger  */ $resource, $widget,
		/* amh       */ $amhEditorService) {

	$actions.newAction({
		id: 'amh.cms.pages',
		type: 'action',
		priority : 11,
		icon : 'list',
		title: 'Pages',
		description: 'Pages',
		action: function(){
			$mdSidenav('amh.cms.pages.sidenav').toggle();
		},
		groups:['amh.owner-toolbar.public']
	});

	// TODO: Hadi 1396-12-23: should get login action and add it to desired group
	$actions.newAction({ 
		id: 'amh.login',
		type: 'link',
		priority : 10,
		icon : 'login',
		title : 'Login',
		url: 'users/login',
		groups:['amh.sidenav']
	});
	// TODO: Hadi 1396-12-23: should get logout action and add it to desired group
	$actions.newAction({ 
		id: 'amh.logout',
		type: 'action',
		priority : 10,
		icon : 'exit',
		title : 'Logout',
		action : function(){
			$app.logout();
		},
		groups:['amh.sidenav']
	});
	// TODO: Hadi 1396-12-23: should get home action and add it to desired group
	$actions.newAction({ 
		id: 'amh.home',
		type: 'link',
		priority : 100,
		icon : 'home',
		title : 'Home',
		url: '/',
		groups:['amh.sidenav', 'mb.toolbar.menu']
	});

	$toolbar.newToolbar({
		id: 'amh.owner-toolbar',
		title: 'Owner Toolbar',
		description: 'Toolbar for owners',
		controller: 'AmhOwnerToolbarCtrl',
		controllerAs: 'ctrl',
		templateUrl: 'views/toolbars/amh-owner-toolbar.html',
		visible: function(){
			return !$rootScope.__account.anonymous;
		},
		raw: true
	});

	/*********************************************************************
	 * workbench  Sidenave :
	 * 
	 * - widgets
	 * - settings
	 * - templates
	 * - pages
	 ********************************************************************/
	/*
	 * Display list of widgets to drag and drop into the current document
	 * and editors.
	 */
	$sidenav.newSidenav({
		id : 'amh.workbench.weburger.widgets',
		title : 'Widgets',
		description : 'Basic widgets of Weburger',
		template : '<wb-widgets-explorer ng-model="widgets.items"></wb-widgets-explorer>',
		locked : true,
		visible : function () {
			return $rootScope.showWidgetsPanel;
		},
		position : 'start',
		/*
		 * @ngInject
		 */
		controller: function($scope){
			$widget.widgets()
			.then(function(list){
				$scope.widgets = list;
			});
		},
		controllerAs: 'ctrl'
	});
	$sidenav.newSidenav({
		id : 'amh.workbench.weburger.navigator',
		title : 'Navigator',
		description : 'Navigator widgets of Weburger',
		templateUrl : 'views/sidenavs/amh-contents-navigator.html',
		locked : true,
		visible : function () {
			return $rootScope.showWidgetsNavigator;
		},
		position : 'start',
		/*
		 * @ngInject
		 */
		controller: function($scope){

			var ctrl = this;

			$scope.treeOptions = {
//					accept: function(sourceNodeScope, destNodesScope, destIndex) {
//						return true;
//					},
//					beforeDrag(sourceNodeScope)
//					removed(node)
//					dropped(event)
//					dragStart(event)
//					dragMove(event)
//					dragStop(event)
//					beforeDrop(event)
//					toggle(collapsed, sourceNodeScope)
			};

			$scope.collapseAll = function () {
				$scope.$broadcast('angular-ui-tree:collapse-all');
			};

			$scope.expandAll = function () {
				$scope.$broadcast('angular-ui-tree:expand-all');
			};

			$scope.toggleItem = function (handler, $event) {
				handler.toggle();
				$event.stopPropagation();
			};

			$scope.dblclick = function(widget, handler, $event){
				$event.items = [widget];
				$actions.exec('amh.workbench.widget.dblclick', $event);
			};

			$scope.click = function(widget, handler, $event){
				$event.items = [widget];
				$actions.exec('amh.workbench.widget.click', $event);
			};


			$scope.openHelp = function(widget){
				return $help.openHelp(widget);
			};

			$scope.selectChildren = function(widget, $event){
				$event.items = [widget];
				$actions.exec('amh.workbench.widget.selectChildren', $event);
			}

			function setRootWidget(widget){
				ctrl.widgets = [widget];
			}

			function activeEditorChanged(/*event*/){
				var activeEditor  = ctrl.workbench.getActiveEditor();
				if(!activeEditor){
					return;
				}
				activeEditor.on('rootWidgetChanged', function($event){
					setRootWidget($event.value);
				});
				setRootWidget(activeEditor.getRootWidget());
			}

			function setWorkbench(workbench){
				if(ctrl.workbench){
					ctrl.workbench.off('activeEditorChanged', activeEditorChanged);
				}
				ctrl.workbench = workbench;
				if(ctrl.workbench){
					ctrl.workbench.on('activeEditorChanged', activeEditorChanged);
				}
				activeEditorChanged();
			}

			function handleWorkbench(event){
				setWorkbench(event.value);
			}

			setWorkbench($amhEditorService.getWorkbench());
			$amhEditorService.on('workbenchChanged', handleWorkbench);
			$scope.$on('destory', function(){
				$amhEditorService.off('workbenchChanged', handleWorkbench);
				if(ctrl.workbench){
					ctrl.workbench.on('contentChanged', contentChanged);
				}
			});
		},
		controllerAs: 'ctrl'
	});
	/*
	 * Adds a sidenav to manage settings of selected widgets. You can
	 * ecit attributes, style, and events of an widget.
	 */
	$sidenav.newSidenav({
		id : 'amh.workbench.weburger.settings',
		title : 'Settings',
		description : 'Settings and configurations fo selected widgets',
		templateUrl : 'views/sidenavs/amh-workbench-settings.html',
		locked : true,
		visible : function () {
			return $rootScope.showWorkbenchSettingPanel;
		},
		position : 'end',
		/*
		 * @ngInject
		 */
		controller: function(){
			$rootScope.showWorkbenchPanel = true;
			this.setVisible = function(visible){
				$rootScope.showWorkbenchSettingPanel = visible;
			};

			this.isVisible = function(){
				return $rootScope.showWorkbenchSettingPanel;
			}
		},
		controllerAs: 'ctrl'
	});
	/*
	 * Allow user to search and apply a template from the backend.
	 */
	$sidenav.newSidenav({
		id : 'amh.workbench.weburger.templates',
		title : 'Templates',
		description : 'Select and apply a templte to the current page',
		templateUrl : 'views/sidenavs/amh-workbench-template.html',
		locked : false,
//		visible : function () {
//		return true;
//		},
		position : 'start',
		/*
		 * @ngInject
		 */
		controller: function(){
			this.loadModelTemplate = function(template, $event){
				var $event = $event || jQuery.Event( "upload", { 
					target: this
				});
				$event.template = [template];
				return $actions.exec('amh.workbench.contentValue.upload', $event);
			};
		},
		controllerAs: 'ctrl'
	});

	/*
	 * Allow user to search and find pages from the backend.
	 */
	$sidenav.newSidenav({
		id : 'amh.cms.pages.sidenav',
		title : 'Pages',
		description : 'Sidenav contain navigations about AMH',
		templateUrl : 'views/sidenavs/amh-contents-list.html',
		locked : false,
		position : 'start',
		/*
		 * @ngInject
		 */
		controller: function($scope){
			$scope.pageId = 'pages';
			$scope.goto = function(pageId) {
				$scope.pageId = pageId;
			};
		}
	});
	/*
	 * Allow user to change current content settings such as title, description
	 * and ..
	 */
	$sidenav.newSidenav({
		id : 'amh.workbench.content',
		title : 'Page settings',
		description : 'Manages current page settings',
		templateUrl : 'views/sidenavs/amh-workbench-content.html',
		locked : false,
//		visible : function () {
//		return true;
//		},
		position : 'start',
		/*
		 * @ngInject
		 */
		controller: function($scope){
			var ctrl = this;

			function contentChanged(){
				if(ctrl.workbench){
					ctrl.content = ctrl.workbench.getContent();
				}
			}

			function setWorkbench(workbenc){
				if(ctrl.workbench){
					ctrl.workbench.off('contentChanged', contentChanged);
				}
				ctrl.workbench = workbenc;
				if(ctrl.workbench){
					ctrl.workbench.on('contentChanged', contentChanged);
				}
				contentChanged();
			}

			function handleWorkbench(event){
				setWorkbench(event.value);
			}

			this.saveChanges = function(){
				var promise = this.content.update()
				.then(function(){
					ctrl.contentDirty = false;
				});
				var job = new AmhWorkbenchJob('Save content', promise);
				this.workbench.addJob(job);
				return job;
			};

			setWorkbench($amhEditorService.getWorkbench());
			$amhEditorService.on('workbenchChanged', handleWorkbench);
			$scope.$on('destory', function(){
				$amhEditorService.off('workbenchChanged', handleWorkbench);
				if(ctrl.workbench){
					ctrl.workbench.on('contentChanged', contentChanged);
				}
			});
		},
		controllerAs: 'ctrl'
	});

	/*********************************************************************
	 * Resources
	 ********************************************************************/
	$resource.newPage({
		type: 'pages',
		icon: 'insert_drive_file',
		label: 'pages',
		templateUrl: 'views/resources/pages.html',
		controller: 'AmhSeenSelectPagesCtrl',
		controllerAs: 'ctrl',
		priority: 10,
		tags: ['url']
	});
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('mblowfish-core')

/*
 * Enable crisp chat
 */
.run(function($window, $rootScope, $wbWindow) {

	/*
	 * Watch system configuration
	 */
	$rootScope.$watch('app.config.crisp.id', function(value){
		if (!value) {
			return;
		}
		
		$wbWindow.loadLibrary('https://client.crisp.chat/l.js');
		$window.$crisp=[];
		$window.CRISP_WEBSITE_ID = value;
	});
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
/*
 * load processor of workbench and editor
 */
.run(function($amhEditorService, 
		/* editor    */ AmhEditorProcessorClipboard, AmhEditorProcessorCommon, AmhEditorProcessorUtils,
		/* workbench */ AmhWorkbenchProcessorClone, AmhWorkbenchProcessorContentValue, 
		AmhWorkbenchProcessorMetainfo, AmhWorkbenchProcessorCrud) {
	// editor
	$amhEditorService.addEditorProcessor(AmhEditorProcessorClipboard);
	$amhEditorService.addEditorProcessor(AmhEditorProcessorCommon);
	$amhEditorService.addEditorProcessor(AmhEditorProcessorUtils);
	
	// workbench
	$amhEditorService.addWorkbenchProcessor(AmhWorkbenchProcessorClone);
	$amhEditorService.addWorkbenchProcessor(AmhWorkbenchProcessorContentValue);
	$amhEditorService.addWorkbenchProcessor(AmhWorkbenchProcessorCrud);
	$amhEditorService.addWorkbenchProcessor(AmhWorkbenchProcessorMetainfo);
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
/**
 * Adds basic system settings
 * 
 */
.run(function ($preferences) {
	$preferences
	
	/**
	 * @ngdoc preferences
	 * @name crisp-chat
	 * @description adds crisp chat into the page
	 */
	.newPage({
		id : 'crisp-chat',
		title : 'CRISP chat',
		templateUrl : 'views/preferences/mb-crisp-chat.html',
		description : 'Give your customer messaging experience a human touch with CRISP.',
		icon : 'chat',
		tags : [ 'chat' ]
	})
	
	/**
	 * @ngdoc preferences
	 * @name main-page-template
	 * @description Set initial template for main page of the current SPA
	 * 
	 * This page is used to init main page and used in the initial process
	 */
	.newPage({
		id: 'main-page-template',
		templateUrl: 'views/preferences/main-page-template.html',
		controller: 'AmhMainPageTmplCtrl',
		title: 'Main Page Template',
		description: 'Set initial template for main page.',
		icon: 'view_quilt',
		priority: 1,
		hidden: true,
		required: true
	})
	
	/**
	 * @ngdoc preferences
	 * @name pageNotFound
	 * @description Manage page not found page
	 * 
	 * In this preference page, user are allowed to design the page 
	 * of NOT FOUND.
	 * 
	 */
	.newPage({
		id: 'pageNotFound',
		templateUrl: 'views/preferences/pageNotFound.html',
		title: 'Page not found',
		description: 'Default error page of the application.',
		icon: 'pageview',
		helpId: 'page-not-found',
		/*
		 * @ngInject
		 */
		controller: function ($scope, $actions, $rootScope) {
			/*
			 * Add scope menus
			 */
			if(!$rootScope.app.config.pageNotFound){
				$scope.app.config.pageNotFound = {};
			}
		}
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')

/**
 * Load widgets
 */
.run(function ($settings) {
	/************************************************************************
	 * Model and Widgets
	 ************************************************************************/
	$settings.addPage({
		type: 'general',
		label: 'Model',
		icon: 'opacity',
		templateUrl: 'views/settings/wb-widget-general.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingGeneralCtrl',
	})
	.addPage({
		type: 'a',
		label: 'Link',
		description: 'Manage link in the current widget.',
		icon: 'settings',
		templateUrl: 'views/settings/wb-widget-a.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingACtrl',
		targets: ['^a$']
	})
	.addPage({
		type: 'img',
		label: 'Image',
		description: 'Manage image widget settings.',
		icon: 'insert_photo',
		templateUrl: 'views/settings/wb-widget-img.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetImgCtrl',
		targets: ['img']
	})
	.addPage({
		type: 'input',
		label: 'Input',
		description: 'Manage input widget settings.',
		icon: 'input',
		templateUrl: 'views/settings/wb-widget-input.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetInputCtrl',
		targets: ['input']
	})
	.addPage({
		type: 'microdata',
		label: 'Microdata',
		description: 'Manage widget microdata.',
		icon: 'label_important',
		templateUrl: 'views/settings/wb-widget-microdata.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetMicrodataCtrl'
	})
	.addPage({
		type: 'meta',
		label: 'Meta',
		icon: 'label_important',
		templateUrl: 'views/settings/wb-widget-meta.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetMetaCtrl'
	})
	.addPage({
		type: 'iframe',
		label: 'Frame',
		description: 'Manges IFrame attributes',
		icon: 'filter_frames',
		templateUrl: 'views/settings/wb-widget-iframe.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetIFrameCtrl',
		targets: ['iframe']
	})
	.addPage({
		type: 'form',
		label: 'Form',
		description: 'Manges form attributes',
		icon: 'filter_frames',
		templateUrl: 'views/settings/wb-widget-form.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetFormCtrl',
		targets: ['form']
	})
	.addPage({
		type: 'source',
		label: 'Source',
		templateUrl: 'views/settings/wb-widget-source.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetSourceCtrl',
		targets: ['source']
	})
	.addPage({
		type: 'picture',
		label: 'Picture',
		templateUrl: 'views/settings/wb-widget-picture.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetPictureCtrl',
		targets: ['picture']
	})
	.addPage({
		type: 'video',
		label: 'Video',
		templateUrl: 'views/settings/wb-widget-video.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetVideoCtrl',
		targets: ['video']
	})
	.addPage({
		type: 'audio',
		label: 'Audio',
		templateUrl: 'views/settings/wb-widget-audio.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingWidgetAudioCtrl',
		targets: ['audio']
	});


	/************************************************************************
	 * Style
	 ************************************************************************/
	$settings.addPage({
		type: 'style.animation',
		label: 'Animation',
		icon: 'opacity',
		templateUrl: 'views/settings/wb-style-animation.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingStyleAnimationCtrl',
	})
	.addPage({
		type: 'style.background',
		label: 'Background',
		icon: 'opacity',
		templateUrl: 'views/settings/wb-style-background.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingStyleBackgroundCtrl',
	})
	.addPage({
		type: 'style.border',
		label: 'Border',
		icon: 'opacity',
		templateUrl: 'views/settings/wb-style-border.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingStyleBorderCtrl',
	})
	.addPage({
		type: 'style.general',
		label: 'General',
		icon: 'opacity',
		templateUrl: 'views/settings/wb-style-general.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingStyleGeneralCtrl',
	})
	.addPage({
		type: 'style.layout',
		label: 'Layout',
		icon: 'opacity',
		templateUrl: 'views/settings/wb-style-layout.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingStyleLayoutCtrl',
	})
	.addPage({
		type: 'style.media',
		label: 'Medai',
		icon: 'opacity',
		templateUrl: 'views/settings/wb-style-media.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingStyleMediaCtrl',
	})
	.addPage({
		type: 'style.size',
		label: 'Size',
		icon: 'opacity',
		templateUrl: 'views/settings/wb-style-size.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingStyleSizeCtrl',
	})
	.addPage({
		type: 'style.text',
		label: 'Text',
		icon: 'opacity',
		templateUrl: 'views/settings/wb-style-text.html',
		controllerAs: 'ctrl',
		controller: 'WbSettingStyleTextCtrl',
	});
});

///* 
// * The MIT License (MIT)
// * 
// * Copyright (c) 2016 weburger
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//angular.module('am-wb-core')
//
///*
// * Load default resources
// */
//.run(function(/*$resource, TinymcePluginImageTool, TinymcePluginCodesample, TinymcePluginLink*/) {
////	var pluginManager = tinymce.PluginManager;
//	// XXX: maso, 2019: update to tinymce5
////	pluginManager.add('codesample', TinymcePluginCodesample);
////	pluginManager.add('image', TinymcePluginImageTool);
////	pluginManager.add('link', TinymcePluginLink);
//});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')


/***********************************************************************
 * Editors
 ***********************************************************************/
.run(function ($widget, $resource) {

	/*
	 * 
	 * meta.type: file, image, media
	 */
	function filePickerCallback(callback, value, meta) {
		// Provide file and text for the link dialog
		if (meta.filetype == 'file') {
			return $resource.get('url', {
				value: value
			})
			.then(function(url){
				callback(url, {
					text: 'My text'
				});
			});
		}

		// Provide image and alt text for the image dialog
		if (meta.filetype == 'image') {
			return $resource.get('image-url', {
				value: value
			})
			.then(function(url){
				callback(url, {
					alt: 'My text'
				});
			});
		}

		// Provide alternative source and posted for the media dialog
		if (meta.filetype == 'media') {
			return $resource.get('media', {
				value: value
			})
			.then(function(url){
				callback(url, {
					source2: 'alt.ogg', 
					poster: 'image.jpg'
				});
			});
		}
	}

	/***************************************
	 * Section editor
	 * 
	 *  A section is combination of blocks
	 * widgets such as h, p, and pre. This 
	 * is an editor to edit the section.
	 ***************************************/
	$widget.setEditor('section', {
		type: 'WidgetEditorTinymceSection',
		options:{
			inline: true,
			menubar: false,
			inline_boundaries: false,
			plugins: ['link', 'lists', 'powerpaste', 'autolink', 'code', 'image', 'fonticon', 'fullpage'],
			valid_elements: '*[*]',
			// Toolbar
			toolbar: ['close save code | image fonticon | undo redo | bold italic underline link | fontselect fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignfull | numlist bullist outdent indent | fullpage'],
			fixed_toolbar_container: '#demo-widget-editor-toolbar',
			toolbar_drawer: 'floating',
			// multimedia and file
			file_picker_callback: filePickerCallback,
			file_picker_types: 'file image media',
			// Image
			image_caption: true,
			image_advtab: true,
			image_description: true,
			image_dimensions: true,
			image_uploadtab: true,
//			image_list: imageList,
//			images_upload_handler: imagesUploadHandler
		}
	});

	/***************************************
	 * Single line editor
	 * 
	 *  Single line editor is used for nay widget
	 * with html property.
	 ***************************************/
	var lineWidgetsTypes = [
		'h1','h2','h3','h4','h5','h6', 
		'li',
		'button',
		'a',
		'p',
		'figcaption',
		'i', 'em', 
		's', 'samp', 'small', 'span', 'strong', 
		'mark', 'cite', 'dfn'
		];
	_.forEach(lineWidgetsTypes, function(type){
		$widget.setEditor(type, {
			type: 'WidgetEditorTinymceSingleLine',
			options:{
				property: 'html',
				inline: true,
				menubar: false,
				inline_boundaries: false,
				plugins: ['link', 'code', 'image', 'fonticon'],
				valid_elements: '*[*]',
				// Toolbar
				toolbar: 'close save code | image fonticon | undo redo | bold italic underline link| fontselect fontsizeselect | forecolor backcolor | widgetalignleft widgetaligncenter widgetalignjustify widgetalignright ',
				fixed_toolbar_container: '#demo-widget-editor-toolbar',
				toolbar_drawer: 'floating',
				// multimedia and file
				file_picker_callback: filePickerCallback,
				file_picker_types: 'file image media',
				// Image
				image_caption: true,
				image_advtab: true,
				image_description: true,
				image_dimensions: true,
				image_uploadtab: true,
//				image_list: imageList,
//				images_upload_handler: imagesUploadHandler
			}
		});
	});

	/************************************
	 * Code editor
	 * 
	 ************************************/
	$widget.setEditor('pre', {
		type: 'WidgetEditorCode',
		options: {}
	});
	$widget.setEditor('style', {
		type: 'WidgetEditorCode',
		options: {}
	});
});

/* 

 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('am-wb-core')

/***********************************************************************
 * Convertors
 ***********************************************************************/
.run(function ($widget, WbConverterWeburger, WbConverterDom, WbConverterText) {
	$widget.addConverter(new WbConverterWeburger());
	$widget.addConverter(new WbConverterDom());
	$widget.addConverter(new WbConverterText());
})
/***********************************************************************
 * Processors
 ***********************************************************************/
.run(function ($widget, WbProcessorMicrodata, WbProcessorEvent, WbProcessorAttribute) {
	$widget.setProcessor('microdata', new WbProcessorMicrodata());
	$widget.setProcessor('event', new WbProcessorEvent());
	$widget.setProcessor('attribut', new WbProcessorAttribute());
})
/***********************************************************************
 * Providers
 ***********************************************************************/
.run(function (
		/* angularjs */ $location, $http, 
		/* WB        */ $widget, $mdMedia, 
		$wbWindow, $wbLocal, $WbProviderTimeout, 
		$dispatcher, $storage) {
	$widget//
	.setProvider('$http', $http)
	.setProvider('$window', $wbWindow)
	.setProvider('$location', $location)
	.setProvider('$dispatcher', $dispatcher)
	.setProvider('$storage', $storage)
	.setProvider('$timeout', $WbProviderTimeout)
	.setProvider('$local', $wbLocal)
	.setProvider('$media', $mdMedia);
})

/***********************************************************************
 * Widgets
 ***********************************************************************/
.run(function ($widget) {
	$widget.newWidget({
		// widget description
		type: 'a',
		title: 'A link',
		description: 'A widget to add external link. It is used as block item.',
		icon: 'wb-widget-a',
		groups: ['basic'],
		// functional properties
		template: '<a></a>',
		model: {
			html: 'Link title'
		},
		controller: 'WbWidgetA',
		isLeaf: true
	});
	$widget.newWidget({
		// widget description
		type: 'address',
		title: 'address',
		description: 'description.',
		icon: 'wb-widget-address',
		groups: ['basic'],
		// functional properties
		template: '<address></address>',
		controller: 'WbWidgetAddress',
		isLeaf: false
	});
	$widget.newWidget({
		// widget description
		type: 'applet',
		title: 'applet',
		description: 'applet.',
		icon: 'wb-widget-applet',
		groups: ['basic'],
		// functional properties
		template: '<applet></applet>',
		controller: 'WbWidgetApplet',
		isLeaf: true
	});
	$widget.newWidget({
		// widget description
		type: 'area',
		title: 'area',
		description: 'area',
		icon: 'wb-widget-area',
		groups: ['basic'],
		// functional properties
		template: '<area></area>',
		controller: 'WbWidgetArea'
	});
	$widget.newWidget({
		// widget description
		type: 'article',
		title: 'article',
		description: 'article',
		icon: 'wb-widget-article',
		groups: ['basic'],
		// functional properties
		template: '<article></article>',
		controller: 'WbWidgetArticle'
	});
	$widget.newWidget({
		// widget description
		type: 'aside',
		title: 'aside',
		description: 'aside',
		icon: 'wb-widget-aside',
		groups: ['basic'],
		// functional properties
		template: '<aside></aside>',
		controller: 'WbWidgetAside'
	});
	$widget.newWidget({
		type: 'audio',
		title: 'Audio',
		label: 'audio',
		icon: 'wb-widget-audio',
		description: 'This widget is used to add audio in the document.',
		groups: ['basic'],
		template: '<audio></audio>',
		model: {
			media: '(min-width: 650px)',
			src: 'http://www.gitlab.com/am-wb/am-wb-commonhttps://unsplash.com/photos/8emNXIvrCL8/download?force=true'
		},
		controller: 'WbWidgetAudio', 
		isLeaf: false, 
	});
	$widget.newWidget({
		type: 'blockquote',
		title: 'blockquote',
		label: 'blockquote',
		icon: 'wb-widget-blockquote',
		description: 'description',
		groups: ['basic'],
		template: '<blockquote></blockquote>',
		controller: 'WbWidgetBlockquote',
		isLeaf: true, 
	});
	$widget.newWidget({
		type: 'button',
		title: 'button',
		label: 'button',
		icon: 'wb-widget-button',
		description: 'description',
		groups: ['basic'],
		template: '<button></button>',
		controller: 'WbWidgetButton',
		isLeaf: true, 
	});
	$widget.newWidget({
		type: 'canvas',
		title: 'canvas',
		label: 'canvas',
		icon: 'wb-widget-canvas',
		description: 'description',
		groups: ['basic'],
		template: '<canvas></canvas>',
		controller: 'WbWidgetCanvas',
		isLeaf: true, 
	});
	$widget.newWidget({
		type: 'datalist',
		title: 'datalist',
		label: 'datalist',
		icon: 'wb-widget-datalist',
		description: 'description',
		groups: ['basic'],
		template: '<datalist></datalist>',
		controller: 'WbWidgetDatalist', 
	});
	$widget.newWidget({
		type: 'dd',
		title: 'dd',
		label: 'dd',
		icon: 'wb-widget-dd',
		description: 'description',
		groups: ['basic'],
		template: '<dd></dd>',
		controller: 'WbWidgetDd', 
	});
	$widget.newWidget({
		type: 'details',
		title: 'details',
		label: 'details',
		icon: 'wb-widget-details',
		description: 'description',
		groups: ['basic'],
		template: '<details></details>',
		controller: 'WbWidgetDetails', 
	});
	$widget.newWidget({
		type: 'dialog',
		title: 'dialog',
		label: 'dialog',
		icon: 'wb-widget-dialog',
		description: 'description',
		groups: ['basic'],
		template: '<dialog></dialog>',
		controller: 'WbWidgetDialog', 
	});
	$widget.newWidget({
		type: 'div',
		title: 'div',
		label: 'div',
		icon: 'wb-widget-div',
		description: 'description',
		groups: ['basic'],
		template: '<div></div>',
		controller: 'WbWidgetDiv',
		isLeaf: false
	});
	$widget.newWidget({
		type: 'dl',
		title: 'dl',
		label: 'dl',
		icon: 'wb-widget-dl',
		description: 'description',
		groups: ['basic'],
		template: '<dl></dl>',
		controller: 'WbWidgetDl', 
	});
	$widget.newWidget({
		type: 'dt',
		title: 'dt',
		label: 'dt',
		icon: 'wb-widget-dt',
		description: 'description',
		groups: ['basic'],
		template: '<dt></dt>',
		controller: 'WbWidgetDt', 
	});
	$widget.newWidget({
		type: 'embed',
		title: 'embed',
		label: 'embed',
		icon: 'wb-widget-embed',
		description: 'description',
		groups: ['basic'],
		template: '<embed></embed>',
		help: 'http://dpq.co.ir/more-information-embed',
		controller: 'WbWidgetEmbed', 
	});
	$widget.newWidget({
		type: 'fieldset',
		title: 'fieldset',
		label: 'fieldset',
		icon: 'wb-widget-fieldset',
		description: 'description',
		groups: ['basic'],
		template: '<fieldset></fieldset>',
		help: 'http://dpq.co.ir/more-information-fieldset',
		controller: 'WbWidgetFieldset', 
	});
	$widget.newWidget({
		type: 'figcaption',
		title: 'figcaption',
		label: 'figcaption',
		icon: 'wb-widget-figcaption',
		description: 'description',
		groups: ['basic'],
		template: '<figcaption></figcaption>',
		help: 'http://dpq.co.ir/more-information-figcaption',
		controller: 'WbWidgetFigcaption', 
	});
	$widget.newWidget({
		type: 'figure',
		title: 'figure',
		label: 'figure',
		icon: 'wb-widget-figure',
		description: 'description',
		groups: ['basic'],
		template: '<figure></figure>',
		help: 'http://dpq.co.ir/more-information-figure',
		controller: 'WbWidgetFigure', 
	});
	$widget.newWidget({
		type: 'footer',
		title: 'footer',
		label: 'footer',
		icon: 'wb-widget-footer',
		description: 'description',
		groups: ['basic'],
		template: '<footer></footer>',
		help: 'http://dpq.co.ir/more-information-footer',
		controller: 'WbWidgetFooter', 
	});
	$widget.newWidget({
		type: 'form',
		title: 'form',
		label: 'form',
		icon: 'wb-widget-form',
		description: 'description',
		groups: ['basic'],
		template: '<form></form>',
		help: 'http://dpq.co.ir/more-information-form',
		controller: 'WbWidgetForm', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'frame',
		title: 'frame',
		label: 'frame',
		icon: 'wb-widget-form',
		description: 'description',
		groups: ['basic'],
		template: '<frame></frame>',
		help: 'http://dpq.co.ir/more-information-frame',
		controller: 'WbWidgetFrame', 
	});
	$widget.newWidget({
		type: 'frameset',
		title: 'frameset',
		label: 'frameset',
		icon: 'wb-widget-frameset',
		description: 'description',
		groups: ['basic'],
		template: '<frameset></frameset>',
		help: 'http://dpq.co.ir/more-information-frameset',
		controller: 'WbWidgetFrameset', 
		isLeaf: false
	});
	for(var i = 1; i < 7; i++){
		var type = 'h'+i;
		$widget.newWidget({
			// widget description
			type: type,
			title: 'Header Level '+i,
			description: 'A header widget',
			icon: 'wb-widget-h'+i,
			groups: ['basic'],
			model: {
				name: 'Header-'+i,
				style: {
					padding: '8px'
				}
			},
			// functional properties
			template: '<h' +i +'></h' + i + '>',
			controller:'WbWidgetH',
			isLeaf: true
		});
	}
	$widget.newWidget({
		type: 'header',
		title: 'header',
		label: 'header',
		icon: 'wb-widget-header',
		description: 'description',
		groups: ['basic'],
		template: '<header></header>',
		controller: 'WbWidgetHeader', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'hr',
		title: 'hr',
		label: 'hr',
		icon: 'wb-widget-hr',
		description: 'description',
		groups: ['basic'],
		template: '<hr></hr>',
		controller: 'WbWidgetHr',
		isLeaf: true, 
	});
	$widget.newWidget({
		// widget description
		type: 'i',
		title: 'Italics',
		description: 'The widget defines a part of text in an alternate voice or mood.',
		icon: 'wb-widget-i',
		groups: ['basic'],
		model: {
			name: 'i',
			html: 'Text'
		},
		// help id
		help: 'http://dpq.co.ir',
		helpId: 'wb-widget-i',
		// functional properties
		template: '<i></i>',
		controllerAs: 'ctrl',
		controller: 'WbWidgetI',
		isLeaf: true,
	});
	$widget.newWidget({
		// widget description
		type: 'iframe',
		title: 'Inline Frame',
		description: 'Add inline frame to show another document within current one.',
		icon: 'wb-widget-iframe',
		groups: ['basic'],
		model: {
			name: 'iframe',
			sandbox: 'allow-same-origin allow-scripts',
			src: 'https://www.google.com',
			style: {
				padding: '8px'
			}
		},
		// help id
		help: 'http://dpq.co.ir',
		helpId: 'wb-widget-iframe',
		// functional properties
		template: '<iframe>Frame Not Supported?!</iframe>',
		controllerAs: 'ctrl',
		controller: 'WbWidgetIframe',
		isLeaf: true,
	});
	$widget.newWidget({
		type: 'img',
		title: 'Image',
		label: 'image',
		icon: 'wb-widget-img',
		description: 'A widget to insert an link to page.',
		groups: ['basic'],
		template: '<img></img>',
		model: {
			html: 'img',
			src: 'resources/wb-brand-3.0.png',
			style: {
				width: '80%',
				maxWidth: '500px'
			}
		},
		controllerAs: 'ctrl',
		controller: 'WbWidgetImg',
		isLeaf: true, 
	});
	$widget.newWidget({
		// widget description
		type: 'input',
		title: 'Input field',
		description: 'A widget to get data from users.',
		icon: 'wb-widget-input',
		groups: ['basic'],
		model: {
			name: 'input',
			sandbox: 'allow-same-origin allow-scripts',
			src: 'https://www.google.com',
			style: {
				padding: '8px'
			}
		},
		// help id
		help: 'http://dpq.co.ir',
		helpId: 'wb-widget-input',
		// functional properties
		template: '<input></input>',
		controller: 'WbWidgetInput',
		controllerAs: 'ctrl',
		isLeaf: true,
	});
	$widget.newWidget({
		type: 'kbd',
		title: 'kbd',
		label: 'kbd',
		icon: 'wb-widget-kbd',
		description: 'description',
		groups: ['basic'],
		template: '<kbd></kbd>',
		controller: 'WbWidgetKbd', 
	});
	$widget.newWidget({
		type: 'label',
		title: 'label',
		label: 'label',
		icon: 'wb-widget-label',
		description: 'description',
		groups: ['basic'],
		template: '<label></label>',
		controller: 'WbWidgetLabel',
		isLeaf: true, 
	});
	$widget.newWidget({
		type: 'legend',
		title: 'legend',
		label: 'legend',
		icon: 'wb-widget-legend',
		description: 'description',
		groups: ['basic'],
		template: '<legend></legend>',
		controller: 'WbWidgetLegend',
		isLeaf: true, 
	});
	$widget.newWidget({
		type: 'li',
		title: 'li',
		label: 'li',
		icon: 'wb-widget-li',
		description: 'description',
		groups: ['basic'],
		template: '<li></li>',
		controller: 'WbWidgetLi',
		isLeaf: false, 
	});
	$widget.newWidget({
		type: 'link',
		title: 'Link',
		label: 'link',
		icon: 'wb-widget-link',
		description: 'A widget to insert an link to page.',
		groups: ['basic'],
		template: '<link></link>',
		model: {
			html: 'Link',
			url: 'http://www.gitlab.com/am-wb/am-wb-common'
		},
		controllerAs: 'ctrl',
		controller: 'WbWidgetLink',
		isLeaf: true, 
	});
	$widget.newWidget({
		type: 'main',
		title: 'main',
		label: 'main',
		icon: 'wb-widget-main',
		description: 'A widget to insert an link to page.',
		groups: ['basic'],
		template: '<main></main>',
		controller: 'WbWidgetMain', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'map',
		title: 'map',
		label: 'map',
		icon: 'wb-widget-map',
		description: 'description',
		groups: ['basic'],
		template: '<map></map>',
		controller: 'WbWidgetMap', 
		isLeaf: false
	});
	$widget.newWidget({
		// widget description
		type: 'meta',
		title: 'Meta',
		description: 'A widget to add meta data.',
		icon: 'wb-widget-meta',
		groups: ['basic'],
		model: {
			name: 'name',
			content: 'content',
			style: {
				margin: '8px',
				background: {
					color: '#313131',
				},
				border: {
					style:  'dotted',
					color:  '#afafaf'
				},
				color:  '#ffffff',
				padding:  '8px'
			}
		},
		// functional properties
		template: '<meta></meta>',
		controllerAs: 'ctrl',
		controller: 'WbWidgetMeta'
	});
	$widget.newWidget({
		type: 'meter',
		title: 'meter',
		label: 'meter',
		icon: 'wb-widget-meter',
		description: 'description',
		groups: ['basic'],
		template: '<meter></meter>',
		controller: 'WbWidgetMeter', 
	});
	$widget.newWidget({
		type: 'nav',
		title: 'nav',
		label: 'nav',
		icon: 'wb-widget-nav',
		description: 'description',
		groups: ['basic'],
		template: '<nav></nav>',
		controller: 'WbWidgetNav', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'noscript',
		title: 'noscript',
		label: 'noscript',
		icon: 'wb-widget-noscript',
		description: 'description',
		groups: ['basic'],
		template: '<noscript></noscript>',
		controller: 'WbWidgetNoscript', 
	});
	$widget.newWidget({
		type: 'object',
		title: 'object',
		label: 'object',
		icon: 'wb-widget-object',
		description: 'description',
		groups: ['basic'],
		template: '<object></object>',
		controller: 'WbWidgetObject', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'ol',
		title: 'ol',
		label: 'ol',
		icon: 'wb-widget-ol',
		description: 'description',
		groups: ['basic'],
		template: '<ol></ol>',
		controller: 'WbWidgetOl', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'optgroup',
		title: 'optgroup',
		label: 'optgroup',
		icon: 'wb-widget-optgroup',
		description: 'description',
		groups: ['basic'],
		template: '<optgroup></optgroup>',
		controller: 'WbWidgetOptgroup', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'option',
		title: 'option',
		label: 'option',
		icon: 'wb-widget-option',
		description: 'description',
		groups: ['basic'],
		template: '<option></option>',
		controller: 'WbWidgetOption', 
	});
	$widget.newWidget({
		type: 'output',
		title: 'output',
		label: 'output',
		icon: 'wb-widget-output',
		description: 'description',
		groups: ['basic'],
		template: '<output></output>',
		controller: 'WbWidgetOutput', 
	});
	$widget.newWidget({
		// widget description
		type: 'p',
		title: 'Paragraph',
		description: 'A widget to add paragraph.',
		icon: 'wb-widget-p',
		groups: ['basic'],
		model: {
			name: 'Pragraph',
			style: {
				padding: '8px'
			}
		},
		// functional properties
		template: '<p></p>',
		controllerAs: 'ctrl',
		controller: 'WbWidgetP',
		isLeaf: true
	});
	$widget.newWidget({
		type: 'param',
		title: 'param',
		label: 'param',
		icon: 'wb-widget-param',
		description: 'description',
		groups: ['basic'],
		template: '<param></param>',
		controller: 'WbWidgetParam',
		isLeaf: true, 
	});
	$widget.newWidget({
		type: 'picture',
		title: 'Picture',
		label: 'picture',
		icon: 'wb-widget-picture',
		description: 'This widget is used to add picture in the document.',
		groups: ['basic'],
		template: '<picture></picture>',
		model: {
			media: '(min-width: 650px)',
			src: 'http://www.gitlab.com/am-wb/am-wb-commonhttps://unsplash.com/photos/8emNXIvrCL8/download?force=true'
		},
		controller: 'WbWidgetPicture', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'pre',
		title: 'Preformatted',
		label: 'preformatted',
		icon: 'wb-widget-pre',
		description: 'A widget to insert an Preformatted text to page.',
		groups: ['basic'],
		template: '<pre></pre>',
		model: {
			html: 'class A {\n\tint a;\n}',
		},
		controller: 'WbWidgetPre', 
		controllerAs: 'ctrl', 
		isLeaf: true
	});
	$widget.newWidget({
		// widget description
		type: 'progress',
		title: 'Progress',
		description: 'A widget to add progress.',
		icon: 'wb-widget-progress',
		groups: ['basic'],
		model: {
			name: 'progress',
			style: {
				padding: '8px',
				margin: '8px',
				size: {
					height: '30px'
				}
			}
		},
		// functional properties
		template: '<progress value="22" max="100"></progress>',
		controller: 'WbWidgetProgress'
	});
	$widget.newWidget({
		type: 'q',
		title: 'q',
		label: 'q',
		icon: 'wb-widget-q',
		description: 'description',
		groups: ['basic'],
		template: '<q></q>',
		controller: 'WbWidgetQ', 
	});
	$widget.newWidget({
		type: 's',
		title: 'S',
		icon: 'wb-widget-s',
		description: 'The widget is used to define text that is no longer correct.',
		groups: ['basic'],
		template: '<s></s>',
		model: {
			html: 'Text'
		},
		controller: 'WbWidgetS', 
	});
	$widget.newWidget({
		type: 'samp',
		title: 'Samp',
		icon: 'wb-widget-samp',
		description: 'It defines sample output from a computer program.',
		groups: ['basic'],
		template: '<samp></samp>',
		model: {
			html: 'Text'
		},
		controller: 'WbWidgetSamp', 
	});
	$widget.newWidget({
		type: 'script',
		title: 'script',
		label: 'script',
		icon: 'wb-widget-script',
		description: 'description',
		groups: ['basic'],
		template: '<script></script>',
		controller: 'WbWidgetScript', 
	});
	$widget.newWidget({
		type: 'section',
		title: 'section',
		label: 'section',
		icon: 'wb-widget-section',
		description: 'description',
		groups: ['basic'],
		template: '<section></section>',
		controller: 'WbWidgetSection', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'select',
		title: 'select',
		label: 'select',
		icon: 'wb-widget-select',
		description: 'description',
		groups: ['basic'],
		template: '<select></select>',
		controller: 'WbWidgetSelect', 
	});
	$widget.newWidget({
		type: 'small',
		title: 'Small',
		icon: 'wb-widget-small',
		description: 'The widget defines smaller text.',
		groups: ['basic'],
		template: '<small></small>',
		model: {
			html: 'Small text'
		},
		controller: 'WbWidgetSmall', 
	});
	$widget.newWidget({
		type: 'source',
		title: 'Source',
		label: 'source',
		icon: 'wb-widget-source',
		description: 'This widget is used to add source in the document.',
		groups: ['basic'],
		template: '<source></source>',
		model: {
			media: '(min-width: 650px)',
			src: 'http://www.gitlab.com/am-wb/am-wb-commonhttps://unsplash.com/photos/8emNXIvrCL8/download?force=true'
		},
		controller: 'WbWidgetSource', 
	});
	$widget.newWidget({
		type: 'span',
		title: 'Span',
		icon: 'wb-widget-span',
		description: 'The widget is used to group inline-elements in a document.',
		groups: ['basic'],
		template: '<span></span>',
		model: {
			html: 'Text'
		},
		controller: 'WbWidgetSpan', 
	});
	$widget.newWidget({
		type: 'strong',
		title: 'Strong',
		icon: 'wb-widget-strong',
		description: 'The widget defines strong emphasized text.',
		groups: ['basic'],
		template: '<strong></strong>',
		model: {
			html: 'Text'
		},
		controller: 'WbWidgetStrong', 
	});
	$widget.newWidget({
		type: 'style',
		title: 'style',
		label: 'style',
		icon: 'wb-widget-style',
		description: 'description',
		groups: ['basic'],
		template: '<style></style>',
		controller: 'WbWidgetStyle', 
	});
	$widget.newWidget({
		type: 'summary',
		title: 'summary',
		label: 'summary',
		icon: 'wb-widget-summary',
		description: 'description',
		groups: ['basic'],
		template: '<summary></summary>',
		controller: 'WbWidgetSummary', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'svg',
		title: 'svg',
		label: 'svg',
		icon: 'wb-widget-svg',
		description: 'description',
		groups: ['basic'],
		template: '<svg></svg>',
		controller: 'WbWidgetSvg', 
	});
	$widget.newWidget({
		type: 'template',
		title: 'template',
		label: 'template',
		icon: 'wb-widget-template',
		description: 'description',
		groups: ['basic'],
		template: '<template></template>',
		controller: 'WbWidgetTemplate', 
		isLeaf: false
	});
	$widget.newWidget({
		// widget description
		type: 'textarea',
		title: 'Text Area field',
		description: 'A widget to get data from users.',
		icon: 'wb-widget-textarea',
		groups: ['basic'],
		model: {
			name: 'textarea',
			style: {
				padding: '8px'
			}
		},
		// help id
		help: 'http://dpq.co.ir',
		helpId: 'wb-widget-textarea',
		// functional properties
		template: '<textarea></textarea>',
		controller: 'WbWidgetTextarea',
	});
	$widget.newWidget({
		type: 'track',
		title: 'track',
		label: 'track',
		icon: 'wb-widget-track',
		description: 'description',
		groups: ['basic'],
		template: '<track></track>',
		help: 'http://dpq.co.ir/more-information-track',
		controller: 'WbWidgetTrack', 
	});
	$widget.newWidget({
		type: 'ul',
		title: 'ul',
		label: 'ul',
		icon: 'wb-widget-ul',
		description: 'description',
		groups: ['basic'],
		template: '<ul></ul>',
		controller: 'WbWidgetUl', 
		isLeaf: false
	});
	$widget.newWidget({
		type: 'video',
		title: 'Video',
		label: 'video',
		icon: 'wb-widget-video',
		description: 'This widget is used to add video in the document.',
		groups: ['basic'],
		template: '<video></video>',
		model: {
			media: '(min-width: 650px)',
			src: 'http://www.gitlab.com/am-wb/am-wb-commonhttps://unsplash.com/photos/8emNXIvrCL8/download?force=true'
		},
		controller: 'WbWidgetVideo', 
		isLeaf: false
	});
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('ngMaterialHome')
.service('$amhEditorService', function (WbObservableObject) {
	

	// load observable
	angular.extend(this, WbObservableObject.prototype);
	WbObservableObject.apply(this);

	var processors = {
			editor:[],
			workbench: []
	};

	function addProcessor(type, processor){
		processors[type].push(processor);
	}

	function connect (type, ctrl){
		disconnect(ctrl);
		ctrl.__processors = [];
		// connect processors
		_.forEach(processors[type], function(Processor){
			try{
				var processor = new Processor(ctrl);
				ctrl.__processors.push(processor);
			} catch(error){
				// TODO:
				console.log(error);
			}
		});
	}

	function disconnect(ctrl){
		if(!_.isArray(ctrl.__processors)){
			return;
		}
		_.forEach(ctrl.__processors, function(processor){
			try{
				processor.destroy();
			} catch(error){
				// TODO:
				console.log(error);
			}
		});
		delete ctrl.__processors;
	}

	/**
	 * Adds a processor to editors
	 * 
	 * @memberof $amhEditorService
	 */
	this.addEditorProcessor = function(processor){
		addProcessor('editor', processor);
	};

	/**
	 * Process the editor based on event type and event
	 * 
	 * @memberof $amhEditorService
	 */
	this.connectEditor = function(editor){
		return connect('editor', editor);
	};

	/**
	 * Removes all processor from editor
	 * 
	 * @memberof $amhEditorService
	 */
	this.disconnectEditor = function(editor){
		return disconnect(editor);
	}

	/**
	 * Adds a processor to workbench
	 * 
	 * @memberof $amhEditorService
	 */
	this.addWorkbenchProcessor = function(processor){
		addProcessor('workbench', processor);
	};

	/**
	 * Process the Workbench based on event type and event
	 * 
	 * @memberof $amhEditorService
	 */
	this.connectWorkbench= function(workbench){
		var oldValue = this.currentWorkbench;
		this.currentWorkbench = workbench;
		this.fire('workbenchChanged', {
			value: workbench,
			oldValue: oldValue
		});
		return connect('workbench', workbench);
	};

	/**
	 * Removes all processor from editor
	 * 
	 * @memberof $amhEditorService
	 */
	this.disconnectWorkbench = function(editor){
		return disconnect(editor);
	}
	
	this.getWorkbench = function(){
		return this.currentWorkbench;
	}

	this.getEditors = function(){
		return this.currentWorkbench.editors;
	}
});

///*
// * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//
//
//// TODO: hadi: move it to new module angular-material-home-seo
//angular.module('ngMaterialHome')
//
///**
// * @ngdoc service
// * @name $page
// * @description A page management service
// * 
// * 
// * 
// */
//.service('$page', function($rootScope, $rootElement) {
//
//
//
//	/*
//	 * <!-- OG -->
//	 * <meta property="og:site_name" content="$title">
//	 */
//
//	$rootScope.page = {
//		title: '',
//		description: '',
//		keywords: [],
//		links:[]
//	};
//	var page = $rootScope.page;
//
//	/**
//	 * 
//	 * @param title
//	 * @returns
//	 */
//	function setTitle(title){
//		page.title = title;
//		var head = $rootElement.find('head');
//		var elements = head.find('title');
//		var metaElement;
//		if(elements.length === 0){
//			// title element not found
//			metaElement = angular.element('<title></title>');
//			head.append(metaElement);
//		} else {
//			metaElement = angular.element(elements[0]);
//		}
//		metaElement.text(title);
//		setMeta('twitter:title', title);
//		setMetaOg('og:title', title);
//		return this;
//	}
//
//	/**
//	 * 
//	 * @returns
//	 */
//	function getTitle(){
//		return page.title;
//	}
//
//	/**
//	 * 
//	 * @param description
//	 * @returns
//	 */
//	function setDescription(description){
//		page.description = description;
//		setMeta('description', description);
//		setMeta('twitter:description', description);
//		setMetaOg('og:description', description);
//		return this;
//	}
//
//	/**
//	 * 
//	 * @returns
//	 */
//	function getDescription(){
////		return getMeta('description');
//		return page.description;
//	}
//
//	/**
//	 * 
//	 * @param keywords
//	 * @returns
//	 */
//	function setKeywords(keywords){
//		page.keywords = keywords;
//		setMeta('keywords', keywords);
//		return this;
//	}
//
//	/**
//	 * 
//	 * @returns
//	 */
//	function getKeywords(){
////		return getMeta('keywords');
//		return page.keywords;
//	}
//	
//	function setFavicon(favicon){
//		updateLink('favicon-link', {
//			href: favicon,
//			rel: 'icon'
//		});
//		setMeta('twitter:image', favicon);
//		setMetaOg('og:image', favicon);
//		return this;
//	}
//
//	function updateLink(key, data){
//		var searchkey = key.replace(new RegExp(':', 'g'), '\\:');
//		var head = $rootElement.find('head');
//		var elements = head.find('link[key='+searchkey+']');
//		var metaElement;
//		if(elements.length === 0){
//			// title element not found
//			metaElement = angular.element('<link key=\''+key+'\' />');
//			head.append(metaElement);
//		} else {
//			metaElement = angular.element(elements[0]);
//		}
//		for (var property in data) {
//			metaElement.attr(property, data[property]);
//		}
//		return this;
//	}
//
//	function setMeta(key, value){
//		var searchkey = key.replace(new RegExp(':', 'g'), '\\:');
//		var head = $rootElement.find('head');
//		var elements = head.find('meta[name='+searchkey+']');
//		var metaElement;
//		if(elements.length === 0){
//			// title element not found
//			metaElement = angular.element('<meta name=\''+key+'\' content=\'\' />');
//			head.append(metaElement);
//		} else {
//			metaElement = angular.element(elements[0]);
//		}
//		metaElement.attr('content', value);
//		return this;
//	}
//	
//	/**
//	 * Adds or set an OG meta tag to document.
//	 * Note: OG meta tag is differ than usual meta tags. Attributes of an OG meta tag are: property and content
//	 * while attributes of an usual meta tag are: name and content. 
//	 */
//	function setMetaOg(key, value){
//		var searchkey = key.replace(new RegExp(':', 'g'), '\\:');
//		var head = $rootElement.find('head');
//		var elements = head.find('meta[name='+searchkey+']');
//		var metaElement;
//		if(elements.length === 0){
//			// title element not found
//			metaElement = angular.element('<meta property=\''+key+'\' content=\'\' />');
//			head.append(metaElement);
//		} else {
//			metaElement = angular.element(elements[0]);
//		}
//		metaElement.attr('content', value);
//		return this;
//	}
//
//	/**
//	 * Adds or set an OG meta tag to document.
//	 * Note: OG meta tag is differ than usual meta tags. Attributes of an OG meta tag are: property and content
//	 * while attributes of an usual meta tag are: name and content. 
//	 */
//	function setMetaOg(key, value){
//		var searchkey = key.replace(new RegExp(':', 'g'), '\\:');
//		var head = $rootElement.find('head');
//		var elements = head.find('meta[name='+searchkey+']');
//		var metaElement;
//		if(elements.length === 0){
//			// title element not found
//			metaElement = angular.element('<meta property=\''+key+'\' content=\'\' />');
//			head.append(metaElement);
//		} else {
//			metaElement = angular.element(elements[0]);
//		}
//		metaElement.attr('content', value);
//		return this;
//	}
//	
//	/*
//	 * Service struct
//	 */
//	return {
//		// Init
//		setTitle: setTitle,
//		getTitle: getTitle,
//		setDescription: setDescription,
//		getDescription: getDescription,
//		setKeywords: setKeywords,
//		getKeywords: getKeywords,
//		setFavicon: setFavicon,
//		setMeta: setMeta,
//		setLink: updateLink
//	};
//});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')

/**
 * @ngdoc Services
 * @name $settings
 * @description Manage settings panel 
 * 
 * 
 */
.service('$settings', function() {
	
	/**
	 * Setting page storage
	 * 
	 */
	var settingPages = [];

	var notFound = {
			label : 'Settings not found',
			templateUrl : 'views/settings/wb-notfound.html'
	};

	function pageMatchWith(page, widgetDescription){
		if(_.isUndefined(page.targets) || page.targets.length === 0){
			return true;
		}
		for(var i = 0; i < page.targets.length; i++){
			var patt = new RegExp(page.targets[i]);
			if(patt.test(widgetDescription.type)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Fetchs a setting page with the given type
	 * 
	 * @memberof $settings
	 * @param model
	 * @returns
	 */
	this.getPage = function (type) {
		var pageResult = notFound;
		_.forEach(settingPages, function(settingPage){
			if (type === settingPage.type) {
				pageResult = settingPage;
			}
		});
		return pageResult;
	};

	/**
	 * 
	 * @memberof $settings
	 * @param page type
	 * @returns
	 */
	this.removePage = function (type) {
		settingPages=  _.remove(settingPages, function(page) {
			return type === page.type;
		});
		this.settingPagesCach = [];
		return this;
	};

	/**
	 * Adds new setting page.
	 * 
	 * @memberof $settings
	 * @returns
	 */
	this.addPage = function (page) {
		settingPages.push(page);
		this.settingPagesCach = [];
		return this;
	};

	/**
	 * Set new setting page.
	 * 
	 * @memberof $settings
	 * @returns
	 */
	this.setPage = function (page) {
		this.remvoePage(page.type);
		return this.addPage(page);
	};

	/**
	 * Finds and lists all setting pages.
	 * 
	 * @memberof $settings
	 * @returns
	 */
	this.getPages = function () {
		return _.clone(settingPages);
	};

	/**
	 * Finds and lists all setting pages.
	 * 
	 * @deprecated
	 * @memberof $settings
	 * @returns
	 */
	this.pages = this.getPages;

	/**
	 * Defines default settings for widget
	 * 
	 * @memberof $settings
	 * @param widget
	 * @returns
	 */
	this.getSettingsFor = function (widgetDescription) {
		// if it was cached before
		this.settingPagesCach = this.settingPagesCach || {};
		if(_.has(this.settingPagesCach, widgetDescription.type)){
			return this.settingPagesCach[widgetDescription.type];
		}

		// create list
		var widgetSettings = [];
		_.forEach(settingPages, function(page){
			if(pageMatchWith(page, widgetDescription)){
				widgetSettings.push(page);
			}
		});

		// put into cache
		this.settingPagesCach[widgetDescription.type] =  widgetSettings;

		return widgetSettings;
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')
/**
 * A service is used to create unique ID's, this prevents duplicate ID's if there are multiple editors on screen.
 */
.service('uiTinymceService', function() {
    
    var UITinymceService = function() {
        var ID_ATTR = 'ui-tinymce';
        // uniqueId keeps track of the latest assigned ID
        var uniqueId = 0;
        // getUniqueId returns a unique ID
        var getUniqueId = function() {
            uniqueId ++;
            return ID_ATTR + '-' + uniqueId;
        };
        // return the function as a public method of the service
        return {
            getUniqueId: getUniqueId
        };
    };
    // return a new instance of the service
    return new UITinymceService();
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


//TODO: hadi: move it to new module angular-material-home-seo
angular.module('ngMaterialHome')

.service('uuid4', function() {
	/**! http://stackoverflow.com/a/2117523/377392 */
	var fmt = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
	this.generate = function() {
		return fmt.replace(/[xy]/g, function(c) {
			var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
			return v.toString(16);
		});
	};
}); 

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @ngdoc service
 * @name $wbmodel
 * @description Utility of a model
 * 
 */
function WbModelService($q, $http, $wbUtil) {
    // init data
    this.$q = $q;
    this.$http = $http;
    this.$wbUtil = $wbUtil;
}

/**
 * Embeds all image with data url
 * 
 * @memberof $wbmodel
 */
WbModelService.prototype.embedImagesDeep = function (model) {
    var jobs = [];
    // embed images
    jobs.push(this.embedImages(model));

    var service = this;
    // go to child
    if (model.type === 'Group') {
	angular.forEach(model.children, function (subModel) {
	    jobs.push(service.embedImages(subModel));
	});
    }
    return this.$q.all(jobs);
};

/**
 * Embed images
 * 
 * @memberof $wbmodel
 */
WbModelService.prototype.embedImages = function (model) {
    var jobs = [];
    var job;
    var $http = this.$http;

    // background image
    var url = this.getBackgroundImage(model);
    var service = this;
    if (url) {
	job = this.urlToDataImage(url)
		.then(function (res) {
		    service.setBackgroundImage(model, res.data);
		});
	jobs.push(job);
    }

    // image
    url = this.getImageUrl(model);
    if (url) {
	job = this.urlToDataImage(url)
		.then(function (res) {
		    service.setImageUrl(model, res.data);
		});
	jobs.push(job);
    }

    // carousel
    if (model.type == 'am-wb-carouselNavigator') {
	angular.forEach(model.slides, function (slide) {
	    jobs.push(service.urlToDataImage(slide.image)
		    .then(function (url) {
			slide.image = url;
		    }));
	});
    }

    return this.$q.all(jobs);
};

/**
 * Converts URL to image data
 * 
 * @memberof $wbmodel
 */
WbModelService.prototype.urlToDataImage = function (url) {
    if (url.startsWith('data:')) {
	return $q.when(url);
    }
    return this.$http({
	method: 'GET',
	url: url,
	responseType: "arraybuffer",
	transformResponse: function (data) {
	    var uint8View = new Uint8Array(data);
	    return 'data:image/*;base64,' + btoa(uint8View.reduce((data, byte) => data + String.fromCharCode(byte), ''));
	}
    });
};

/**
 * Get background image url or null
 * 
 * @memberof $wbmodel
 */
WbModelService.prototype.getBackgroundImage = function (model) {
    if (!model.style) {
	return;
    }
    if (!model.style.background) {
	return;
    }
    return model.style.background.image;
};

/**
 * Sets background image URL
 * 
 * @memberof $wbmodel
 */
WbModelService.prototype.setBackgroundImage = function (model, url) {
    if (!model.style) {
	model.style = {};
    }
    if (!model.style.background) {
	model.style.background = {};
    }
    model.style.background.image = url;
};

WbModelService.prototype.getImageUrl = function (model) {
    if (!model.type == 'Imgage') {
	return;
    }
    return model.url;
};

WbModelService.prototype.setImageUrl = function (model, url) {
    if (!model.type == 'Imgage') {
	return;
    }
    model.url = url;
};

WbModelService.prototype.copyToClipboard = function (model) {
    /*
     * TODO: Masood, 2019: There is also another solution but now it doesn't work because of browsers problem
     * A detailed solution is presented in:
     * https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript 
     */ 
    var js = JSON.stringify(model);
    var fakeElement = document.createElement('textarea');
    fakeElement.value = js;
    document.body.appendChild(fakeElement);
    fakeElement.select();
    document.execCommand('copy');
    document.body.removeChild(fakeElement);
    return;
};

WbModelService.prototype.pasteFromClipboard = function () {
    var def = this.$q.defer();
    var ctrl = this;
    navigator.clipboard.readText()//
	    .then(function (data) {
		    try {
			var model = JSON.parse(data);
			model = ctrl.$wbUtil.clean(model);
			def.resolve(model);
		    } catch(error){
			alert('Failed to paste.');
		    }
	    }, function (e) {
		alert(e);
	    });
    return def.promise;
};


//TODO: hadi: move it to new module angular-material-home-seo
angular.module('ngMaterialHome').service('$wbmodel', function ($q, $http, $wbUtil) {
    var service = new WbModelService($q, $http, $wbUtil);
    return service;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('ngMaterialHome')
/**
 * @ngdoc service
 * @name $wbModuleManager
 * @description Utility of a model
 * 
 */
.service('$wbModuleManager', function ($dispatcher) {
	

	this.setState = function(state){
		this.state = state;
	};

	this.getState = function(){
		return this.state;
	};


	this.removeModules = function(){

		return $q.resolve(this);
	}

	this.loadModules = function(){

		return $q.resolve(this);
	}

	this.getModules = function(){};
	this.addModule = function(module){};
	this.removeModule = function(module){};
	this.hasModule = function(module){
		return false;
	};



	var ctrl = this;
	function listenToApp(event){
		if(event.type !== 'update' || !$app.getState().startsWith('ready')){
			return;
		}
		ctrl.setState('loading');
		ctrl.removeModules()
		.then(function(){
			ctrl.loadModules();
		})
		.finally(function(){
			ctrl.setState('ready');
		});
	}

	// init the service
	$dispatcher.on('/app/state', listenToApp);
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingPageCtrl
 * @description Manages settings page
 * 
 * Manages settings pages.
 * 
 * This is an abstract implementation of a setting page and contains list of 
 * utilities.
 * 
 */
.controller('WbSettingPageCtrl', function (WbObservableObject) {
	// extend from observable object
	angular.extend(this, WbObservableObject.prototype);
	WbObservableObject.apply(this);
	/*
	 * Attributes
	 */
	this.widgets = [];
	this.attributes = [];
	this.attributesValue = {};
	this.styles = [];
	this.stylesValue = {};
	this.callbacks = {
			widgetChanged: []
	};

	/********************************************************
	 * Widget management
	 ********************************************************/
	/**
	 * Sets new widgets list into the setting page
	 * 
	 * Note: the old widgets will be removed from the view and the
	 * new one is connect.
	 * 
	 * @memberof WbSettingPageCtrl
	 * @param widget {Widget} to track
	 */
	this.setWidget = function (widgets) {
		widgets =  widgets || [];
		if(!_.isArray(widgets)){
			widgets = [widgets];
		}
		var oldWidgets = this.widgets;
		this.disconnect();
		this.widgets = widgets;
		this.connect();
		// load values
		this.loadAttributes();
		this.loadStyles();
		// propagate the change
		this.fire('widgetChanged', {
			value: widgets,
			oldValue: oldWidgets
		});
	};

	/**
	 * Gets the current widgets list 
	 * 
	 * @memberof WbSettingPageCtrl
	 * @return the current widget
	 */
	this.getWidgets = function () {
		return this.widgets;
	};

	/**
	 * Checks if the current widget can contain others
	 * 
	 * @memberof WbSettingPageCtrl
	 * @return true if the widget is a container
	 */
	this.isContainerWidget = function(){
		var widgets = this.getWidgets();
		if(widgets.length === 0 || widgets.length > 1){
			return false;
		}
		var widget = widgets[0];
		return !widget.isLeaf();
	};

	/**
	 * Removes listeners to the widget
	 * 
	 * The controller track the widget changes by adding listener into it. This
	 * function removes all listener.
	 * 
	 * @memberof WbSettingPageCtrl
	 */
	this.disconnect = function(){
		var widgets = this.getWidgets();
		if(_.isEmpty(widgets) || !_.isFunction(this.widgetListener)){
			return;
		}
		var ctrl = this;
		_.forEach(widgets, function(widget){
			widget.off('modelUpdated', ctrl.widgetListener);
		});
	};

	/**
	 * Adds listeners to the widget
	 * 
	 * @memberof WbSettingPageCtrl
	 */
	this.connect= function(){
		var widgets = this.getWidgets();
		if(_.isEmpty(widgets)){
			return;
		}
		if(_.isEmpty(this.widgetListeners)){
			var ctrl = this;
			this.widgetListener = function($event) {
				if(ctrl.isSilent()){
					return;
				}
				if($event.type === 'modelUpdated'){
					ctrl.updateValues($event.key);
				}
			};
		}
		// Adding listeners
		_.forEach(widgets, function(widget){
			widget.on('modelUpdated', ctrl.widgetListener);
		});
	};


	this.updateValues = function(keys){
		if(_.isUndefined(keys)){
			return;
		}
		if(!_.isArray(keys)){
			keys = [keys];
		}
		var widgets = this.widgets;
		var firstFit = widgets.length > 1;
		var ctrl = this;
		var attrs = this.attributesValue;
		var styles = this.stylesValue;
		_.forEach(keys, function(key){
			_.forEach(widgets, function(widget){
				// load attribute
				if(_.includes(ctrl.attributes, key)){
					if(!firstFit || _.isUndefined(attrs[key])){
						attrs[key] = widget.getModelProperty(key);
					}
					return;
				}
				// load style
				if(!key.startsWith('style.')){
					return;
				}
				var stylekey =  key.substring(6);
				if(_.includes(ctrl.styles, stylekey)){
					if(!firstFit || _.isUndefined(styles[key])){
						styles[stylekey] = widget.getModelProperty(key);
					}
				}
			});
		});
	};

	/***************************************************************
	 * Track widget changes
	 ***************************************************************/

	/*
	 * INTERNAL
	 */
	this.loadAttributes= function(){
		// 0- clean
		this.attributesValue = {};
		this.updateValues(this.attributes);
	};

	/*
	 * INTERNAL
	 */
	this.loadStyles= function(){
		// 0- clean
		this.stylesValue = {};
		this.updateValues(_.map(this.styles, function(styleKey){
			return 'style.'+styleKey;
		}));
	};

	this.setSilent = function(silent){
		this._silent = silent;
	};

	this.isSilent = function(){
		return this._silent;
	};

	/* **************************************************************
	 * attribute utilities
	 * **************************************************************/
	/**
	 * Adds list of attributes to track
	 * 
	 * @memberof WbSettingPageCtrl
	 * @param {string[]} attributes to track
	 */
	this.trackAttributes = function(attributes){
		this.attributes = attributes || [];
		this.loadAttributes();
	};

	/**
	 * Adds key,value as attribute to the page
	 * 
	 * @memberof WbSettingPageCtrl
	 * @param {string} key to use
	 * @param {string} value to set for the key
	 */
	this.setAttribute = function (key, value) {
		if (_.isEmpty(this.widgets)) {
			return;
		}
		this.setSilent(true);
		try{
			this.attributesValue[key] = value;
			_.forEach(this.widgets, function(widget){
				widget.setModelProperty(key, value);
			});
		} finally {
			this.setSilent(false);
		}
	};

	/* **************************************************************
	 * style utilities
	 * **************************************************************/
	/**
	 * Adds list of styles to track
	 * 
	 * @memberof WbSettingPageCtrl
	 * @param {string[]} styles to track
	 */
	this.trackStyles = function(styles){
		this.styles = styles || [];
		this.loadStyles();
	};

	/**
	 * Adds key,value as style to the widget
	 * 
	 * @memberof WbSettingPageCtrl
	 * @param {string} key to use
	 * @param {string} value to set for the key
	 */
	this.setStyle = function (key, value) {
		if (_.isEmpty(this.widgets)) {
			return;
		}
		this.stylesValue[key] = value;
		this.setSilent(true);
		try{
			_.forEach(this.widgets, function(widget){
				widget.setModelProperty('style.' + key, value);
			});
		} finally {
			this.setSilent(false);
		}
	};

});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('am-wb-seen-core')
/**
 * @ngdoc Settings Ctrl
 * @name WbSeenCollectionSettingCtrl
 * @description Collection setting controller
 * 
 */
.controller('WbSeenCollectionSettingCtrl', function () {
    'use strict';
    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackAttributes(['url', 'filters', 'sorts', 'query', 'properties', 'template']);
        this.trackStyles([]);
    };


    /**
     * Add filter to the current filters
     * 
     * @memberof WbSeenCollectionSettingCtrl
     */
    this.addFilter = function () {
    	var filters = this.attributesValue.filters || [];
        filters.push({
            key: '',
            value: ''
        });
        this.setAttribute('filters', filters);
    };

    /**
     * Remove filter to the current filters
     * 
     * @memberof WbSeenCollectionSettingCtrl
     */
    this.removeFilter = function (index) {
    	var filters = this.attributesValue.filters || [];
    	filters.splice(index, 1);
        this.setAttribute('filters', filters);
    };

    /**
     * Add sort to the current sorts
     * 
     * @memberof WbSeenCollectionSettingCtrl
     */
    this.addSort = function () {
    	var sorts = this.attributesValue.sorts || [];
        sorts.push({
            key: '',
            order: ''
        });
        this.setAttribute('sorts', sorts);
    };

    /**
     * Removes filter 
     * 
     * @param index {in} of the sort
     * @memberof WbSeenCollectionSettingCtrl
     */
    this.removeSort = function (index) {
    	var sorts = this.attributesValue.sorts || [];
    	sorts.splice(index, 1);
        this.setAttribute('sorts', sorts);
    };
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('am-wb-seen-core')
/**
 * @ngdoc Widgets
 * @name WbSeenImportSettingCtrl
 * @description Collection setting controller
 * 
 */
.controller('WbSeenImportSettingCtrl', function () {
    'use strict';
    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackAttributes(['url']);
    };
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingStyleAnimationCtrl
 * @description Manage style animations
 * 
 */
.controller('WbSettingStyleAnimationCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackStyles([
            'animation',
            'animationName',
            'animationDuration',
            'animationDelay',
            'animationDirection',
            'animationIterationCount',
            'animationTimingFunction',
            'animationFillMode',
            'animationPlayState',
            ]);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingStyleBackgroundCtrl
 * @description Manage general settings of a widget
 * 
 *  This controller controls the background attribute. If the user choose an image for 
 * the background then sets a default values to the background property. These values are used to show 
 * the image in a suitable form; and if the user remove the background image then remove those values 
 * from the background.
 * 
 */
.controller('WbSettingStyleBackgroundCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackStyles([
            // id
            'background',
            'backgroundColor',
            'backgroundImage',
            'backgroundPosition',
            'backgroundSize',
            'backgroundRepeat',
            'backgroundOrigin',
            'backgroundClip',
            'backgroundAttachment'
            ]);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingStyleBorderCtrl
 * @description Manage Widget img 
 * 
 */
.controller('WbSettingStyleBorderCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackStyles([
            'border',
            'borderWidth',
            'borderStyle',
            'borderColor',
            'borderCollapse',
            'borderSpacing',

            'borderTop',
            'borderTopWidth',
            'borderTopStyle',
            'borderTopColor',

            'borderRight',
            'borderRightWidth',
            'borderRightStyle',
            'borderRightColor',

            'borderBottom',
            'borderBottomWidth',
            'borderBottomStyle',
            'borderBottomColor',

            'borderLeft',
            'borderLeftWidth',
            'borderLeftStyle',
            'borderLeftColor',

            'borderRadius',
            'borderTopRightRadius',
            'borderTopLeftRadius',
            'borderBottomLeftRadius',
            'borderBottomRightRadius',

            'borderImage',
            'borderImageSource',
            'borderImageSlice',
            'borderImageWidth',
            'borderImageOutset',
            'borderImageRepeat',

            'outline',
            'outlineOffset',
            'outlineWidth',
            'outlineStyle',
            'outlineColor',

            'boxDecorationBreak',
            'boxShadow',
            'boxSizing'
            ]);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingStyleGeneralCtrl
 * @description Manage a widget with html text.
 * 
 * Most of textual widgets (such as h1..h6, p, a, html) just used html
 * text in view. This controller are about to manage html attribute of
 * a widget.
 * 
 */
.controller('WbSettingStyleGeneralCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackStyles([
            'opacity',
            'visibility',
            'color',
            'mixBlendMode',
            'isolation',

            'cursor',
            'pointerEvents',
            ]);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * 
 * @ngdoc Widget Settings
 * @name WbSettingStyleLayoutCtrl
 * @description Manages element layout
 * 
 * Layout is consists of the following attributes for a group:
 * 
 * <ul>
 *     <li>direction</li>
 *     <li>direction-inverse</li>
 *     <li>wrap</li>
 *     <li>wrap-inverse</li>
 *     <li>align</li>
 *     <li>justify</li>
 * </ul>
 * 
 * and following ones for a widget (or group):
 * 
 * <ul>
 *     <li>grow</li>
 *     <li>shrink</li>
 *     <li>order</li>
 * </ul>
 * 
 * See the layout documents for more details.
 * 
 * @see wb-layout
 */
.controller('WbSettingStyleLayoutCtrl', function (/*$scope, $element*/) {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackStyles([
            'display',
            'order',
            'zIndex',
            'clear',
            'float',
            // Position
            'position',
            'bottom',
            'left',
            'right',
            'top',
            // overflow
            'overflow',
            'overflowX',
            'overflowY',
            'scrollBehavior',
            // Print
            'pageBreakAfter',
            'pageBreakBefore',
            'pageBreakInside',
            // Flex
            'alignContent',
            'alignItems',
            'alignSelf',
            'justifyContent',
            'flex',
            'flexBasis',
            'flexDirection',
            'flexGrow',
            'flexShrink',
            'flexWrap',
            // grid
            'grid',
            'gridArea',
            'gridAutoColumns',
            'gridAutoFlow',
            'gridAutoRows',
            'gridColumn',
            'gridColumnEnd',
            'gridColumnGap',
            'gridColumnStart',
            'gridGap',
            'gridRow',
            'gridRowEnd',
            'gridRowGap',
            'gridRowStart',
            'gridTemplate',
            'gridTemplateAreas',
            'gridTemplateColumns',
            'gridTemplateRows',
            // column view
            'columns',
            'columnWidth',
            'columnCount',
            'columnSpan',
            'columnFill',
            'columnGap',
            'columnRule',
            'columnRuleColor',
            'columnRuleStyle',
            'columnRuleWidth',
            ]);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingStyleMediaCtrl
 * @description Edit text style of a widget
 * 
 */
.controller('WbSettingStyleMediaCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackStyles([
            'clip',
            'clipPath',
            'filter',
            'objectFit',
            'objectPosition',
            ]);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingStyleSizeCtrl
 * @description Manage Widget 
 * 
 */
.controller('WbSettingStyleSizeCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackStyles([
            'margin',
            'marginTop',
            'marginRight',
            'marginBottom',
            'marginLeft',
            'padding',
            'paddingTop',
            'paddingRight',
            'paddingBottom',
            'paddingLeft',
            'resize',
            'height',
            'maxHeight',
            'minHeight',
            'width',
            'maxWidth',
            'minWidth',
            ]);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingStyleTextCtrl
 * @description Edit text style of a widget
 * 
 */
.controller('WbSettingStyleTextCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackStyles([
            // writing
            'hyphens',
            'letterSpacing',
            'lineHeight',
            'quotes',
            'tabSize',
            'verticalAlign',
            'whiteSpace',
            'wordBreak',
            'wordSpacing',
            'wordWrap',
            'writingMode',
            'userSelect',
            // Text
            'textAlign',
            'textAlignLast',
            'textDecoration',
            'textDecorationColor',
            'textDecorationLine',
            'textDecorationStyle',
            'textIndent',
            'textJustify',
            'textOverflow',
            'textShadow',
            'textTransform',
            // Local
            'direction',
            'unicodeBidi',
            // font
            'font',
            'fontFamily',
            'fontKerning',
            'fontSize',
            'fontSizeAdjust',
            'fontStretch',
            'fontStyle',
            'fontVariant',
            'fontWeight',
            ]);
    };


});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingACtrl
 * @description Manage Widget A 
 * 
 */
.controller('WbSettingACtrl', function () {

	/*
	 * Initial the setting editor
	 */
	this.init = function () {
		this.trackAttributes([
			// id
			'download',
			'href',
			'hreflang',
			'media',
			'ping',
			'referrerpolicy',
			'rel',
			'target',
			'type',
			]);
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Settings
 * @name WbSettingWidgetAudioCtrl
 * @description Manage IFrame widget 
 * 
 */
.controller('WbSettingWidgetAudioCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackAttributes(['autoplay', 'controls',
            'loop', 'muted', 'preload', 'src']);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Settings
 * @name WbSettingWidgetFormCtrl
 * @description Manage Widget form
 * 
 */
.controller('WbSettingWidgetFormCtrl', function () {

	/*
	 * Initial the setting editor
	 */
	this.init = function () {
		this.trackAttributes(['acceptCharset', 'action', 'autocomplete', 'off',
			'enctype', 'method', 'name', 'novalidate', 'target']);
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Settings
 * @name WbSettingGeneralCtrl
 * @description Manage Widget general attributes
 * 
 */
.controller('WbSettingGeneralCtrl', function () {
    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackAttributes([
            'id',  
            'accesskey', 
            'title',   
            'name',
            'tabindex',  
            'class',  
            'hidden',  
            'contenteditable', 
            'draggable',   
            'dropzone',
            'dir', 
            'lang',
            'spellcheck',   
            'translate',  
            ]);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Settings
 * @name WbSettingWidgetIFrameCtrl
 * @description Manage IFrame widget 
 * 
 */
.controller('WbSettingWidgetIFrameCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackAttributes(['name', 'src', 'srcdoc', 'sandbox']);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Settings
 * @name WbSettingWidgetImgCtrl
 * @description Manage Widget img 
 * 
 */
.controller('WbSettingWidgetImgCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackAttributes([
            // id
            'alt',
            'crossorigin',
            'height',
            'ismap',
            'longdesc',
            'size',
            'src',
            'srcset',
            'usemap',
            'width',
        ]);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Settings
 * @name WbSettingWidgetInputCtrl
 * @description Manage Widget input
 * 
 */
.controller('WbSettingWidgetInputCtrl', function () {

	/*
	 * Initial the setting editor
	 */
	this.init = function () {
		this.trackAttributes(['accept', 'alt',
			'autocomplete', 'autofocus', 'checked',
			'dirname', 'disabled', 
			'form', 'formaction', 'formenctype', 'formmethod', 'formnovalidate', 'formtarget',
			'height', 'list','max',
			'maxlength', 'min', 'multiple', 'name',
			'pattern', 'placeholder', 'readonly',
			'required', 'size', 'src', 'step', 'inputType',
			'value', 'width']);
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingWidgetMetaCtrl
 * @description Manage a widget with html text.
 * 
 * 
 */
.controller('WbSettingWidgetMetaCtrl', function () {

	/*
	 * Initial the setting editor
	 */
	this.init = function () {
		this.trackAttributes(['charset', 'content', 'httpEquiv', 'name']);
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Controllers
 * @name WbSettingWidgetMicrodataCtrl
 * @description Manage a widget with html text.
 * 
 * Most of textual widgets (such as h1..h6, p, a, html) just used html
 * text in view. This controller are about to manage html attribute of
 * a widget.
 * 
 */
.controller('WbSettingWidgetMicrodataCtrl', function () {

	/*
	 * Initial the setting editor
	 */
	this.init = function () {
		this.trackAttributes([
			'itemscope',
			'itemtype',
			'itemprop',
			'itemid',
			'itemref',
			// extra attributes
			'value',
			'content',
			]);
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Settings
 * @name WbSettingWidgetPictureCtrl
 * @description Manage IFrame widget 
 * 
 */
.controller('WbSettingWidgetPictureCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackAttributes(['alt', 'crossorigin', 'height',
            'hspace', 'ismap', 'longdesc', 'sizes', 'src',
            'usemap', 'width']);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('am-wb-core')//

/**
 * @ngdoc Settings
 * @name WbSettingWidgetSourceCtrl
 * @description Manage IFrame widget 
 * 
 */
.controller('WbSettingWidgetSourceCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackAttributes(['src', 'srcset', 'media', 'sizes', 'type']);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('am-wb-core')//

/**
 * @ngdoc Settings
 * @name WbSettingWidgetVideoCtrl
 * @description Manage IFrame widget 
 * 
 */
.controller('WbSettingWidgetVideoCtrl', function () {

    /*
     * Initial the setting editor
     */
    this.init = function () {
        this.trackAttributes(['autoplay', 'controls', 'height',
            'loop', 'muted', 'poster', 'preload', 'src',
            'usemap', 'width']);
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Processor
 * @name WbProcessorDnd
 * @description Widget processor
 * 
 */
.factory('WbProcessorDnd', function (WbProcessorAbstract, $widget) {

    // In standard-compliant browsers we use a custom mime type and also encode the dnd-type in it.
    // However, IE and Edge only support a limited number of mime types. The workarounds are described
    // in https://github.com/marceljuenemann/angular-drag-and-drop-lists/wiki/Data-Transfer-Design
//    var MIME_TYPE = 'application/x-dnd';
//    var EDGE_MIME_TYPE = 'application/json';
    var MSIE_MIME_TYPE = 'Text';

    // All valid HTML5 drop effects, in the order in which we prefer to use them.
    var ALL_EFFECTS = ['move', 'copy', 'link'];

    // While an element is dragged over the list, this placeholder element is inserted
    // at the location where the element would be inserted after dropping.
    var placeholder = angular.element('<div class="wbDndPlaceholder"></div>');
    var placeholderNode = placeholder[0];
    placeholder.remove();


    /**
     * We use the position of the placeholder node to determine at which position of the array the
     * object needs to be inserted
     */
    function getPlaceholderIndex(widget) {
        var listNode = widget.getElement()[0];
        return Array.prototype.indexOf.call(listNode.children, placeholderNode);
    }

    /**
     * Small helper function that cleans up if we aborted a drop.
     */
    function stopDragover() {
        placeholder.remove();
        return true;
    }

    /**
     * Filters an array of drop effects using a HTML5 effectAllowed string.
     */
    function filterEffects(effects, effectAllowed) {
        if (effectAllowed === 'all') {
            return effects;
        }
        return effects.filter(function(effect) {
            return effectAllowed.toLowerCase().indexOf(effect) !== -1;
        });
    }

    /**
     * Given the types array from the DataTransfer object, returns the first valid mime type.
     * A type is valid if it starts with MIME_TYPE, or it equals MSIE_MIME_TYPE or EDGE_MIME_TYPE.
     */
    function getMimeType(types) {
        if (!types){
            return MSIE_MIME_TYPE; // IE 9 workaround.
        }
        var converters = $widget.getConverters();
        for (var i = 0; i < converters.length; i++) {
            var converter = converters[i];
            if (_.includes(types, converter.getMimetype())){
                return converter.getMimetype();
            }
        }
        return null;
    }


//  /**
//  * Determines the type of the item from the dndState, or from the mime type for items from
//  * external sources. Returns undefined if no item type was set and null if the item type could
//  * not be determined.
//  */
//  function getItemType(mimeType) {
////if (dndState.isDragging){
////return dndState.itemType || undefined;
////}
//  if (mimeType == MSIE_MIME_TYPE || mimeType == EDGE_MIME_TYPE){
//  return null;
//  }
//  return (mimeType && mimeType.substr(MIME_TYPE.length + 1)) || undefined;
//  }


    /**
     * Determines which drop effect to use for the given event. In Internet Explorer we have to
     * ignore the effectAllowed field on dataTransfer, since we set a fake value in dragstart.
     * In those cases we rely on dndState to filter effects. Read the design doc for more details:
     * https://github.com/marceljuenemann/angular-drag-and-drop-lists/wiki/Data-Transfer-Design
     */
    function getDropEffect(event, ignoreDataTransfer) {
        var effects = ALL_EFFECTS;
        if (!ignoreDataTransfer) {
            effects = filterEffects(effects, event.dataTransfer.effectAllowed);
        }
//      if (dndState.isDragging) {
//      effects = filterEffects(effects, dndState.effectAllowed);
//      }
//      if (attr.dndEffectAllowed) {
//      effects = filterEffects(effects, attr.dndEffectAllowed);
//      }
        // MacOS automatically filters dataTransfer.effectAllowed depending on the modifier keys,
        // therefore the following modifier keys will only affect other operating systems.
        if (!effects.length) {
            return 'none';
        } else if (event.ctrlKey && effects.indexOf('copy') !== -1) {
            return 'copy';
        } else if (event.altKey && effects.indexOf('link') !== -1) {
            return 'link';
        } else {
            return effects[0];
        }
    }

    function dragstart(event) {
        event = event.originalEvent || event;
        // 1 - load state, data and etc.
        var widget = $widget.widgetFromElement(event.currentTarget);

        widget.$$dndState = widget.$$dndState || {};

        // Initialize global state.
        widget.$$dndState.isDragging = true;
        widget.$$dndState.itemType = widget.getType();

        // Set the allowed drop effects. See below for special IE handling.
        widget.$$dndState.dropEffect = 'none';
        widget.$$dndState.effectAllowed = /*dndEffectAllowed ||*/ ALL_EFFECTS[0];

        // 2 - convert and put data
        event.dataTransfer.effectAllowed =  widget.$$dndState.effectAllowed;
        var converters = $widget.getConverters();
        _.forEach(converters, function(converter){
            try {
                event.dataTransfer.setData(converter.getMimetype(), converter.encode(widget));
            } catch (e) {
                // TODO: maso, 2019: log errors
//                console.log('fail to convert to :' + converter.getMimetype(), e);
            }
        });

        // Internet Explorer and Microsoft Edge don't support custom mime types, see design doc:
        // https://github.com/marceljuenemann/angular-drag-and-drop-lists/wiki/Data-Transfer-Design
////    var item = scope.$eval(attr.dndDraggable);
////    var mimeType = MIME_TYPE + (dndState.itemType ? ('-' + dndState.itemType) : '');
////    try {
////    event.dataTransfer.setData(mimeType, angular.toJson(item));
////    } catch (e) {
////    // Setting a custom MIME type did not work, we are probably in IE or Edge.
////    var data = angular.toJson({item: item, type: dndState.itemType});
////    try {
////    event.dataTransfer.setData(EDGE_MIME_TYPE, data);
////    } catch (e) {
////    // We are in Internet Explorer and can only use the Text MIME type. Also note that IE
////    // does not allow changing the cursor in the dragover event, therefore we have to choose
////    // the one we want to display now by setting effectAllowed.
////    var effectsAllowed = filterEffects(ALL_EFFECTS, dndState.effectAllowed);
////    event.dataTransfer.effectAllowed = effectsAllowed[0];
////    event.dataTransfer.setData(MSIE_MIME_TYPE, data);
////    }
////    }

        // Try setting a proper drag image if triggered on a dnd-handle (won't work in IE).
        if (event._dndHandle && event.dataTransfer.setDragImage) {
            event.dataTransfer.setDragImage(widget.getElement()[0], 0, 0);
        }

        event.stopPropagation();
    }

    function dragend(event) {
        // Clean up
        placeholder.remove();
        event = event.originalEvent || event;
        var widget = $widget.widgetFromElement(event.currentTarget);
        widget.$$dndState.isDragging = false;
        widget.$$dndState.callback = undefined;

        // do action
        var effect = event.dataTransfer.dropEffect;
        switch(effect) {
        case 'move':
            widget.delete();
            break;
        case 'copy':
        case 'link':
        case 'canceled':
//            console.log('not supported');
            break;
        }

        event.stopPropagation();
    }

    function dragenter(event) {
        // Calculate list properties, so that we don't have to repeat this on every dragover event.
        event = event.originalEvent || event;
        var widget = $widget.widgetFromElement(event.currentTarget);
        if(widget.isLeaf()){
            return true;
        }
        var mimeType = getMimeType(event.dataTransfer.types);
        if (!mimeType){
            return true;
        }
        event.preventDefault();
    }

    function dragover(event) {
        event = event.originalEvent || event;
        var widget = $widget.widgetFromElement(event.currentTarget);

        if(widget.isLeaf()){
            return true;
        }
        // Check whether the drop is allowed and determine mime type.
        var mimeType = getMimeType(event.dataTransfer.types);
//      var itemType = getItemType(mimeType);
        if (!mimeType) {
            return true;
        }

        var element = widget.getElement();
        var listNode = element[0];

        // Make sure the placeholder is shown, which is especially important if the list is empty.
        if (placeholderNode.parentNode !== listNode) {
            element.append(placeholder);
        }

        var listItemNode = null;
        var horizontal;
        var rect;
        if (widget.isLeaf()) {
            // Try to find the node direct directly below the list node.
            listItemNode = event.target;
            while (listItemNode.parentNode !== listNode && listItemNode.parentNode) {
                listItemNode = listItemNode.parentNode;
            }

            if (listItemNode.parentNode === listNode && listItemNode !== placeholderNode) {
                // If the mouse pointer is in the upper half of the list item element,
                // we position the placeholder before the list item, otherwise after it.
                rect = listItemNode.getBoundingClientRect();
                horizontal = true; // TODO:
                var isFirstHalf;
                if (horizontal) {
                    isFirstHalf = event.clientX < rect.left + rect.width / 2;
                } else {
                    isFirstHalf = event.clientY < rect.top + rect.height / 2;
                }
                listNode.insertBefore(placeholderNode,
                        isFirstHalf ? listItemNode : listItemNode.nextSibling);
            }
        } else {
            horizontal = widget.isHorizontal();
            for(var i = 0; i < listNode.childNodes.length; i++){
                var node = listNode.childNodes[i];
                rect = node.getBoundingClientRect();
                if (horizontal) {
                    if(rect.left > event.clientX) {
                        listItemNode = node;
                        break;
                    }
                } else {
                    if(rect.top > event.clientY) {
                        listItemNode = node;
                        break;
                    }
                }
            }
            if(listItemNode){
                listNode.insertBefore(placeholderNode, listItemNode);
            }
        }

        // In IE we set a fake effectAllowed in dragstart to get the correct cursor, we therefore
        // ignore the effectAllowed passed in dataTransfer. We must also not access dataTransfer for
        // drops from external sources, as that throws an exception.
        var ignoreDataTransfer = mimeType === MSIE_MIME_TYPE;
        var dropEffect = getDropEffect(event, ignoreDataTransfer);
        if (dropEffect === 'none') {
            return stopDragover();
        }

        // Set dropEffect to modify the cursor shown by the browser, unless we're in IE, where this
        // is not supported. This must be done after preventDefault in Firefox.
        event.preventDefault();
        if (!ignoreDataTransfer) {
            event.dataTransfer.dropEffect = dropEffect;
        }

        event.stopPropagation();
        return false;
    }

    function drop(event) {
        event = event.originalEvent || event;

        // Check whether the drop is allowed and determine mime type.
        var mimeType = getMimeType(event.dataTransfer.types);
//      var itemType = getItemType(mimeType);
        if (!mimeType){
            return true;
        }

        // The default behavior in Firefox is to interpret the dropped element as URL and
        // forward to it. We want to prevent that even if our drop is aborted.
        event.preventDefault();

        // Unserialize the data that was serialized in dragstart.
        var data;
        try {
            var converter = $widget.getConverter(mimeType);
            data = converter.decode(event.dataTransfer.getData(mimeType));
        } catch(e) {
            placeholder.remove();
            return false;
        }

        // Drops with invalid types from external sources might not have been filtered out yet.
//      if (mimeType === MSIE_MIME_TYPE || mimeType === EDGE_MIME_TYPE) {
//      itemType = data.type || undefined;
//      data = data.item;
//      if (!isDropAllowed(itemType)) return stopDragover();
//      }

        // Special handling for internal IE drops, see dragover handler.
        var ignoreDataTransfer = mimeType === MSIE_MIME_TYPE;
        var dropEffect = getDropEffect(event, ignoreDataTransfer);
        if (dropEffect === 'none') {
            return stopDragover();
        }

        // Invoke the callback, which can transform the transferredObject and even abort the drop.
        var widget = $widget.widgetFromElement(event.currentTarget);
        var index = getPlaceholderIndex(widget);
        widget.addChildrenModel(index, data);

        // Clean up
        placeholder.remove();
        event.stopPropagation();
        return false;
    }

    function dragleave(event) {
        event = event.originalEvent || event;
        var widget = $widget.widgetFromElement(event.currentTarget);

        var listNode = widget.getElement()[0];

        var newWidget = $widget.widgetFromPoint(event.clientX, event.clientY);
        if(!newWidget){
            return;
        }
        var newTarget = newWidget.getElement()[0];
        if (!event._dndPhShown && listNode.contains(newTarget) && newWidget.isLeaf()) {
            // Signalize to potential parent lists that a placeholder is already shown.
            event._dndPhShown = true;
        } else {
            placeholder.remove();
        }
    }

    /**
     * Creates new instance of DND processor
     * 
     * @memberof WbProcessorDnd
     */
    function Processor(){
        WbProcessorAbstract.apply(this);
    }

    // extend functionality
    Processor.prototype = new WbProcessorAbstract();
    Processor.prototype.process = function(widget, event){
        if(event.type !== 'stateChanged') {
            return;
        }
        var element = widget.getElement();
        if(widget.state === 'edit') {

            /*
             * Groups must handle:
             * - dragstart
             * - dragend
             */
            if(!widget.isRoot()) {
                /*
                 * Set the HTML5 draggable attribute on the element.
                 */
                element.attr('draggable', 'true');

                /*
                 * When the drag operation is started we have to prepare the dataTransfer object,
                 * which is the primary way we communicate with the target element
                 */
                element.on('dragstart', dragstart);

                /*
                 * The dragend event is triggered when the element was dropped or when the drag
                 * operation was aborted (e.g. hit escape button). Depending on the executed action
                 * we will invoke the callbacks specified with the dnd-moved or dnd-copied attribute.
                 */
                element.on('dragend', dragend);
            }

            /*
             * Groups must handle:
             * - dragenter
             * - dragover
             * - dragleave
             * - drop
             */
            if(!widget.isLeaf()){
                /**
                 * The dragenter event is fired when a dragged element or text selection enters a valid drop
                 * target. According to the spec, we either need to have a dropzone attribute or listen on
                 * dragenter events and call preventDefault(). It should be noted though that no browser seems
                 * to enforce this behaviour.
                 */
                element.on('dragenter', dragenter);


                /**
                 * The dragover event is triggered "every few hundred milliseconds" while an element
                 * is being dragged over our list, or over an child element.
                 */
                element.on('dragover', dragover);

                /**
                 * We have to remove the placeholder when the element is no longer dragged over our list. The
                 * problem is that the dragleave event is not only fired when the element leaves our list,
                 * but also when it leaves a child element. Therefore, we determine whether the mouse cursor
                 * is still pointing to an element inside the list or not.
                 */
                element.on('dragleave', dragleave);

                /**
                 * When the element is dropped, we use the position of the placeholder element as the
                 * position where we insert the transferred data. This assumes that the list has exactly
                 * one child element per array element.
                 */
                element.on('drop', drop);
            }
        } else {
            element.removeAttr('draggable');
            element.off('dragstart', dragstart);
            element.off('dragend', dragend);
            if(!widget.isLeaf()){
                element.off('dragenter', dragenter);
                element.off('dragover', dragover);
                element.off('dragleave', dragleave);
                element.off('drop', drop);
            }
        }
    };
    return Processor;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Processor
 * @name WbProcessorLocator
 * @description Widget processor
 * 
 */
.factory('WbProcessorLocator', function ($wbUtil, WbProcessorAbstract, WidgetLocatorManager) {
    function Processor(){
        WbProcessorAbstract.apply(this);
        this.widgetLocator = new WidgetLocatorManager();
        this.autoVisible = true;
    }

    Processor.prototype = new WbProcessorAbstract();
    
    Processor.prototype.process = function(widget, event){
        if(event.type !== 'stateChanged' || !widget.isRoot()){
            return;
        }
        /*
         * NOTE: we just trak a single root 
         */
        this.widgetLocator.setRootWidget(widget);
        if(this.autoVisible){
            this.widgetLocator.setEnable(widget.state === 'edit');
        }
    };

    /**
     * Enable the processor
     */
    Processor.prototype.setEnable = function(enable){
        this.enable = enable;
        var widgetLocator = this.widgetLocator;
        widgetLocator.setEnable(enable);
    };

    /**
     * Follow widget if is root
     */
    Processor.prototype.setTrackRoot = function(trackRoot){
        this.trackRoot = trackRoot;
    };
    
    Processor.prototype.setAutoVisible = function(autoVisible){
        this.autoVisible = autoVisible;
    };
    
    
    return Processor;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Processor
 * @name WbProcessorSelect
 * @description Widget processor
 * 
 */
.factory('WbProcessorSelect', function ($rootScope, $widget, WbProcessorAbstract) {
	var EVENT_TYPE_SELECTION_CHANGE = 'selectionChange';

	function Processor(){
		WbProcessorAbstract.apply(this, arguments);
		this.selectedWidgets = [];

		var ctrl = this;
		this.clickListener = function($event){
			try{
				$event.preventDefault();
				$event.stopPropagation();
			} catch(ex){
				log.error({
					source: 'WbProcessorSelect',
					message: 'fail to stop event propagation (click)',
					error: ex
				});
			}
			var widget = $event.source;
			if(ctrl.lock || widget.isSilent() || widget.isSelected()){
				return;
			}
			ctrl.lock = true;
			try{
				if($event.shiftKey){
					ctrl.addSelectedWidgets(widget);
				} else {
					ctrl.setSelectedWidgets(widget);
				}

				$rootScope.$digest();
			} catch(ex){
				log.error({
					source: 'WbProcessorSelect',
					message: 'fail to selec a widget type:' + widget.getType(),
					error: ex
				});
			} finally {
				delete ctrl.lock;
			}
		};

		this.dblclickListener = function($event){
			var widget = $event.source;
			if(ctrl.lock || widget.isSilent()){
				return;
			}
			ctrl.lock = true;
			try{
				ctrl.setSelectedWidgets(widget);
				// Open an editor 
				var editor = $widget.getEditor(widget);
				editor.show();

				$event.preventDefault();
				$event.stopPropagation();

				$rootScope.$digest();
			} catch(ex){
				log.error({
					source: 'WbProcessorSelect',
					message: 'fail to open editor for a widget of type:' + widget.getType(),
					error: ex
				});
			} finally {
				delete ctrl.lock;
			}
		};

		this.selectionListener = function($event){
			var widget = $event.source;
			if(ctrl.lock || widget.isSilent()){
				return;
			}
			ctrl.setSelectedWidgets(widget);
		};
	}
	Processor.prototype = new WbProcessorAbstract();

	/**
	 * Processes the widget based on event
	 * 
	 * @memberof WbProcessorSelect
	 */
	Processor.prototype.process = function(widget, event){
		if(event.type !== 'stateChanged') {
			return;
		}
		if(widget.state === 'edit') {
			widget.on('click', this.clickListener);
			widget.on('dblclick', this.dblclickListener);
			widget.on('select', this.selectionListener);
		} else {
			widget.off('click', this.clickListener);
			widget.off('dblclick', this.dblclickListener);
			widget.off('select', this.selectionListener);
		}
	};

	/**
	 * Enable the processor
	 * 
	 * @memberof WbProcessorSelect
	 */
	Processor.prototype.setEnable = function(enable){
		this.enable = enable;
	};

	/**
	 * Sets selected widgets
	 * 
	 * @memberof WbProcessorSelect
	 */
	Processor.prototype.setSelectedWidgets = function(widgets){
		if(!_.isArray(widgets)){
			widgets = [...arguments];
		}

		try{
			this.lock = true;
			_.forEach(this.selectedWidgets, function(widget){
				if(!_.includes(widgets, widget)){
					widget.setSelected(false);
				}
			});

			// clear selection
			// TODO: maso, 2019: check if shift key is hold
			this.selectedWidgets = widgets;
			_.forEach(this.selectedWidgets, function(widget){
				widget.setSelected(true);
			});
		} finally{
			this.lock = false;
		}

		this.fire(EVENT_TYPE_SELECTION_CHANGE, {
			widgets: this.selectedWidgets
		});
	};

	Processor.prototype.addSelectedWidgets = function(widgets){
		if(!_.isArray(widgets)){
			widgets = [...arguments];
		}
		this.setSelectedWidgets(_.concat(this.selectedWidgets, widgets));
	};

	Processor.prototype.getSelectedWidgets = function(){
		return _.clone(this.selectedWidgets || []);
	};

	return Processor;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
.factory('AmhWorkbenchProcessorClone', function(AmhWorkbenchProcessor, $actions, $app, $sidenav) {
	

	var Processor = function (editor, options) {
		options = options || {};
		AmhWorkbenchProcessor.apply(this, [editor, options]);
		
	};
	Processor.prototype = new AmhWorkbenchProcessor();
	
	Processor.prototype.connect = function(){
		var ctrl = this;
		var workbench = this.editor;
		this.actions = [{// clone new menu
			id: 'amh.scope.clone',
			priority: 15,
			icon: 'content_copy',
			title: 'Clone',
			description: 'Clone page',
			visible: function () {
				return ctrl.isClonable();
			},
			/*
			 * @ngInject
			 */
			action: function () {
				return ctrl.cloneContent();
			},
			groups: ['amh.owner-toolbar.scope'],
			scope: this.editor.getScope()
		}];
		_.forEach(this.actions, function(action){
			$actions.newAction(action); 
		});
	};
	
	Processor.prototype.disconnect = function(){
		_.forEach(this.actions || [], function(action){
			$actions.removeAction(action); 
		});
		delete this.actions;
	};
	
	Processor.prototype.isClonable = function(){
		var workbench = this.editor;
		var content = workbench.getContent();
		if(!content){
			return false;
		}
		var permissions = $app.getProperty('account.permissions');
		return permissions.tenant_owner || permissions.cms_author || permissions.cms_editor;
	};
	
	Processor.prototype.cloneContent = function(){
		this
		.then(function () {
			$window.toast('Page is cloned.');
		}, function (/*error*/) {
			$window.alert('Fail to clone the page.');
		});
	};
	
	return Processor;
});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
.factory('AmhWorkbenchProcessorContentValue', function(
		/* ??        */ FileSaver,
		/* angular   */ $q, $rootScope, $window,
		/* amh       */ AmhWorkbenchProcessor, AmhWorkbenchJob,
		/* mblowfish */ $navigator, $actions, $sidenav, $app,
		/* am-wb     */ $wbUtil, $resource, $wbWindow) {
	
	var MIME_WB = 'application/weburger+json';

	var Processor = function (editor, options) {
		options = options || {};
		AmhWorkbenchProcessor.apply(this, [editor, options]);
	};
	Processor.prototype = new AmhWorkbenchProcessor();

	Processor.prototype.connect = function(){
		var ctrl = this;
		var workbench = this.editor;
		var $scope = workbench.getScope();
		this.actions = [
			//--------------------------------------------------------------
			// UI actions
			// Are responsible to open or change UI 
			//--------------------------------------------------------------
			{// template menu
				id: 'amh.workbench.contentValue.template.show',
				priority: 10,
				icon: 'photo_album',
				title: 'Templates',
				description: 'Show list of templates',
				visible: function(){
					return workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function () {
					$sidenav
					.getSidenav('amh.workbench.weburger.templates')
					.toggle();
				},
				groups: ['amh.workbench.toolbar#advance'],
				scope: $scope
			}, {
				id: 'amh.workbench.weburger.widgets.show',
				type: 'action',
				priority : 11,
				icon : 'add',
				title: 'Widgets',
				description: 'Show widget panel',
				/*
				 * @ngInject
				 */
				action: function(){
					if(!workbench.isContentEditable()){
						return false;
					}
					$rootScope.showWidgetsPanel = !$rootScope.showWidgetsPanel;
				},
				groups:['amh.workbench.toolbar'],
				scope: $scope
			}, {
				id: 'amh.workbench.weburger.navigator.show',
				type: 'action',
				priority : 11,
				icon : 'near_me',
				title: 'Content Navigator',
				description: 'Show widgets tree',
				/*
				 * @ngInject
				 */
				action: function(){
					if(!workbench.isContentEditable()){
						return false;
					}
					$rootScope.showWidgetsNavigator = !$rootScope.showWidgetsNavigator;
				},
				groups:['amh.workbench.toolbar'],
				scope: $scope
			}, 

			//--------------------------------------------------------------
			// Basic actions
			// Manages basic informations in the system (e.g. save changes).
			//--------------------------------------------------------------
			{// download menu
				id: 'amh.workbench.contentValue.download',
				priority: 8,
				icon: 'cloud_download',
				title: 'Download',
				description: 'Download page',
				visible: function(){
					return workbench.isContentValueLoaded();
				},
				/*
				 * @ngInject
				 */
				action: function () {
					return ctrl.downloadContentValue();
				},
				groups: ['amh.owner-toolbar.scope'],
				scope: $scope
			},{// upload menu
				id: 'amh.workbench.contentValue.upload',
				priority: 8,
				icon: 'cloud_upload',
				title: 'Upload',
				description: 'Upload a pre desinged page',
				visible: function(){
					return workbench.isContentValueLoaded();
				},
				/*
				 * @ngInject
				 */
				action: function ($event) {
					if($event.template){
						return ctrl.setContentValue($event.template[0]);
					}
					return ctrl.uploadContentValue();
				},
				groups: ['amh.owner-toolbar.scope'],
				scope: $scope
			}, { // edit menu
				id: 'amh.workbench.contentValue.edit',
				priority: 15,
				icon: 'edit',
				title: 'Edit',
				description: 'Edit the page',
				visible: function(){
					return workbench.isContentValueLoaded();
				},
				/*
				 * @ngInject
				 */
				action: function () {
					return workbench.toggleEditMode();
				},
				groups: ['amh.owner-toolbar.scope'],
				scope: $scope
			}, {// save menu
				id: 'amh.workbench.contentValue.save',
				priority: 10,
				icon: 'save',
				title: 'Save',
				description: 'Save content value',
				visible: function(){
					return workbench.isContentValueLoaded() && 
					workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function () {
					return ctrl.saveContentValue();
				},
				groups: ['amh.owner-toolbar.scope'],
				scope: $scope
			},
			//--------------------------------------------------------------
			// Module actions
			// Are responsible to open or change UI 
			//--------------------------------------------------------------
			{
				id: 'amh.workbench.weburger.modules.show',
				type: 'action',
				priority : 1,
				icon : 'view_module',
				title: 'Modules',
				description: 'Manage page modules',
				/*
				 * @ngInject
				 */
				action: function(){
					return $navigator.openDialog({
						config: {},
						locals: {
							$processor: ctrl
						},
						controllerAs: 'dialogCtrl',
						templateUrl: 'views/dialogs/amh-workbench-content-modules.html', 
						fullscreen: true,
						multiple:true
					});
				},
				groups:['amh.workbench.toolbar']
			},
			{ // Content: new
				id: 'amh.workbench.content.module.remove',
				priority: 15,
				icon: 'delete',
				title: 'Delete',
				description: 'Delete module from content',
				visible: function () {
					return workbench.canCreateContent();
				},
				/*
				 * @ngInject
				 */
				action: function ($event) {
					ctrl.deleteModule($event.modules[0]);
				},
				scope: $scope
			}, { // Content: new
				id: 'amh.workbench.content.module.add',
				priority: 15,
				icon: 'add',
				title: 'Add',
				description: 'Add module to the workbench module',
				/*
				 * @ngInject
				 */
				visible: function () {
					return workbench.canCreateContent();
				},
				action: function (/*$event*/) {
					ctrl.createModule();
				},
				scope: $scope
			}];

		_.forEach(this.actions, function(action){
			$actions.newAction(action);
		});

		this._listenToContent = function(){
			var content = workbench.getContent();
			if( content && !content.isAnonymous()){
				ctrl.loadContentValue();
			}
		};

		this._listenState = function(){
			ctrl.loadContent();
		};

		workbench.on('contentChanged', this._listenToContent);
		workbench.on('stateChanged', this._listenState);
	};

	Processor.prototype.disconnect = function(){
		_.forEach(this.actions, function(action){
			$actions.removeAction(action);
		});
		delete this.actions;

		var workbench = this.editor;
		workbench.off('contentChanged', this._listenToContent);
		workbench.off('stateChanged', this._listenToState);
		delete this._listenToContent;
	};

	/**
	 * Downloads content value and stores in a file
	 * 
	 * @memberof AmhContentCtrl
	 * @return {promiss} to save content value
	 */
	Processor.prototype.downloadContentValue = function () {
		var workbench = this.editor;
		var content = workbench.getContent();
		var model = this.getContentValue();
		// save result
		var promise = $navigator.openDialog({
			templateUrl: 'views/dialogs/amh-content-download-options.html',
			config: {
				options: {
					fileName: content.name
				}
			}
		})
		.then(function (options) {
			var preprocess = null;
//			if (options.embeddedImage) {
//			// TODO: convert image into the data url
//			model = _.cloneDeep(model);
//			preprocess = $wbmodel.embedImagesDeep(model);
//			}
			return $q.when(preprocess)
			.then(function () {
				// save the model
				var dataString = JSON.stringify(model);
				var data = new Blob([dataString], {
					type: MIME_WB
				});
				return FileSaver.saveAs(data, (options.fileName || meta.name) + '.wb');
			});
		});
		var job = new AmhWorkbenchJob('Download content value', promise);
		workbench.addJob(job);
		return job;
	};

	Processor.prototype.loadContentValue = function () {
		var ctrl = this;
		var workbench = this.editor;
		var content = workbench.getContent();
		var promise = content.downloadValue()
		.then(function(value){
			ctrl.setContentValue(value);
		}, function(){
			// TODO: maso, 2018: open a page to load templates
			ctrl.setContentValue({type: 'div'});
		});
		var job = new AmhWorkbenchJob('Download content value', promise);
		workbench.addJob(job);
		return job;
	};

	Processor.prototype.uploadContentValue = function () {
		var workbench = this.editor;
		var ctrl = this;
		var fileInput = document.createElement('input');
		fileInput.type = 'file';
		fileInput.style.display = 'none';
		fileInput.onchange = function (event) {
			var reader = new FileReader();
			reader.onload = function (event) {
				var model = JSON.parse(event.target.result);
				ctrl.setContentValue(model);
				var $scope = workbench.getScope();
				$scope.$apply();
			};
			reader.readAsText(event.target.files[0]);
		};
		document.body.appendChild(fileInput);
		// click element (fileInput)
		var eventMouse = document.createEvent('MouseEvents');
		eventMouse.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		fileInput.dispatchEvent(eventMouse);
	};

	Processor.prototype.saveContentValue = function () {
		var workbench = this.editor;
		var content = workbench.getContent();
		var promise = content.uploadValue(this.getContentValue())
		.then(function(){
			// workbench.setDerty(false);
		});
		var job = new AmhWorkbenchJob('Saving content value', promise);
		workbench.addJob(job);
		return job;
	};


	Processor.prototype.loadModules = function(filter){
		var jobs = [];
		var modules = this.getModules();
		_.forEach(modules, function(module){
			if(module.load !== filter && (module.load || filter !== 'lazy')){
				return;
			}
			module.state = 'loading';
			var job;
			switch(module.type){
			case 'js':
				job = $wbWindow.loadLibrary(module.url);
				break;
			case 'css':
				job = $wbWindow.loadStyle(module.url);
				break;
			}
			if(job){
				jobs.push(job.then(function(){
					module.state = 'success';
				}, function(error){
					module.state = 'error';
					module.error = error;
				}));
			}
		});
		return $q.all(jobs);
	};

	Processor.prototype.loadBeforModules = function(){
		return this.loadModules('before');
	};
	Processor.prototype.loadLazyModules= function(){
		return this.loadModules('lazy');
	};
	Processor.prototype.loadAfterModules= function(){
		return this.loadModules('after');
	};

	Processor.prototype.removeModules = function(){
		var jobs = [];
		var modules = this.getModules();
		_.forEach(modules, function(module){
			switch(module.type){
			case 'js':
				jobs.push($wbWindow.removeLibrary(module.url));
				break;
			case 'css':
				jobs.push($wbWindow.removeStyle(module.url));
				break;
			}
		});
		return $q.all(jobs)
		.then(function(){
			$window.toast('Modules are removed. Reload the page to effect.');
		});
	};

	Processor.prototype.createModule = function(){
		var workbench = this.editor;
		var ctrl = this;
		$resource.get('/app/modules', {
			style:{},
		})
		.then(function(modules){
			var newList = _.concat(workbench.getContentModules(), modules);
			ctrl.setModules(newList);
			return workbench.setContentModules(newList);
		})
		.then(function(){
			$window.toast('Modules are added. Reload the page to effect.');
		});
	};

	Processor.prototype.deleteModule = function(module){
		var workbench = this.editor;
		var newList = _.remove(workbench.getContentModules(), function(moduleSrc){
			return !_.isEqual(moduleSrc, module);
		});
		workbench.setContentModules(newList)
		this.setModules(newList);
	};


	/**
	 * Load value as content into the current editor
	 * 
	 * @params value {json object} to load as content
	 */
	Processor.prototype.setContentValue = function(value){
		this.origenalValue = value;
		this.loadContent();
	};

	/**
	 * Return actual value of content
	 * 
	 */
	Processor.prototype.getContentValue = function(){
		return this.origenalValue;
	};

	Processor.prototype.loadContent = function(){
		var ctrl = this;
		var workbench = this.editor;

		workbench.setContentModules(this.getModules());

		// Run preprocess
		$q.all([
			ctrl.loadBeforModules(),
			ctrl.loadTemplate()
			])
			.catch(function(ex){
				// TODO: something wrong
				log.error({
					message: 'fail to load preprocess (template or moduel)',
					srouce: ex
				});
			})
		// load value
		.then(function(){
			ctrl.loadLazyModules();
			var value = ctrl.origenalValue;
			if((workbench.getState() !== 'edit')&&
					(ctrl.template && ctrl.templateAnchor)){
				var newVal = _.cloneDeep(ctrl.template);
				$wbUtil.replaceWidgetModelById(
						newVal, 
						ctrl.templateAnchor, 
						value);
				value = newVal;
			}
			return workbench.setContentValue(value);
		})
		.then(function(){
			return ctrl.loadAfterModules();
		});
	};

	Processor.prototype.loadTemplate = function(){
		if(this.editor.getState() === 'edit'){
			return;
		}
		var templatepath = this.origenalValue ? this.origenalValue.template:null;
		var ctrl = this;
		if(!templatepath){
			this.template = null;
			this.templateAnchor = null;
			return;
		}

		var templatepath = new URL(templatepath, window.location.href);
		switch(templatepath.protocol){
		case 'http:':
			break;
		case 'mb:':
			var key = templatepath.pathname.substring(2).replace(/\//gi, '.');
			templatepath = new URL($app.getProperty(key))
		default:
			log.error('unsupported template protocule', templatepath);
			return;
		}

		this.templateAnchor = templatepath.hash.substring(1);
		templatepath.hash = "";
		return $wbUtil.downloadWidgetModel(templatepath.toString())
		.then(function(model){
			ctrl.template = model;
		});
	};

	/**
	 * THis is internal method
	 * 
	 * change original data model and set the module list
	 * 
	 * the model will be dirty 
	 */
	Processor.prototype.setModules = function(modules){
		var model = this.getContentValue();
		if(!model){
			return;
		}
		model.modules = modules;
	};

	/*
	 * this is internal method
	 * 
	 * returns list of modules from original model.
	 */
	Processor.prototype.getModules = function(){
		var model = this.getContentValue();
		if(!model){
			return [];
		}
		return model.modules || [];
	}


	/*
	 * Object
	 */
	return Processor;
});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
.factory('AmhWorkbenchProcessorCrud', function(
		/* AMh       */ AmhWorkbenchProcessor,  AmhWorkbenchJob,
		/* weburger  */ $dispatcher, $resource,
		/* angular   */ $window, $routeParams,  $q,
		/* mblowfish */ $actions, $app, $navigator,$sidenav,
		/* cms       */ CmsContent, CmsContentMetadata, CmsTermTaxonomy, $cms
) {
	

	var graphql = '{' +
	/* content */ 'id,name,title,description,mime_type,media_type,file_name,file_size,downloads,status,creation_dtime,modif_dtime,author_id' +
	/* metas */ ',metas{id,content_id,key,value}'+
	/* terms */ ',term_taxonomies{id, taxonomy, term{id, name, metas{key,value}}}' +
	'}';
	var categoryGraphql = "{items{id,description,term{id,name, metas{key,value}}}}";
	var tagGraphql = "{items{id,description,term{id,name, metas{key,value}}}}";

	/*
	 * Fetch model id from the environment
	 * 
	 * Returns name of the contents to load. If use define a name, the actual
	 * value is returned. In the other case, name of the home content with
	 * language is returned.
	 * 
	 * default language is en
	 */
	function calculateContentId() {
		if ($routeParams.name) {
			return $routeParams.name;
		}
		var key = $app.getProperty('app.key', 'undefined');
		var lang = $routeParams.language ||
		$app.getProperty('app.setting.localLanguage') ||
		$app.getProperty('app.config.localLanguage') ||
		$app.getProperty('tenat.setting.localLanguage') ||
		$app.getProperty('tenat.config.localLanguage') ||
		'en';
		return key + '-' + lang;
	};

	function isSameContent (currentContent, content){
		if(_.isUndefined(currentContent)){
			var id = calculateContentId();
			return content.id == id || content.name == id;
		}
		return currentContent.id == content.id;
	}

	function Processor(editor, options) {
		var ctrl = this;
		this.dispatchers = [{ // listen to application state
			id: undefined,
			type: '/app/state',
			action: function(event){
				if(event.type !== 'update' || !event.value.startsWith('ready')){
					return;
				}
				return ctrl.readContent();
			}
		},{ // listen to content changes
			id: undefined,
			type: '/cms/contents',
			action: function(event){
				// TODO:
				var values = event.values || [event.value];
				_.forEach(values, function(content){
					if(isSameContent(ctrl.editor.getContent(), content)){
						switch(event.key) {
						case 'create':
						case 'read':
						case 'update':
							if(_.isUndefined(ctrl.editor.getContent())){
								ctrl.readContent();
							} else {
								ctrl.editor.setContent(content);
							}
							break;
						case 'delete':
							ctrl.editor.clean();
							break;
						}
					}
				});
			}
		}];

		options = options || {};
		AmhWorkbenchProcessor.apply(this, [editor, options]);

		var state = $app.getState() || '';
		if(state.startsWith('ready')){
			ctrl.readContent();
		}
	};
	Processor.prototype = new AmhWorkbenchProcessor();

	Processor.prototype.connect = function(){
		var workbench = this.editor;
		var ctrl = this;
		var $scope = workbench.getScope();
		this.actions = [ 
			//--------------------------------------------------------------
			// UI actions
			// Are responsible to open or change UI 
			//--------------------------------------------------------------
			{ // Content: show setting editor
				id: 'amh.workbench.content.show',
				priority: 10,
				icon: 'settings',
				title: 'Content settings',
				description: 'Edit settings of the current content',
				visible: function(){
					return workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function () {
					$sidenav.getSidenav('amh.workbench.content')
					.toggle();
				},
				groups: ['amh.workbench.toolbar'],
				scope: $scope
			}, { // Content Meta: show content setting 
				id: 'amh.workbench.contentMeta.show',
				priority: 8,
				icon: 'perm_device_information',
				title: 'Content metadata',
				description: 'List of metadata',
				visible: function(){
					return workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function () {
					return $navigator.openDialog({
						config: {},
						locals: {
							$processor: ctrl
						},
						controllerAs: 'dialogCtrl',
						templateUrl: 'views/dialogs/amh-workbench-content-metadata.html', 
						fullscreen: true,
						multiple:true
					});
				},
				groups: ['amh.workbench.toolbar'],
				scope: $scope
			}, { // TermTaxonomies: show editor 
				id: 'amh.workbench.termTaxonomies.show',
				priority: 9,
				icon: 'label',
				title: 'Content labels and categories',
				description: 'Edit categories or labels of the current content',
				visible: function(){
					return workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function () {
					// $sidenav.getSidenav('amh.workbench.termTaxonomies')
					// .toggle();
					return $navigator.openDialog({
						config: {},
						locals: {
							$processor: ctrl
						},
						controllerAs: 'dialogCtrl',
						templateUrl: 'views/dialogs/amh-workbench-content-termTaxonomies.html', 
						fullscreen: true,
						multiple:true
					});
				},
				groups: ['amh.workbench.toolbar'],
				scope: $scope
			}, 


			//--------------------------------------------------------------
			// Basic actions
			// Manages basic informations in the system (e.g. save changes).
			//--------------------------------------------------------------
			{ // Content: new
				id: 'amh.workbench.content.new',
				priority: 15,
				icon: 'add_box',
				title: 'New',
				description: 'Add a new page',
				visible: function () {
					return workbench.canCreateContent();
				},
				/*
				 * @ngInject
				 */
				action: function () {
					ctrl.createContent();
				},
				groups: ['amh.owner-toolbar.scope'],
				scope: $scope
			}, { // Content: delete
				id: 'amh.workbench.content.delete',
				priority: 9,
				icon: 'delete',
				title: 'Delete',
				description: 'Delete page',
				visible: function () {
					return workbench.isContentDeletable();
				},
				/*
				 * @ngInject
				 */
				action: function () {
					return $window.confirm('Delete the page?')
					.then(function () {
						ctrl.deleteContent();
					});
				},
				groups: ['amh.owner-toolbar.scope'],
				scope: $scope
			}, 
			//--------------------
			// Metadata
			//-------------------
			{ // Metadata: add 
				id: 'amh.workbench.content.metadata.create',
				priority: 8,
				icon: 'perm_device_information',
				title: 'Adds new metadata',
				description: 'Adds new metadata',
				visible: function(){
					return workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function (/*$event*/) {
					ctrl.createMetadata();
				},
				scope: $scope
			}, { // Metadata: remove 
				id: 'amh.workbench.content.metadata.delete',
				priority: 8,
				icon: 'perm_device_information',
				title: 'Update Term-Taxonomies',
				description: 'Save all changed term taxonomies',
				visible: function(){
					return workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function ($event) {
					if($event.metadata) {
						ctrl.deleteMetadata($event.metadata);
					}
				},
				scope: $scope
			}, { // Metadata: edit 
				id: 'amh.workbench.content.metadata.update',
				priority: 8,
				icon: 'perm_device_information',
				title: 'Update Term-Taxonomies',
				description: 'Save all changed term taxonomies',
				visible: function(){
					return workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function ($event) {
					ctrl.updateMetadata($event.metadata[0]);
				},
				scope: $scope
			},
			//--------------------
			// TermTaxonomies
			//-------------------
			{ // TermTaxonomies: add 
				id: 'amh.workbench.content.termTaxonomies.create',
				visible: function(){
					return workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function ($event) {
					$resource.get('/cms/term-taxonomies', {
						event: $event,
						data: workbench.getTermTaxonomies(),
						style: {
							title: 'Term Taxonomies',
							description: 'Select term-taxonomies to add into the current content'
						}
					})
					.then(function(items){
						ctrl.addTermTaxonomies(items);
					});
				},
				scope: $scope
			}, { // TermTaxonomies: remove 
				id: 'amh.workbench.content.termTaxonomies.delete',
				visible: function(){
					return workbench.isContentEditable();
				},
				/*
				 * @ngInject
				 */
				action: function ($event) {
					if($event.items) {
						ctrl.removeTermTaxonomies($event.items);
					}
				},
				scope: $scope
			}];

		// connect listeners
		_.forEach(this.dispatchers, function(dispatcher){
			dispatcher.id = $dispatcher.on(dispatcher.type, dispatcher.action);
		});

		_.forEach(this.actions, function(action){
			$actions.newAction(action); 
		});
	};

	/**
	 * Removes all listeneres
	 */
	Processor.prototype.disconnect = function(){
		_.forEach(this.dispatchers,  function(dispatcher){
			$dispatcher.off(dispatcher.type, dispatcher.id);
			dispatcher.id = undefined;
		});
		_.forEach(this.actions || [], function(action){
			$actions.removeAction(action); 
		});
		delete this.actions;
	};

	/**
	 * Creates new content
	 */
	Processor.prototype.createContent = function () {
		var process = this.checkOtherProcess();
		if(process){
			return process;
		}

		// initialize data
		var workbench = this.editor;
		var config = {};
		var content = workbench.getContent();
		// if model does not exist we set model id
		if(!content){
			config.name = calculateContentId();
		}
		// open dialog to create the model
		return $navigator.openDialog({
			templateUrl: 'views/dialogs/amh-content-new.html',
			controller: 'AmhPageNewDialogCtrl',
			controllerAs: 'ctrl',
			config: config
		});
	};

	/**
	 * Load content data
	 * 
	 * @memberof AmhContentCtrl
	 * @param {string}
	 *            contentName The id or name of the content.
	 * @return {promiss} to load the content data
	 */
	Processor.prototype.readContent = function () {
		var process = this.checkOtherProcess();
		if(process){
			return process;
		}
		var workbench = this.editor;
		var contentName = calculateContentId();
		var promise =  $cms.getContent(contentName, {
			'graphql': graphql
		}) //
		.then(function (data) {
			// meta
			var metas = [];
			_.forEach(data.metas || [], function(metadataItem){
				metas.push(new CmsContentMetadata(metadataItem));
			});
			delete data.metas;
			workbench.setContentMetadata(metas);

			// taxonomis
			var taxonomies = [];
			_.forEach(data.term_taxonomies || [], function(data){
				taxonomies.push(new CmsTermTaxonomy(data));
			});
			delete data.term_taxonomies;
			workbench.setTermTaxonomies(taxonomies);

			// content
			var content = new CmsContent(data);
			workbench.setContent(content);
		});
	};

	/**
	 * Save the model
	 * 
	 * The editor can save the page if you add saveModel function. By default,
	 * there is not save model function.
	 */
	Processor.prototype.updateContent = function () {
		var process = this.checkOtherProcess();
		if(process){
			return process;
		}
		var workbench = this.editor;
		var state = workbench.getState();
		var meta = workbench.getContent();
		meta.media_type = workbench.getContentType();
		return meta.update()
		.then(function (content) {
			$dispatcher.dispatch('/cms/contents', {
				key: 'updated',
				values: [content]
			});
		}, function(error){
			throw error;
		});
	};


	/**
	 * Delete the content
	 * 
	 * The editor can delete the page if you add deleteModel function. By
	 * default, there is not delete model function.
	 */
	Processor.prototype.deleteContent = function () {
		var process = this.checkOtherProcess();
		if(process){
			return process;
		}
		var workbench = this.editor;
		var meta = workbench.getContent();
		var event = {
				key: 'delete',
				values: [angular.copy(meta)]
		};
		return meta.delete()
		.then(function () {
			$dispatcher.dispatch('/cms/contents', event);
		});
	};

	Processor.prototype.checkOtherProcess = function(){
		var workbench = this.editor;
		if(workbench.getJobs().length){
			return workbench.getJobs()[0];
		};
	};


	Processor.prototype.addTermTaxonomies = function (termTaxonomies) {
		var workbench = this.editor;
		var jobs = [];
		var content = workbench.getContent();
		_.forEach(termTaxonomies, function (termTaxonomy) {
			jobs.push(content.putTermTaxonomy(termTaxonomy));
		});

		var promise = $q.all(jobs)
		.then(function(){
			var newList = _.concat(workbench.getTermTaxonomies(), termTaxonomies);
			workbench.setTermTaxonomies(newList);
		});

		var job = new AmhWorkbenchJob('Adding term taxonomies', promise);
		workbench.addJob(job);
		return job;
	};

	Processor.prototype.removeTermTaxonomies = function (termTaxonomies) {
		var workbench = this.editor;
		var jobs = [];
		var content = workbench.getContent();
		_.forEach(termTaxonomies, function (termTaxonomy) {
			jobs.push(content.deleteTermTaxonomy(termTaxonomy));
		});

		var promise = $q.all(jobs)
		.then(function(){
			var newList = _.filter(workbench.getTermTaxonomies(), function(tt){
				var flag = true;
				_.forEach(termTaxonomies, function(tt2){
					if(_.isEqual(tt.id, tt2.id)){
						flag = false;
					}
				});
				return flag;
			});
			workbench.setTermTaxonomies(newList);
		});

		var job = new AmhWorkbenchJob('Adding term taxonomies', promise);
		workbench.addJob(job);
		return job;
	};



	Processor.prototype.createMetadata = function () {
		// views/dialogs/amh-metadata.html
		var workbench = this.editor;
		return $navigator.openDialog({
			templateUrl: 'views/dialogs/amh-metadata.html',
			/**
			 * @ngInject
			 */
			controller: function ($scope, $controller, config) {
				// Extends Items controller
				angular.extend(this, $controller('AmdNavigatorDialogCtrl', {
					$scope: $scope,
					config: config
				}));

				this.data = config.data || {};
			},
			controllerAs: 'ctrl',
			config: {
				data: {
					key: 'key-' + Math.random(),
					value: 'value'
				}
			}
		}).then(function (data) {
			return workbench.getContent().putMetadatum(data);
		}).then(function(metadata){
			var newList = _.concat(workbench.getContentMetadata(), metadata);
			workbench.setContentMetadata(newList);
		});
	};

	Processor.prototype.updateMetadata = function (meta) {
		var workbench = this.editor;
		return $navigator.openDialog({
			templateUrl: 'views/dialogs/amh-metadata.html',
			/**
			 * @ngInject
			 */
			controller: function ($scope, $controller, config) {
				// Extends Items controller
				angular.extend(this, $controller('AmdNavigatorDialogCtrl', {
					$scope: $scope,
					config: config
				}));

				this.data = config.data || {};
			},
			controllerAs: 'ctrl',
			config: {
				data: _.clone(meta)
			}
		})
		.then(function(data){
			meta.setData(data);
			meta.update();
			
			// force to fire
			workbench.setContentMetadata(workbench.getContentMetadata());
		});
	};

	Processor.prototype.deleteMetadata = function (metadata) {
		var workbench = this.editor;
		var jobs = [];
		var removed = [];
		var content = workbench.getContent();
		_.forEach(metadata, function(metadatum){
			jobs.push(content.deleteMetadatum(metadatum)
					.then(function(){
						removed.push(metadatum);
					}));
		});

		$q.all(jobs)
		.then(function(){
			var newList = _.filter(workbench.getContentMetadata(), function(tt){
				var flag = true;
				_.forEach(removed, function(tt2){
					if(_.isEqual(tt.id, tt2.id)){
						flag = false;
					}
				});
				return flag;
			});
			workbench.setContentMetadata(newList);
		});
	};

	return Processor;
});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
.factory('AmhWorkbenchProcessorMetainfo', function(
		AmhWorkbenchProcessor, 
		/* mblowfish */ $page,
		/* am-wb-core */ $wbWindow) {
	

	function Processor(editor, options) {
		options = options || {};
		AmhWorkbenchProcessor.apply(this, [editor, options]);
		this.info = {};
	};
	Processor.prototype = new AmhWorkbenchProcessor();


	Processor.prototype.connect = function(){
		var ctrl = this;
		this.metadataListener = function() {
			ctrl.updateFromMetadata();
		};
		this.editor.on('contentMetadataChanged', this.metadataListener);
	};


	/**
	 * Disconnect form editor
	 */
	Processor.prototype.disconnect = function(){
		this.editor.off('contentMetadataChanged', this.metadataListener);
		delete this.metadataListener;
	};

	Processor.prototype.updateFromMetadata = function() {
		var wrokbench = this.editor;
		var metadata = wrokbench.getContentMetadata() || [];
		// clean old values
		var ctlr = this;
		_.forEach(this.info, function(value, key){
			ctlr.info[key] = undefined;
		});
		// load new values
		for (var i = 0; i < metadata.length; i++) {
			var m = metadata[i];
			this.info[m.key] = m.value;
		}
		this.loadPageDescription();
	};

	/**
	 * Load page description
	 * 
	 * Page SEO is loaded with $page service.
	 * 
	 * Here is reserved keys:
	 * 
	 * - title
	 * - language
	 * - meta.description
	 * - meta.keywords
	 * - link.favicon
	 * - link.cover
	 * - link.canonical
	 */
	Processor.prototype.loadPageDescription = function () {
		// Load SEO and page
		$page //
			.setTitle(this.info['title']) //
			.setLanguage(this.info['language'])//
			.setDescription(this.info['meta.description'])//
			.setKeywords(this.info['meta.keywords']) //
			.setFavicon(this.info['link.favicon']) //
			.setCover(this.info['link.cover']) //
			.setCanonicalLink(this.info['link.canonical']);
		
		// Set meta datas
		_.forEach(this.info, function(value, key){
			try{
				$wbWindow.setMeta(key, value);
			} catch(ex){}
		});
	};

	return Processor;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialHome')
.factory('AmhWorkbenchProcessor', function() {
	

	var Processor = function (editor) {
		this.editor = editor;
		this.connect();
	};
	
	Processor.prototype.destroy = function(){
		this.disconnect();
		delete this.editor;
	};
	
	
	Processor.prototype.connect = function(){};
	Processor.prototype.disconnect = function(){};
	
	
	return Processor;
});


angular.module('ngMaterialHome').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-seen-settings/collection.html',
    "<fieldset layout=column> <legend translate>Type</legend> <md-input-container> <label translate>URL of collection: (Ex: /api/v2/cms/contents)</label> <input ng-model=ctrl.attributesValue.url ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=\"ctrl.setAttribute('url', ctrl.attributesValue.url)\"> </md-input-container> <md-input-container> <label translate>Properties</label> <input ng-model=ctrl.attributesValue.properties ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=\"ctrl.setAttribute('properties', ctrl.attributesValue.properties)\"> </md-input-container> <md-input-container> <label translate>Query</label> <input ng-model=ctrl.attributesValue.query ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=\"ctrl.setAttribute('query', ctrl.attributesValue.query)\"> </md-input-container> </fieldset> <fieldset layout=column> <legend translate>Filters</legend> <div layout=row ng-repeat=\"filter in ctrl.attributesValue.filters track by $index\"> <md-input-container> <label translate=\"\">Key</label> <input ng-model=filter.key ng-model-options=\"{updateOn: 'change blur', debounce: 1000}\" ng-change=ctrl.filterChanged($index)> </md-input-container> <md-input-container> <label translate=\"\">Value</label> <input ng-model=filter.value ng-model-options=\"{updateOn: 'change blur', debounce: 1000}\" ng-change=ctrl.filterChanged($index)> </md-input-container> <md-button ng-click=ctrl.removeFilter($index) class=md-icon-button> <wb-icon>delete</wb-icon> </md-button> </div> <div layout=row layout-align=\"end center\"> <md-button ng-click=ctrl.addFilter() class=md-icon-button> <wb-icon>add</wb-icon> </md-button> </div> </fieldset> <fieldset layout=column> <legend translate>Sort</legend> <div layout=row ng-repeat=\"sort in ctrl.attributesValue.sorts track by $index\"> <md-input-container flex=40> <label translate=\"\">Sort by</label> <input ng-model=sort.key ng-model-options=\"{updateOn: 'change blur', debounce: 1000}\" ng-change=ctrl.sortChanged($index)> </md-input-container> <md-input-container flex=40> <label translate=\"\">Sort order</label> <md-select ng-model=sort.order ng-model-options=\"{updateOn: 'change blur', debounce: 1000}\" ng-change=ctrl.sortChanged($index)> <md-option value=a translate=\"\">Ascending</md-option> <md-option value=d translate=\"\">Descending</md-option> </md-select> </md-input-container> <md-button ng-click=ctrl.removeSort($index) class=md-icon-button> <wb-icon>delete</wb-icon> </md-button> </div> <div layout=row layout-align=\"end center\"> <md-button ng-click=ctrl.addSort() class=md-icon-button> <wb-icon>add</wb-icon> </md-button> </div> </fieldset> <fieldset layout=column> <legend translate>Template</legend> <textarea style=\"resize: vertical; width: 100%; max-width: 100%; min-width: 100%; padding: 0px; margin: 0px\" ng-model=ctrl.attributesValue.template ng-model-options=\"{updateOn: 'change blur', debounce: 1000}\" ng-change=\"ctrl.setAttribute('template', ctrl.attributesValue.template)\"></textarea> </fieldset>"
  );


  $templateCache.put('views/am-wb-seen-settings/import.html',
    "<fieldset layout=column> <legend translate>Source</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.url ng-change=\"ctrl.setAttribute('url', ctrl.attributesValue.url)\" wb-title=URL wb-description=\"Path of the page\" wb-resource-type=url> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/amh-content-editor.html',
    "<div ng-controller=\"AmhContentWorkbenchCtrl as workbench\" id=amh-content-workbench name=workbench layout=column flex> <md-progress-linear class=md-accent ng-if=workbench.jobs.length md-mode=indeterminate> </md-progress-linear> <amh-workbench-editor-toolbar ng-if=workbench.isEditorLoaded() ng-show=\"workbench.state == 'edit'\" ng-model=workbench> </amh-workbench-editor-toolbar> <div ng-if=workbench.isEditorLoaded() ng-show=\"workbench.state == 'edit'\" id=demo-widget-editor-toolbar></div> <div id=amh-content-editor-area layout=row flex> <amh-workbench-navigator ng-show=\"workbench.state == 'edit'\" ng-if=workbench.isEditorLoaded() ng-model=workbench></amh-workbench-navigator>  <md-content ng-controller=\"AmhContentEditorCtrl as editor\" ng-if=\"workbench.contentType === 'weburger'\" flex> <div wb-content wb-on-load=editor.setRootWidget($data) wb-on-error=\"alert('fail to load the model')\" ng-model=workbench.contentValue ng-model-options=\"{ debounce: 100 }\" ng-change=editor.setDerty(true)></div> </md-content> </div> <amh-widget-path ng-if=workbench.isEditorLoaded() ng-show=\"workbench.state == 'edit'\" ng-model=workbench> </amh-widget-path> </div>"
  );


  $templateCache.put('views/amh-signup.html',
    " <md-content layout=column layout-align=none layout-align-gt-sm=\"none center\" flex> <div md-whiteframe=3 flex=none layout=column>  <md-toolbar layout-padding layout=column layout-align=\"space-around center\"> <img ng-src={{app.config.brand.logo}} height=80px width=\"auto\"> <div> <h3 style=font-weight:normal>{{app.config.brand.title}}</h3> </div> </md-toolbar>  <form ng-show=app.user.anonymous name=form ng-submit=signup(registerData) layout=column ayout-padding> <div layout-padding> <h3 translate>sign up</h3> </div> <div layout=row layout-padding> <md-input-container layout-fill> <label translate>username</label> <input name=login ng-model=registerData.login required> <div ng-messages=form.login.$error> <div ng-message=required translate>This is required.</div> </div> </md-input-container> </div> <div layout=row layout-padding> <md-input-container layout-fill> <label translate>email</label> <input name=email ng-model=registerData.email type=email required> <div ng-messages=form.email.$error> <div ng-message=required translate>This is required.</div> <div ng-message=email translate>email is not valid</div> </div> </md-input-container> </div> <div layout=row layout-padding> <md-input-container layout-fill> <label translate>password</label> <input name=password ng-model=registerData.password type=password required> <div ng-messages=form.password.$error> <div ng-message=required translate>This is required.</div> </div> </md-input-container> </div> <div layout=row layout-padding> <span md-colors=\"{color:'default-warn'}\">{{signupMessage}}</span> </div> <div layout=column layout-align=none layout-gt-xs=row layout-align-gt-xs=\"center center\" layout-padding> <a href=users/login style=\"text-decoration: none\" ui-sref=forget flex-order=1 flex-order-gt-xs=-1>{{'have you already account?' | translate}}</a> <md-button flex-order=0 flex-order-gt-xs=0 class=md-raised ng-href=users/login>{{'login' | translate}}</md-button> <md-button ng-disabled=form.$invalid flex-order=-1 flex-order-gt-xs=1 type=submit ng-click=signup(registerData) class=\"md-raised md-primary\">{{ 'sign up' | translate }}</md-button> </div> </form> <div layout-padding ng-show=!app.user.anonymous layout=column layout-align=\"none center\"> <img width=150px height=150px ng-show=!uploadAvatar ng-src=\"{{app.user.current.avatar}}\"> <h3>{{app.user.current.login}}</h3> <p translate>you are signed in. go to one of the following options.</p> </div> <div ng-show=!app.user.anonymous layout=column layout-align=none layout-gt-xs=row layout-align-gt-xs=\"center center\" layout-padding> <md-button ng-href=\"\" flex-order=-1 flex-order-gt-xs=1 class=md-raised><wb-icon>home</wb-icon> {{'main page' | translate}}</md-button> <md-button ng-click=back() flex-order=0 flex-order-gt-xs=0 class=md-raised><wb-icon>settings_backup_restore</wb-icon> {{'back page' | translate}}</md-button> <md-button ng-href=users/account flex-order=1 flex-order-gt-xs=-1 class=md-raised><wb-icon>account_circle</wb-icon> {{'user account' | translate}}</md-button> </div> </div> </md-content>"
  );


  $templateCache.put('views/dialogs/amh-content-download-options.html',
    "<md-dialog layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <wb-icon>file</wb-icon> <h2 translate>Content</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.options)> <wb-icon aria-label=Done>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding flex> <span flex=10></span> <md-input-container> <label translate>File name</label> <input ng-model=config.options.fileName aria-label=\"File name\"> </md-input-container> <md-checkbox aria-label=\"Disabled checkbox\" ng-model=config.options.embeddedImage> <span translate>Embedded image</span> </md-checkbox> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amh-content-new-old.html',
    "<md-dialog layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <wb-icon>file</wb-icon> <h2 translate>Content</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(ctrl.data)> <wb-icon aria-label=Done>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content> <md-content layout=column layout-padding> <span flex=10></span> <md-input-container class=\"md-icon-float md-icon-right md-block\" required> <label translate>Name</label> <input ng-model=ctrl.data.name required> <wb-icon ng-click=ctrl.generateRandomName()>autorenew</wb-icon> </md-input-container> <md-input-container> <label translate>Title</label> <input ng-model=ctrl.data.title> </md-input-container> <md-input-container> <label translate>Description</label> <textarea ng-model=ctrl.data.description></textarea> </md-input-container> <md-checkbox ng-model=ctrl.data.openPage aria-label=\"redirect to the new page\"> <span translate>Redirect to the page</span> </md-checkbox> </md-content> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amh-content-new.html',
    "<md-dialog layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <wb-icon>file</wb-icon> <h2 translate>New page</h2> <span flex></span> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=row flex> <md-stepper id=page-stepper md-mobile-step-text=false md-vertical=true md-linear=true md-alternative=true flex> <md-step md-label=\"{{'Type'| translate}}\" id=page-type> <md-step-body layout-padding layout=row layout-wrap layout-align=\"center start\"> <md-button layout=column layout-align=\"center center\" ng-repeat=\"item in ctrl.supportedPageTypes\" class=amh-content-new-type-item ng-class=\"{'selected': ctrl.contentModel.info.mime_type === item.mime_type}\" ng-click=ctrl.setPageType(item)> <img ng-src=\"{{item.icon}}\"> <h3 translate=\"\">{{item.title}}</h3> </md-button> </md-step-body> </md-step> <md-step md-label=\"{{'Information'| translate}}\" id=page-info> <md-step-body layout-padding layout=column> <md-input-container class=\"md-icon-float md-block\"> <label translate>Name</label> <input ng-model=ctrl.contentModel.info.name ng-change=\"ctrl.setPageInfo('name', ctrl.contentModel.info.name);\" ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" required> <wb-icon ng-click=ctrl.generateRandomName()>autorenew</wb-icon> </md-input-container> <md-input-container class=md-block> <label translate>Title</label> <input ng-model=ctrl.contentModel.info.title ng-change=\"ctrl.setPageInfo('title', ctrl.contentModel.info.title);\" ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\"> </md-input-container> <md-input-container class=md-block> <label translate>Description</label> <textarea ng-model=ctrl.contentModel.info.description ng-change=\"ctrl.setPageInfo('description', ctrl.contentModel.info.description);\" ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\"></textarea> </md-input-container> <md-input-container class=md-block> <label translate>Language</label> <input ng-model=ctrl.contentModel.info.language ng-change=\"ctrl.setPageMeta('language', ctrl.contentModel.info.language)\" ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\"> </md-input-container> <md-input-container class=md-block> <label translate>Keywords</label> <textarea ng-model=ctrl.contentModel.info.keywords ng-change=\"ctrl.setPageMeta('meta.keywords', ctrl.contentModel.info.keywords)\" ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\">     \n" +
    "                        </textarea> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <label translate>Cover</label> <input ng-model=ctrl.contentModel.info.cover ng-change=\"ctrl.pageMeta('link.cover', ctrl.contentModel.info.cover)\" ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\"> <wb-icon ng-click=ctrl.selectCoverFromResources()>image</wb-icon> </md-input-container> </md-step-body> <md-step-actions layout=row> <span flex></span> <md-button class=\"md-primary md-raised\" ng-click=ctrl.nextStep() translate>Continue</md-button> </md-step-actions> </md-step> <md-step md-label=\"{{'Template'| translate}}\" id=page-template> <md-step-body layout-padding> <md-list flex> <md-list-item class=md-2-line ng-click=ctrl.setTemplate()> <div class=md-list-item-text layout=column> <h3 translate>Blank</h3> <p translate>Set page with empty content</p> </div> </md-list-item> <md-subheader ng-repeat-start=\"termTax in ctrl.items\" class=md-no-sticky>{{::termTax.term.name}} </md-subheader> <md-list-item class=md-2-line ng-repeat=\"template in termTax.contents\" ng-click=ctrl.setTemplate(template)> <div class=md-list-item-text layout=column> <h3>{{::template.title}}</h3> <p>{{template.description}}</p> </div> <md-button class=md-secondary ng-click=ctrl.showPreviewOf(template) aria-label=\"Show preview\"> <wb-icon>pageview</wb-icon> <md-tooltip md-direction=\"{{app.dir==='rtl' ? 'right' : 'left'}}\"> <span translate>Preview</span> </md-tooltip> </md-button> </md-list-item> <md-divider ng-repeat-end></md-divider> </md-list> </md-step-body> </md-step> <md-step id=page-redirect md-label=\"{{'Creating'| translate}}\"> <md-step-body layout-padding> <div layout=row layout-align=\"center center\" ng-if=\"!ctrl.creatingContent && !ctrl.contentCreated\"> <md-button ng-click=ctrl.cancel() aria-label=\"Cancel the action\"> <span translate>Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=ctrl.createModel(ctrl.contentModel) aria-label=\"Creates the page\"> <span translate>Create</span> </md-button> </div> <div layout=row layout-align=\"center center\" ng-if=\"!ctrl.creatingContent && ctrl.contentCreated\"> </div> <div layout=column layout-align=\"center center\" ng-if=ctrl.creatingContent> <h2 translate=\"\">Creating the page...</h2> <md-progress-linear class=md-accent md-mode=indeterminate> </md-progress-linear> </div> <div layout=column layout-align=\"center center\" ng-if=ctrl.contentCreated> <h2 translate>The page created successfully.</h2>  <md-button class=\"md-raised md-primary\" ng-href={{ctrl.canonicalLink}} ng-click=ctrl.goTo() aria-label=\"Redirect to page\"> <span translate=\"\">Redirect to the page</span> </md-button> </div> </md-step-body> </md-step> </md-stepper> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amh-content.html',
    "<md-dialog layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <wb-icon>file</wb-icon> <h2 translate>Content</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.data)> <wb-icon aria-label=Done>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding flex> <span flex=10></span> <md-input-container> <label translate>Title</label> <input ng-model=config.data.title> </md-input-container> <md-input-container> <label translate>Description</label> <input ng-model=config.data.discription> </md-input-container> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amh-metadata.html',
    "<md-dialog layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <wb-icon>file</wb-icon> <h2 translate>Metadata</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(ctrl.data)> <wb-icon aria-label=Done>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content> <md-content layout=column layout-padding> <span flex=10></span> <md-input-container class=\"md-icon-float md-icon-right md-block\" required> <label translate>Key</label> <input ng-model=ctrl.data.key required> </md-input-container> <md-input-container> <label translate>Value</label> <input ng-model=ctrl.data.value> </md-input-container> </md-content> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amh-template-preview.html',
    "<md-dialog ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <wb-icon>cms-pages</wb-icon> <h2 translate>Template</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.model)> <wb-icon aria-label=Done>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column flex> <md-content flex> <div wb-content ng-model=config.model>  </div></md-content> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amh-workbench-content-metadata.html',
    "<form ng-cloak ng-controller=\"AmhContentWorkbenchMetasCtrl as ctrl\"> <md-toolbar class=\"content-sidenavs-toolbar wb-layer-tool-bottom\" layout=row layout-align=\"space-around center\"> <span translate flex>Meta Fields</span> <md-button class=md-icon-button ng-click=ctrl.addMetadatum($event)> <wb-icon>add</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </md-toolbar> <md-dialog-content layout=row flex> <md-content flex> <md-input-container ng-repeat=\"meta in ctrl.contentMetadata track by meta.id\" class=\"md-icon-float md-icon-right md-block\"> <label>{{meta.key}}</label> <wb-icon ng-click=\"ctrl.editMetadatum(meta, $event)\">edit</wb-icon> <input ng-model=meta.value disabled> <wb-icon ng-click=\"ctrl.deleteMetadatum(meta, $event)\">delete</wb-icon> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-icon-button ng-click=ctrl.addMetadatum($event)> <wb-icon>add</wb-icon> </md-button> </div> </md-content> </md-dialog-content> </form>"
  );


  $templateCache.put('views/dialogs/amh-workbench-content-modules.html',
    "<form ng-cloak ng-controller=\"AmhContentWorkbenchModuleCtrl as ctrl\"> <md-toolbar class=\"content-sidenavs-toolbar wb-layer-tool-bottom\" layout=row layout-align=\"space-around center\"> <span translate flex>Modules</span> <md-button class=md-icon-button ng-click=ctrl.addModule($event)> <wb-icon>add</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </md-toolbar> <md-dialog-content layout=row flex> <md-content flex> <md-list style=\"width: 100%\"> <md-list-item class=md-3-line ng-repeat=\"item in ctrl.modules\" ng-click=\"ctrl.editModule(item, event)\"> <wb-icon ng-if=\"item.type == 'css'\">style</wb-icon> <wb-icon ng-if=\"item.type == 'js'\">tune</wb-icon> <div class=md-list-item-text layout=column> <h3>{{ item.title }}</h3> <h4>{{ item.url }}</h4> <p> Load: {{ item.load }}</p> </div> <wb-icon class=\"md-secondary md-icon-button\" ng-click=\"ctrl.deleteModule(item, $event)\" id=action-delete>delete</wb-icon> </md-list-item> </md-list> </md-content> </md-dialog-content> </form>"
  );


  $templateCache.put('views/dialogs/amh-workbench-content-termTaxonomies.html',
    "<form ng-cloak ng-controller=\"AmhContentWorkbenchTermTaxonomiesCtrl as ctrl\"> <md-toolbar class=\"content-sidenavs-toolbar wb-layer-tool-bottom\" layout=row layout-align=\"space-around center\"> <span translate>Labels and categories</span> <md-button class=md-icon-button ng-click=ctrl.addTermTaxonomies()> <wb-icon>add</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </md-toolbar> <md-dialog-content layout=row flex> <md-content flex> <fieldset ng-repeat=\"(taxonomy, termTaxonomies) in ctrl.termTaxonomiesMap\" layout=column> <legend>{{::taxonomy}}</legend> <div layout=row ng-repeat=\"item in termTaxonomies track by item.id\"> <span flex>{{::item.term.name}}</span> <md-button class=md-icon-button ng-click=\"ctrl.deleteTermTaxonomy(item, $event)\"> <wb-icon>delete</wb-icon> </md-button> </div> </fieldset> </md-content> </md-dialog-content> </form>"
  );


  $templateCache.put('views/directives/amh-mini-action-anchor.html',
    "<div> <md-button ng-repeat=\"item in ctrl.group.items | orderBy:['-priority']\" aria-label={{::item.title}} ng-if=item.visible() ng-click=item.exec($event) class=\"md-icon-button md-mini\"> <md-tooltip md-delay=1500> <span translate>{{::item.title}}</span> </md-tooltip> <wb-icon size=\"{{mbSize || 16}}\">{{::item.icon}}</wb-icon> </md-button> <md-divider ng-if=\"mbDivider && ctrl.group.items.length\"></md-divider> </div>"
  );


  $templateCache.put('views/directives/amh-template-contents.html',
    "<div layout=column> <mb-pagination-bar mb-title=Templates mb-model=ctrl.queryParameter mb-properties=ctrl.properties mb-reload=ctrl.reload() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content layout=column mb-infinate-scroll=ctrl.loadNextPage() mb-preloading=\"ctrl.state !== 'ideal'\"> <md-list style=\"padding: 0px\" flex> <md-subheader ng-repeat-start=\"termTax in ctrl.items\" class=md-no-sticky>{{::termTax.term.name}}</md-subheader> <md-list-item ng-click=ctrl.showPreviewOf(page) ng-repeat=\"page in termTax.contents\" class=md-2-line> <div class=md-list-item-text layout=column> <h3>{{::page.title}}</h3> <h4>{{::page.description}}</h4> </div> </md-list-item> <md-divider ng-repeat-end></md-divider> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/directives/amh-user-widgets.html',
    "<div layout=column layout-padding> <md-input-container> <label translate>Category</label> <md-select ng-model=selectedCategory ng-change=ctrl.loadWidgets(selectedCategory)> <md-option ng-repeat=\"category in ::categories\" ng-value=category>{{::category.title}}</md-option> </md-select> </md-input-container> <md-content flex> <wb-group wb-editable=selectedCategory ng-model=widgets> </wb-group> </md-content> </div>"
  );


  $templateCache.put('views/directives/amh-widget-path.html',
    "<md-toolbar id=amh-widget-path class=\"md-primary md-hue-2 wb-layer-tool-bottom\" layout=row layout-align=\"start center\"> <div layout=row ng-repeat=\"widget in widgets\">  <md-menu mb-context-menu> <md-button aria-label={{widget.getType()}} class=path-segment ng-class=\"{'path-segment-active' : $index >= prefixIndex}\" ng-mouseover=widget.setUnderCursor(widget) ng-click=widget.setSelected(true)> {{widget.getTitle() || widget.getId() || widget.getType()}} </md-button> <md-menu-content width=4> <md-menu-item> <md-button ng-click=widget.setSelected(true)> <span translate>Select</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=\"ctrl.selectChildren(widget, $event)\"> <span translate>Select Children</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.clone()> <span translate>Clone</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.delete()> <span translate>Delete</span> </md-button> </md-menu-item> <md-divider></md-divider>  <md-menu-item> <md-button ng-click=widget.moveFirst()> <span translate>Move first</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.moveBefore()> <span translate>Move before</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.moveNext()> <span translate>Move next</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.moveLast()> <span translate>Move last</span> </md-button> </md-menu-item> <md-divider></md-divider> <md-menu-item> <md-button ng-click=ctrl.openHelp(widget)> <span translate>Widget help</span> </md-button> </md-menu-item> </md-menu-content> </md-menu>  <md-menu> <md-button aria-label=childs class=path-segment ng-click=$mdMenu.open($event) aria-label=\"Open phone interactions menu\"> &gt; </md-button> <md-menu-content> <md-menu-item ng-repeat=\"child in widget.getChildren()\"> <md-button ng-click=child.setSelected(true)> {{child.getTitle() || child.getId() || child.getType()}} </md-button> </md-menu-item> </md-menu-content> </md-menu> </div> <span flex></span> <mb-mini-action-anchor class=path-segment mb-action-group=amh.workbench.footer mb-divider=false layout=row> </mb-mini-action-anchor> </md-toolbar>"
  );


  $templateCache.put('views/directives/amh-workbench-editor-toolbar.html',
    "<md-toolbar class=\"md-primary md-hue-2 wb-layer-tool-bottom amh-mini-toolbar\" id=amh.workbench.editor.weburger.toolbar> <div layout=row> <span flex></span> <mb-mini-action-anchor mb-action-group=amh.workbench.editor.weburger.toolbar mb-size=16 mb-divider=true layout=row> </mb-mini-action-anchor> <mb-mini-action-anchor mb-action-group=amh.workbench.editor.weburger.toolbar#clipboard mb-size=16 mb-divider=true layout=row> </mb-mini-action-anchor> <mb-mini-action-anchor mb-action-group=amh.workbench.editor.weburger.toolbar#common mb-size=16 mb-divider=true layout=row> </mb-mini-action-anchor> <span flex></span> <md-button aria-label=settings class=\"md-icon-button editor-action md-mini\" ng-click=ctrl.toggleSettingPanel()> <wb-icon ng-if=!showWorkbenchSettingPanel size=16>menu</wb-icon> <wb-icon ng-if=showWorkbenchSettingPanel size=16>close</wb-icon> </md-button> </div> </md-toolbar>"
  );


  $templateCache.put('views/directives/amh-workbench-navigator.html',
    "<md-toolbar id=amh.workbench.toolbar class=\"md-primary md-hue-2 wb-layer-tool-bottom workbench-vertical-toolbar\" layout=column layout-align=\"start center\">  <mb-mini-action-anchor mb-action-group=amh.workbench.toolbar layout=column layout-align=\"start center\"> </mb-mini-action-anchor> <span flex></span>  <mb-mini-action-anchor mb-action-group=amh.workbench.toolbar#advance layout=column layout-align=\"start center\"> </mb-mini-action-anchor> </md-toolbar>"
  );


  $templateCache.put('views/directives/wb-event-panel.html',
    "<table class=mb-table style=\"font-size: 10px\"> <thead> <tr md-colors=\"{color: 'primary-700'}\"> <td translate=\"\">Name</td> <td translate=\"\">Code</td> <td></td> </tr> </thead> <tbody> <tr ng-click=\"ctrl.editEvent(event, $event)\" ng-repeat=\"event in events track by $index\" style=\"cursor: pointer\"> <td translate>{{event.title}}</td> <td style=\"cursor: pointer\">{{event.code| limitTo:13 }} ...</td> <td align=center style=\"cursor: pointer;min-height: 22px\"> <md-button style=\"min-height: 20px;height: 20px;margin: 0px;line-height: 20px;padding: 0px\" ng-if=event.code ng-click=\"ctrl.deleteEvent(event, $event)\" class=md-icon-button aria-label=\"delete script\"> <wb-icon size=16px>delete</wb-icon> </md-button> </td> </tr> </tbody> </table>"
  );


  $templateCache.put('views/directives/wb-setting-panel-expansion.html',
    "<md-expansion-panel-group id=WB-SETTING-PANEL> <md-expansion-panel ng-repeat=\"setting in settings| orderBy:priority track by setting.type\" ng-show=setting.visible> <md-expansion-panel-collapsed> <div layout=row layout-align=\"start center\"> <wb-icon size=12px ng-if=setting.icon>{{setting.icon}}</wb-icon> <div class=md-title translate=\"\" flex>{{setting.label}}</div> <md-tooltip md-direction=left> <span translat=\"\">{{setting.description || setting.label}}</span> </md-tooltip> </div> </md-expansion-panel-collapsed> <md-expansion-panel-expanded> <md-expansion-panel-header ng-click=$panel.collapse()> <div layout=row layout-align=\"start center\"> <wb-icon size=12px ng-if=setting.icon>{{setting.icon}}</wb-icon> <div class=md-title translate=\"\" flex>{{setting.label}}</div> </div> </md-expansion-panel-header> <md-expansion-panel-content layout=column style=\"padding: 2px\"> <wb-setting-page ng-model=wbModel wb-type={{setting.type}}> </wb-setting-page> </md-expansion-panel-content> </md-expansion-panel-expanded> </md-expansion-panel> </md-expansion-panel-group>"
  );


  $templateCache.put('views/directives/wb-setting-panel-tabs.html',
    "<div id=am-wb-widget-setting> <md-tabs md-dynamic-height md-border-bottom> <md-tab ng-repeat=\"setting in settings| orderBy:priority track by setting.type\" ng-disabled=!setting.visible id={{setting.key}}> <md-tab-label> <span ng-if=!setting.icon translate=\"\">{{setting.label}}</span> <wb-icon ng-if=setting.icon>{{setting.icon}}</wb-icon> </md-tab-label> <md-tab-body layout-margin> <wb-setting-page ng-model=wbModel wb-type={{setting.type}}> </wb-setting-page> </md-tab-body> </md-tab> </md-tabs> </div>"
  );


  $templateCache.put('views/directives/wb-ui-setting-boolean.html',
    "<field layout=row layout-align=\"start center\"> <field-description flex> <label translate>{{::title}}</label> <wb-icon size=1em>info</wb-icon> </field-description> <input ng-model=value ng-change=setValue(value) aria-label=\"set {{::title}} true or false\" type=checkbox> </field>"
  );


  $templateCache.put('views/directives/wb-ui-setting-color.html',
    "<field layout=row layout-align=\"start stretch\"> <field-description> <label translate>{{::title}}</label> <wb-icon size=1em>info</wb-icon> </field-description> <input ng-model=value ng-change=setValue(value) aria-label=\"color of {{::title}}\" flex> <div ng-click=selectColor($event) class=preview></div> </field>"
  );


  $templateCache.put('views/directives/wb-ui-setting-length.html',
    "<field layout=row layout-align=\"start center\"> <field-description flex> <label translate>{{::title}}</label> <wb-icon size=1em>info</wb-icon> </field-description> <input ng-model=value ng-change=setValue(value) aria-label=\"set {{::title}} true or false\"> </field>"
  );


  $templateCache.put('views/directives/wb-ui-setting-number.html',
    "<field layout=row layout-align=\"start center\"> <field-description flex> <label translate>{{::title}}</label> <wb-icon size=1em>info</wb-icon> </field-description> <div class=number layout=row> <input ng-model=value ng-change=setValue(value) aria-label=\"set {{::title}} true or false\"> </div> </field>"
  );


  $templateCache.put('views/directives/wb-ui-setting-select.html',
    "<field layout=row layout-align=\"start center\"> <field-description flex> <label translate>{{::title}}</label> <wb-icon size=1em>info</wb-icon> </field-description> <div class=select layout=row> <input ng-model=value ng-change=setValue(value) aria-label=\"set {{::title}} true or false\"> </div> </field>"
  );


  $templateCache.put('views/directives/wb-ui-setting-text.html',
    "<field layout=row layout-align=\"start center\"> <field-description> <label translate>{{::title}}</label>  </field-description> <input ng-model=value ng-change=setValue(value) aria-label=\"set {{::title}} true or false\" flex> <wb-icon ng-if=resourceType ng-click=openResource() size=1em>more_horiz</wb-icon> </field>"
  );


  $templateCache.put('views/directives/wb-widgets-explorer.html',
    "<div layout=column>  <md-toolbar ng-show=!(showSearch||showSort||showState)> <div class=md-toolbar-tools> <h3 flex translate>Widgets</h3> <md-button class=md-icon-button aria-label=Search ng-click=\"showSearch = !showSearch\"> <wb-icon>search</wb-icon> </md-button> <md-divider></md-divider> <md-button ng-click=\"wbWidgetExplorer._view_list=!wbWidgetExplorer._view_list\" class=md-icon-button aria-label=\"View mode\"> <wb-icon>{{wbWidgetExplorer._view_list ? 'view_module' : 'view_list'}}</wb-icon> </md-button> <md-button ng-click=\"wbWidgetExplorer._tree_mode=!wbWidgetExplorer._tree_mode\" class=md-icon-button aria-label=\"Tree mode\"> <wb-icon>{{wbWidgetExplorer._tree_mode? 'list' : 'list_tree'}}</wb-icon> </md-button> </div> </md-toolbar>  <md-toolbar class=md-hue-1 ng-show=showSearch> <div class=md-toolbar-tools> <md-button class=md-icon-button ng-click=\"showSearch = !showSearch\" aria-label=Back> <wb-icon>arrow_back</wb-icon> </md-button> <md-input-container md-theme=input flex> <label>&nbsp;</label> <input ng-model=query ng-keyup=\"runQuery(query, $event)\"> </md-input-container> <md-button class=md-icon-button aria-label=Search ng-click=\"showSearch = !showSearch\"> <wb-icon>search</wb-icon> </md-button> </div> </md-toolbar> <md-content> <md-expansion-panel-group ng-if=wbWidgetExplorer._tree_mode> <md-expansion-panel ng-repeat=\"group in groups\"> <md-expansion-panel-collapsed> <span translate>{{group.title || group.id}}</span> </md-expansion-panel-collapsed> <md-expansion-panel-expanded> <md-expansion-panel-header> <span translate>{{group.title || group.id}}</span> </md-expansion-panel-header> <md-expansion-panel-content style=\"padding: 0px; margin: 0px\"> <wb-widgets-list ng-if=wbWidgetExplorer._view_list widgets=group.widgets> </wb-widgets-list> <wb-widgets-module ng-if=!wbWidgetExplorer._view_list widgets=group.widgets> </wb-widgets-module> </md-expansion-panel-content> </md-expansion-panel-expanded> </md-expansion-panel> </md-expansion-panel-group> <wb-widgets-list ng-if=\"!wbWidgetExplorer._tree_mode &amp;&amp; wbWidgetExplorer._view_list\" widgets=widgets> </wb-widgets-list> <wb-widgets-module ng-if=\"!wbWidgetExplorer._tree_mode &amp;&amp; !wbWidgetExplorer._view_list\" widgets=widgets> </wb-widgets-module> </md-content> </div>"
  );


  $templateCache.put('views/directives/wb-widgets-list.html',
    "<md-list flex> <md-list-item class=md-2-line ng-repeat=\"widget in widgets\" wb-on-dragstart=\"putDragdata($event, widget)\" draggable=true> <wb-icon>{{widget.icon}}</wb-icon> <div class=md-list-item-text layout=column> <h3 translate>{{widget.title}}</h3> <p translate>{{widget.description}}</p> </div> <wb-icon ng-if=openHelp class=md-secondary ng-click=\"openHelp(widget, $event)\" aria-label=\"Show help\">help</wb-icon> </md-list-item> </md-list>"
  );


  $templateCache.put('views/directives/wb-widgets-module.html',
    "<div layout=column layout-gt-sm=row layout-align=space-around layout-wrap> <div class=\"wb-widgets-module md-whiteframe-1dp\" ng-repeat=\"widget in widgets\" flex=none flex-gt-sm=30 layout=column layout-align=\"start center\" layout-padding wb-on-dragstart=\"putDragdata($event, widget)\" draggable=true> <wb-icon size=32px wb-icon-name={{widget.icon}}></wb-icon> <p flex class=wb-text-truncate translate=\"\">{{widget.title}}</p> <md-tooltip md-delay=1500>{{widget.description | translate}}</md-tooltip> </div> </div>"
  );


  $templateCache.put('views/partials/wb-widget-options.html',
    ""
  );


  $templateCache.put('views/preferences/main-page-template.html',
    "<div layout=row layout-align=\"center start\" layout-wrap layout-padding mb-preloading=\"loadingTemplatePreview || savingContent\"> <md-card ng-repeat=\"template in templates track by template.name\" flex-xs flex-gt-xs=none layout=column class=amh-core-themplate-card>  <img ng-click=ctrl.showPreviewOf(template) ng-src={{template.thumbnail}} class=amh-core-themplate-card-image alt={{template.name}}>  <md-card-content ng-show=false> <p>{{template.description}}</p> </md-card-content>  <md-card-actions layout=row layout-align=\"end center\"> <md-button ng-click=ctrl.showPreviewOf(template) aria-label=\"Show preview\"> <span translate=\"\">Preview</span> </md-button> <md-button ng-click=ctrl.setTemplate(template) aria-label=\"Apply the template\"> <span translate=\"\">Apply</span> </md-button> </md-card-actions> </md-card> </div>"
  );


  $templateCache.put('views/preferences/mb-crisp-chat.html',
    "<div layout=column layout-margin ng-cloak flex> <md-input-container class=md-block> <label translate>CRISP site ID</label> <input required md-no-asterisk name=property ng-model=\"app.config.crisp.id\"> </md-input-container> </div>"
  );


  $templateCache.put('views/preferences/pageNotFound.html',
    "<div layout=row flex layout-padding> <md-content ng-controller=AmhContentCtrl layout=column flex> <wb-group wb-on-model-select=loadSettings($model) wb-editable=editable ng-model=app.config.pageNotFound flex> </wb-group> </md-content> </div>"
  );


  $templateCache.put('views/resources/pages.html',
    "<md-content ng-controller=\"AmhContentPagesCtrl as ctrl\" mb-infinate-scroll=ctrl.loadNextPage() mb-preloading=\"ctrl.state !== 'ideal'\" flex>  <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getProperties() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-list flex> <md-list-item class=md-2-line ng-repeat=\"page in ctrl.items\" ng-click=setValue(page)> <div class=md-list-item-text layout=column> <h3>{{ page.title}}</h3> <h4>{{ page.description}}</h4> </div> </md-list-item> </md-list> </md-content>"
  );


  $templateCache.put('views/settings/wb-shadow.html',
    " <div layout=row flex> <md-button class=\"md-raised md-primary\" ng-click=ctrl.addShadow()> <span translate>New shadow</span> </md-button> </div>  <fieldset ng-repeat=\"shadow in ctrl.shadows track by $index\" layout=column> <legend><span translate>Shadow</span>{{$index + 1}}</legend> <wb-ui-setting-length title=\"Horizontal Shift\" ng-model=shadow.hShift ng-change=ctrl.updateShadows()> </wb-ui-setting-length> <wb-ui-setting-length title=\"Vertical Shift\" ng-model=shadow.vShift ng-change=ctrl.updateShadows()> </wb-ui-setting-length> <wb-ui-setting-length title=Blur ng-model=shadow.blur ng-change=ctrl.updateShadows()> </wb-ui-setting-length> <wb-ui-setting-length title=Spread ng-model=shadow.spread ng-change=ctrl.updateShadows()> </wb-ui-setting-length> <wb-ui-setting-color title=Color wb-ui-setting-clear-button=true wb-ui-setting-preview=true ng-model=shadow.color ng-change=ctrl.updateShadows()> </wb-ui-setting-color> <md-checkbox ng-model=shadow.inset ng-change=ctrl.updateShadows() aria-label=Inset> <span translate=\"\">Inset</span> </md-checkbox> <div layout=row layout-align=\"end center\"> <p style=\"font-size: 14px\" flex tab-index=0></p> <md-button class=\"md-raised md-accent\" aria-label=\"Delete shadow\" ng-click=ctrl.remove($index)> <span translate>Delete</span> </md-button> </div> </fieldset>"
  );


  $templateCache.put('views/settings/wb-style-animation.html',
    "<fieldset layout=column> <legend translate>Animation</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.animation ng-change=\"ctrl.setStyle('animation', ctrl.stylesValue.animation)\" wb-title=Animation wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.animationName ng-change=\"ctrl.setStyle('animationName', ctrl.stylesValue.animationName)\" wb-title=Name wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.animationDuration ng-change=\"ctrl.setStyle('animationDuration', ctrl.stylesValue.animationDuration)\" wb-title=Duration wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.animationTimingFunction ng-change=\"ctrl.setStyle('animationTimingFunction', ctrl.stylesValue.animationTimingFunction)\" wb-title=\"Timing Function\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.animationDelay ng-change=\"ctrl.setStyle('animationDelay', ctrl.stylesValue.animationDelay)\" wb-title=Delay wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.animationIterationCount ng-change=\"ctrl.setStyle('animationIterationCount', ctrl.stylesValue.animationIterationCount)\" wb-title=\"Iteration Count\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.animationDirection ng-change=\"ctrl.setStyle('animationDirection', ctrl.stylesValue.animationDirection)\" wb-title=Direction wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.animationFillMode ng-change=\"ctrl.setStyle('animationFillMode', ctrl.stylesValue.animationFillMode)\" wb-title=\"Fill Mode\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.animationPlayState ng-change=\"ctrl.setStyle('animationPlayState', ctrl.stylesValue.animationPlayState)\" wb-title=\"Play State\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Transform</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.transform ng-change=\"ctrl.setStyle('transform', ctrl.stylesValue.transform)\" wb-title=\"Play State\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.transformOrigin ng-change=\"ctrl.setStyle('transformOrigin', ctrl.stylesValue.transformOrigin)\" wb-title=\"Play State\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.transformStyle ng-change=\"ctrl.setStyle('transformStyle', ctrl.stylesValue.transformStyle)\" wb-title=\"Play State\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Transition</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.transition ng-change=\"ctrl.setStyle('transition', ctrl.stylesValue.transition)\" wb-title=\"Play State\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.transitionProperty ng-change=\"ctrl.setStyle('transitionProperty', ctrl.stylesValue.transitionProperty)\" wb-title=\"Play State\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.transitionDuration ng-change=\"ctrl.setStyle('transitionDuration', ctrl.stylesValue.transitionDuration)\" wb-title=\"Play State\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.transitionTimingFunction ng-change=\"ctrl.setStyle('transitionTimingFunction', ctrl.stylesValue.transitionTimingFunction)\" wb-title=\"Play State\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.transitionDelay ng-change=\"ctrl.setStyle('transitionDelay', ctrl.stylesValue.transitionDelay)\" wb-title=\"Play State\" wb-description=\"\" wb-action-clean> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-style-background.html',
    "<fieldset layout=column> <legend translate>Background</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.background ng-change=\"ctrl.setStyle('background', ctrl.stylesValue.background)\" wb-title=Background wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.backgroundColor ng-change=\"ctrl.setStyle('backgroundColor', ctrl.stylesValue.backgroundColor)\" wb-title=Color wb-description=\"\" wb-action-clean> </wb-ui-setting-color> <wb-ui-setting-text ng-model=ctrl.stylesValue.backgroundImage ng-change=\"ctrl.setStyle('backgroundImage', ctrl.stylesValue.backgroundImage)\" wb-title=Image wb-description=\"\" wb-resource-type=image-url wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.backgroundPosition ng-change=\"ctrl.setStyle('backgroundPosition', ctrl.stylesValue.backgroundPosition)\" wb-title=Position wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.backgroundSize ng-change=\"ctrl.setStyle('backgroundSize', ctrl.stylesValue.backgroundSize)\" wb-title=Size wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.backgroundRepeat ng-change=\"ctrl.setStyle('backgroundRepeat', ctrl.stylesValue.backgroundRepeat)\" wb-title=Repeat wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.backgroundOrigin ng-change=\"ctrl.setStyle('backgroundOrigin', ctrl.stylesValue.backgroundOrigin)\" wb-title=Origin wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.backgroundClip ng-change=\"ctrl.setStyle('backgroundClip', ctrl.stylesValue.backgroundClip)\" wb-title=Clip wb-description=\"\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.backgroundAttachment ng-change=\"ctrl.setStyle('backgroundAttachment', ctrl.stylesValue.backgroundAttachment)\" wb-title=Attachment wb-description=\"\" wb-action-clean> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-style-border.html',
    "<fieldset layout=column> <legend translate>Border</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.border ng-change=\"ctrl.setStyle('border', ctrl.stylesValue.border)\" wb-title=Boarder wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderWidth ng-change=\"ctrl.setStyle('borderWidth', ctrl.stylesValue.borderWidth)\" wb-title=Width wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderStyle ng-change=\"ctrl.setStyle('borderStyle', ctrl.stylesValue.borderStyle)\" wb-title=Style wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.borderColor ng-change=\"ctrl.setStyle('borderColor', ctrl.stylesValue.borderColor)\" wb-title=Color wb-description=\"\"> </wb-ui-setting-color> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderCollapse ng-change=\"ctrl.setStyle('borderCollapse', ctrl.stylesValue.borderCollapse)\" wb-title=Collapse wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderSpacing ng-change=\"ctrl.setStyle('borderSpacing', ctrl.stylesValue.borderSpacing)\" wb-title=Spacing wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Top</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderTop ng-change=\"ctrl.setStyle('borderTop', ctrl.stylesValue.borderTop)\" wb-title=Top wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderTopWidth ng-change=\"ctrl.setStyle('borderTopWidth', ctrl.stylesValue.borderTopWidth)\" wb-title=Width wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderTopStyle ng-change=\"ctrl.setStyle('borderTopStyle', ctrl.stylesValue.borderTopStyle)\" wb-title=Style wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.borderTopColor ng-change=\"ctrl.setStyle('borderTopColor', ctrl.stylesValue.borderTopColor)\" wb-title=Color wb-description=\"\"> </wb-ui-setting-color> </fieldset> <fieldset layout=column> <legend translate>Right</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderRight ng-change=\"ctrl.setStyle('borderRight', ctrl.stylesValue.borderRight)\" wb-title=Right wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderRightWidth ng-change=\"ctrl.setStyle('borderRightWidth', ctrl.stylesValue.borderRightWidth)\" wb-title=Width wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderRightStyle ng-change=\"ctrl.setStyle('borderRightStyle', ctrl.stylesValue.borderRightStyle)\" wb-title=Style wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.borderRightColor ng-change=\"ctrl.setStyle('borderRightColor', ctrl.stylesValue.borderRightColor)\" wb-title=Color wb-description=\"\"> </wb-ui-setting-color> </fieldset> <fieldset layout=column> <legend translate>Bottom</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderBottom ng-change=\"ctrl.setStyle('borderBottom', ctrl.stylesValue.borderBottom)\" wb-title=Bottom wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderBottomWidth ng-change=\"ctrl.setStyle('borderBottomWidth', ctrl.stylesValue.borderBottomWidth)\" wb-title=Width wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderBottomStyle ng-change=\"ctrl.setStyle('borderBottomStyle', ctrl.stylesValue.borderBottomStyle)\" wb-title=Style wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.borderBottomColor ng-change=\"ctrl.setStyle('borderBottomColor', ctrl.stylesValue.borderBottomColor)\" wb-title=Color wb-description=\"\"> </wb-ui-setting-color> </fieldset> <fieldset layout=column> <legend translate>Left</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderLeft ng-change=\"ctrl.setStyle('borderLeft', ctrl.stylesValue.borderLeft)\" wb-title=Left wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderLeftWidth ng-change=\"ctrl.setStyle('borderLeftWidth', ctrl.stylesValue.borderLeftWidth)\" wb-title=Width wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderLeftStyle ng-change=\"ctrl.setStyle('borderLeftStyle', ctrl.stylesValue.borderLeftStyle)\" wb-title=Style wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.borderLeftColor ng-change=\"ctrl.setStyle('borderLeftColor', ctrl.stylesValue.borderLeftColor)\" wb-title=Color wb-description=\"\"> </wb-ui-setting-color> </fieldset> <fieldset layout=column> <legend translate>Radius</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderRadius ng-change=\"ctrl.setStyle('borderRadius', ctrl.stylesValue.borderRadius)\" wb-title=Radius wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderTopRightRadius ng-change=\"ctrl.setStyle('borderTopRightRadius', ctrl.stylesValue.borderTopRightRadius)\" wb-title=\"Top Right\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderTopLeftRadius ng-change=\"ctrl.setStyle('borderTopLeftRadius', ctrl.stylesValue.borderTopLeftRadius)\" wb-title=\"Top Left\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderBottomLeftRadius ng-change=\"ctrl.setStyle('borderBottomLeftRadius', ctrl.stylesValue.borderBottomLeftRadius)\" wb-title=\"Bottom Left\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderBottomRightRadius ng-change=\"ctrl.setStyle('borderBottomRightRadius', ctrl.stylesValue.borderBottomRightRadius)\" wb-title=\"Bottom Right\" wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Image</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderImage ng-change=\"ctrl.setStyle('borderImage', ctrl.stylesValue.borderImage)\" wb-title=Image wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderImageSource ng-change=\"ctrl.setStyle('borderImageSource', ctrl.stylesValue.borderImageSource)\" wb-title=Source wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderImageSlice ng-change=\"ctrl.setStyle('borderImageSlice', ctrl.stylesValue.borderImageSlice)\" wb-title=Slice wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderImageWidth ng-change=\"ctrl.setStyle('borderImageWidth', ctrl.stylesValue.borderImageWidth)\" wb-title=Width wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderImageOutset ng-change=\"ctrl.setStyle('borderImageOutset', ctrl.stylesValue.borderImageOutset)\" wb-title=Outset wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.borderImageRepeat ng-change=\"ctrl.setStyle('borderImageRepeat', ctrl.stylesValue.borderImageRepeat)\" wb-title=Repeat wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Outline</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.outline ng-change=\"ctrl.setStyle('outline', ctrl.stylesValue.outline)\" wb-title=Outline wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.outlineOffset ng-change=\"ctrl.setStyle('outlineOffset', ctrl.stylesValue.outlineOffset)\" wb-title=Offset wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.outlineWidth ng-change=\"ctrl.setStyle('outlineWidth', ctrl.stylesValue.outlineWidth)\" wb-title=Width wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.outlineStyle ng-change=\"ctrl.setStyle('outlineStyle', ctrl.stylesValue.outlineStyle)\" wb-title=Style wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.outlineColor ng-change=\"ctrl.setStyle('outlineColor', ctrl.stylesValue.outlineColor)\" wb-title=Color wb-description=\"\"> </wb-ui-setting-color> </fieldset> <fieldset layout=column> <legend translate>Box</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.boxDecorationBreak ng-change=\"ctrl.setStyle('boxDecorationBreak', ctrl.stylesValue.boxDecorationBreak)\" wb-title=\"Decoration Break\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.boxShadow ng-change=\"ctrl.setStyle('boxShadow', ctrl.stylesValue.boxShadow)\" wb-title=Shadow wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.boxSizing ng-change=\"ctrl.setStyle('boxSizing', ctrl.stylesValue.boxSizing)\" wb-title=Sizing wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-style-general.html',
    "<fieldset layout=column> <legend translate>View</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.opacity ng-change=\"ctrl.setStyle('opacity', ctrl.stylesValue.opacity)\" wb-title=Opacity wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.visibility ng-change=\"ctrl.setStyle('visibility', ctrl.stylesValue.visibility)\" wb-title=Visibility wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.color ng-change=\"ctrl.setStyle('color', ctrl.stylesValue.color)\" wb-title=Color wb-description=\"\"> </wb-ui-setting-color> <wb-ui-setting-text ng-model=ctrl.stylesValue.mixBlendMode ng-change=\"ctrl.setStyle('mixBlendMode', ctrl.stylesValue.mixBlendMode)\" wb-title=\"\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.isolation ng-change=\"ctrl.setStyle('isolation', ctrl.stylesValue.isolation)\" wb-title=\"\" wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Mouse</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.cursor ng-change=\"ctrl.setStyle('cursor', ctrl.stylesValue.cursor)\" wb-title=Cursor wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.pointerEvents ng-change=\"ctrl.setStyle('pointerEvents', ctrl.stylesValue.pointerEvents)\" wb-title=pointerEvents wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-style-layout.html',
    "<fieldset layout=column> <legend translate>Layout</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.display ng-change=\"ctrl.setStyle('display', ctrl.stylesValue.display)\" wb-title=display wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.order ng-change=\"ctrl.setStyle('order', ctrl.stylesValue.order)\" wb-title=order wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.zIndex ng-change=\"ctrl.setStyle('zIndex', ctrl.stylesValue.zIndex)\" wb-title=zIndex wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.clear ng-change=\"ctrl.setStyle('clear', ctrl.stylesValue.clear)\" wb-title=clear wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.float ng-change=\"ctrl.setStyle('float', ctrl.stylesValue.float)\" wb-title=float wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Position</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.position ng-change=\"ctrl.setStyle('position', ctrl.stylesValue.position)\" wb-title=position wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.bottom ng-change=\"ctrl.setStyle('bottom', ctrl.stylesValue.bottom)\" wb-title=bottom wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.left ng-change=\"ctrl.setStyle('left', ctrl.stylesValue.left)\" wb-title=left wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.right ng-change=\"ctrl.setStyle('right', ctrl.stylesValue.right)\" wb-title=right wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.top ng-change=\"ctrl.setStyle('top', ctrl.stylesValue.top)\" wb-title=top wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>overflow</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.overflow ng-change=\"ctrl.setStyle('overflow', ctrl.stylesValue.overflow)\" wb-title=overflow wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.overflowX ng-change=\"ctrl.setStyle('overflowX', ctrl.stylesValue.overflowX)\" wb-title=overflowX wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.overflowY ng-change=\"ctrl.setStyle('overflowY', ctrl.stylesValue.overflowY)\" wb-title=overflowY wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.scrollBehavior ng-change=\"ctrl.setStyle('scrollBehavior', ctrl.stylesValue.scrollBehavior)\" wb-title=scrollBehavior wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Print</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.pageBreakAfter ng-change=\"ctrl.setStyle('pageBreakAfter', ctrl.stylesValue.pageBreakAfter)\" wb-title=pageBreakAfter wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.pageBreakBefore ng-change=\"ctrl.setStyle('pageBreakBefore', ctrl.stylesValue.pageBreakBefore)\" wb-title=pageBreakBefore wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.pageBreakInside ng-change=\"ctrl.setStyle('pageBreakInside', ctrl.stylesValue.pageBreakInside)\" wb-title=pageBreakInside wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Flex</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.alignContent ng-change=\"ctrl.setStyle('alignContent', ctrl.stylesValue.alignContent)\" wb-title=alignContent wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.alignItems ng-change=\"ctrl.setStyle('alignItems', ctrl.stylesValue.alignItems)\" wb-title=alignItems wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.alignSelf ng-change=\"ctrl.setStyle('alignSelf', ctrl.stylesValue.alignSelf)\" wb-title=alignSelf wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.justifyContent ng-change=\"ctrl.setStyle('justifyContent', ctrl.stylesValue.justifyContent)\" wb-title=justifyContent wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.flex ng-change=\"ctrl.setStyle('flex', ctrl.stylesValue.flex)\" wb-title=flex wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.flexBasis ng-change=\"ctrl.setStyle('flexBasis', ctrl.stylesValue.flexBasis)\" wb-title=flexBasis wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.flexDirection ng-change=\"ctrl.setStyle('flexDirection', ctrl.stylesValue.flexDirection)\" wb-title=flexDirection wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.flexGrow ng-change=\"ctrl.setStyle('flexGrow', ctrl.stylesValue.flexGrow)\" wb-title=flexGrow wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.flexShrink ng-change=\"ctrl.setStyle('flexShrink', ctrl.stylesValue.flexShrink)\" wb-title=flexShrink wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.flexWrap ng-change=\"ctrl.setStyle('flexWrap', ctrl.stylesValue.flexWrap)\" wb-title=flexWrap wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Grid</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.grid ng-change=\"ctrl.setStyle('grid', ctrl.stylesValue.grid)\" wb-title=grid wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridArea ng-change=\"ctrl.setStyle('gridArea', ctrl.stylesValue.gridArea)\" wb-title=gridArea wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridAutoColumns ng-change=\"ctrl.setStyle('gridAutoColumns', ctrl.stylesValue.gridAutoColumns)\" wb-title=gridAutoColumns wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridAutoFlow ng-change=\"ctrl.setStyle('gridAutoFlow', ctrl.stylesValue.gridAutoFlow)\" wb-title=gridAutoFlow wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridAutoRows ng-change=\"ctrl.setStyle('gridAutoRows', ctrl.stylesValue.gridAutoRows)\" wb-title=gridAutoRows wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridColumn ng-change=\"ctrl.setStyle('gridColumn', ctrl.stylesValue.gridColumn)\" wb-title=gridColumn wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridColumnEnd ng-change=\"ctrl.setStyle('gridColumnEnd', ctrl.stylesValue.gridColumnEnd)\" wb-title=gridColumnEnd wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridColumnGap ng-change=\"ctrl.setStyle('gridColumnGap', ctrl.stylesValue.gridColumnGap)\" wb-title=gridColumnGap wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridColumnStart ng-change=\"ctrl.setStyle('gridColumnStart', ctrl.stylesValue.gridColumnStart)\" wb-title=gridColumnStart wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridGap ng-change=\"ctrl.setStyle('gridGap', ctrl.stylesValue.gridGap)\" wb-title=gridGap wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridRow ng-change=\"ctrl.setStyle('gridRow', ctrl.stylesValue.gridRow)\" wb-title=gridRow wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridRowEnd ng-change=\"ctrl.setStyle('gridRowEnd', ctrl.stylesValue.gridRowEnd)\" wb-title=gridRowEnd wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridRowGap ng-change=\"ctrl.setStyle('gridRowGap', ctrl.stylesValue.gridRowGap)\" wb-title=gridRowGap wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridRowStart ng-change=\"ctrl.setStyle('gridRowStart', ctrl.stylesValue.gridRowStart)\" wb-title=gridRowStart wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridTemplate ng-change=\"ctrl.setStyle('gridTemplate', ctrl.stylesValue.gridTemplate)\" wb-title=gridTemplate wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridTemplateAreas ng-change=\"ctrl.setStyle('gridTemplateAreas', ctrl.stylesValue.gridTemplateAreas)\" wb-title=gridTemplateAreas wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridTemplateColumns ng-change=\"ctrl.setStyle('gridTemplateColumns', ctrl.stylesValue.gridTemplateColumns)\" wb-title=gridTemplateColumns wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.gridTemplateRows ng-change=\"ctrl.setStyle('gridTemplateRows', ctrl.stylesValue.gridTemplateRows)\" wb-title=gridTemplateRows wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Column</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.columns ng-change=\"ctrl.setStyle('columns', ctrl.stylesValue.columns)\" wb-title=columns wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.columnWidth ng-change=\"ctrl.setStyle('columnWidth', ctrl.stylesValue.columnWidth)\" wb-title=columnWidth wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.columnCount ng-change=\"ctrl.setStyle('columnCount', ctrl.stylesValue.columnCount)\" wb-title=columnCount wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.columnSpan ng-change=\"ctrl.setStyle('columnSpan', ctrl.stylesValue.columnSpan)\" wb-title=columnSpan wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.columnFill ng-change=\"ctrl.setStyle('columnFill', ctrl.stylesValue.columnFill)\" wb-title=columnFill wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.columnGap ng-change=\"ctrl.setStyle('columnGap', ctrl.stylesValue.columnGap)\" wb-title=columnGap wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.columnRule ng-change=\"ctrl.setStyle('columnRule', ctrl.stylesValue.columnRule)\" wb-title=columnRule wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.columnRuleColor ng-change=\"ctrl.setStyle('columnRuleColor', ctrl.stylesValue.columnRuleColor)\" wb-title=columnRuleColor wb-description=\"\"> </wb-ui-setting-color> <wb-ui-setting-text ng-model=ctrl.stylesValue.columnRuleStyle ng-change=\"ctrl.setStyle('columnRuleStyle', ctrl.stylesValue.columnRuleStyle)\" wb-title=columnRuleStyle wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.columnRuleWidth ng-change=\"ctrl.setStyle('columnRuleWidth', ctrl.stylesValue.columnRuleWidth)\" wb-title=columnRuleWidth wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-style-media.html',
    "<fieldset layout=column> <legend translate>Layout</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.clip ng-change=\"ctrl.setStyle('clip', ctrl.stylesValue.clip)\" wb-title=clip wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.clipPath ng-change=\"ctrl.setStyle('clipPath', ctrl.stylesValue.clipPath)\" wb-title=clipPath wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.filter ng-change=\"ctrl.setStyle('filter', ctrl.stylesValue.filter)\" wb-title=filter wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.objectFit ng-change=\"ctrl.setStyle('objectFit', ctrl.stylesValue.objectFit)\" wb-title=objectFit wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.objectPosition ng-change=\"ctrl.setStyle('objectPosition', ctrl.stylesValue.objectPosition)\" wb-title=objectPosition wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-style-size.html',
    "<fieldset layout=column> <legend translate>Size</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.resize ng-change=\"ctrl.setStyle('resize', ctrl.stylesValue.resize)\" wb-title=resize wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.height ng-change=\"ctrl.setStyle('height', ctrl.stylesValue.height)\" wb-title=height wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.maxHeight ng-change=\"ctrl.setStyle('maxHeight', ctrl.stylesValue.maxHeight)\" wb-title=maxHeight wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.minHeight ng-change=\"ctrl.setStyle('minHeight', ctrl.stylesValue.minHeight)\" wb-title=minHeight wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.width ng-change=\"ctrl.setStyle('width', ctrl.stylesValue.width)\" wb-title=width wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.maxWidth ng-change=\"ctrl.setStyle('maxWidth', ctrl.stylesValue.maxWidth)\" wb-title=maxWidth wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.minWidth ng-change=\"ctrl.setStyle('minWidth', ctrl.stylesValue.minWidth)\" wb-title=minWidth wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Margin</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.margin ng-change=\"ctrl.setStyle('margin', ctrl.stylesValue.margin)\" wb-title=margin wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.marginTop ng-change=\"ctrl.setStyle('marginTop', ctrl.stylesValue.marginTop)\" wb-title=\"Margin Top\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.marginRight ng-change=\"ctrl.setStyle('marginRight', ctrl.stylesValue.marginRight)\" wb-title=\"margin Right\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.marginBottom ng-change=\"ctrl.setStyle('marginBottom', ctrl.stylesValue.marginBottom)\" wb-title=\"margin Bottom\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.marginLeft ng-change=\"ctrl.setStyle('marginLeft', ctrl.stylesValue.marginLeft)\" wb-title=\"margin left\" wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Padding</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.padding ng-change=\"ctrl.setStyle('padding', ctrl.stylesValue.padding)\" wb-title=padding wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.paddingTop ng-change=\"ctrl.setStyle('paddingTop', ctrl.stylesValue.paddingTop)\" wb-title=\"padding Top\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.paddingRight ng-change=\"ctrl.setStyle('paddingRight', ctrl.stylesValue.paddingRight)\" wb-title=\"padding Right\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.paddingBottom ng-change=\"ctrl.setStyle('paddingBottom', ctrl.stylesValue.paddingBottom)\" wb-title=\"padding Bottom\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.paddingLeft ng-change=\"ctrl.setStyle('paddingLeft', ctrl.stylesValue.paddingLeft)\" wb-title=\"padding left\" wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-style-text.html',
    "<fieldset layout=column> <legend translate>Text</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.textAlign ng-change=\"ctrl.setStyle('textAlign', ctrl.stylesValue.textAlign)\" wb-title=Align wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.textAlignLast ng-change=\"ctrl.setStyle('textAlignLast', ctrl.stylesValue.textAlignLast)\" wb-title=\"Align Last\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.textIndent ng-change=\"ctrl.setStyle('textIndent', ctrl.stylesValue.textIndent)\" wb-title=Indent wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.textJustify ng-change=\"ctrl.setStyle('textJustify', ctrl.stylesValue.textJustify)\" wb-title=Justify wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.textOverflow ng-change=\"ctrl.setStyle('textOverflow', ctrl.stylesValue.textOverflow)\" wb-title=Overflow wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.textShadow ng-change=\"ctrl.setStyle('textShadow', ctrl.stylesValue.textShadow)\" wb-title=Shadow wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.textTransform ng-change=\"ctrl.setStyle('textTransform', ctrl.stylesValue.textTransform)\" wb-title=Transform wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Decoration</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.textDecoration ng-change=\"ctrl.setStyle('textDecoration', ctrl.stylesValue.textDecoration)\" wb-title=Decoration wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-color ng-model=ctrl.stylesValue.textDecorationColor ng-change=\"ctrl.setStyle('textDecorationColor', ctrl.stylesValue.textDecorationColor)\" wb-title=Color wb-description=\"\"> </wb-ui-setting-color> <wb-ui-setting-text ng-model=ctrl.stylesValue.textDecorationLine ng-change=\"ctrl.setStyle('textDecorationLine', ctrl.stylesValue.textDecorationLine)\" wb-title=Line wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.textDecorationStyle ng-change=\"ctrl.setStyle('textDecorationStyle', ctrl.stylesValue.textDecorationStyle)\" wb-title=Style wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Font</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.font ng-change=\"ctrl.setStyle('font', ctrl.stylesValue.font)\" wb-title=Font wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.fontFamily ng-change=\"ctrl.setStyle('fontFamily', ctrl.stylesValue.fontFamily)\" wb-title=Family wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.fontKerning ng-change=\"ctrl.setStyle('fontKerning', ctrl.stylesValue.fontKerning)\" wb-title=fontKerning wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.fontSize ng-change=\"ctrl.setStyle('fontSize', ctrl.stylesValue.fontSize)\" wb-title=Size wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.fontSizeAdjust ng-change=\"ctrl.setStyle('fontSizeAdjust', ctrl.stylesValue.fontSizeAdjust)\" wb-title=\"Size Adjust\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.fontStretch ng-change=\"ctrl.setStyle('fontStretch', ctrl.stylesValue.fontStretch)\" wb-title=fontStretch wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.fontStyle ng-change=\"ctrl.setStyle('fontStyle', ctrl.stylesValue.fontStyle)\" wb-title=Style wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.fontVariant ng-change=\"ctrl.setStyle('fontVariant', ctrl.stylesValue.fontVariant)\" wb-title=Variant wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.fontWeight ng-change=\"ctrl.setStyle('fontWeight', ctrl.stylesValue.fontWeight)\" wb-title=Weight wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Writing</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.hyphens ng-change=\"ctrl.setStyle('hyphens', ctrl.stylesValue.hyphens)\" wb-title=Hyphens wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.letterSpacing ng-change=\"ctrl.setStyle('letterSpacing', ctrl.stylesValue.letterSpacing)\" wb-title=\"Letter Spacing\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.lineHeight ng-change=\"ctrl.setStyle('lineHeight', ctrl.stylesValue.lineHeight)\" wb-title=\"Line Height\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.quotes ng-change=\"ctrl.setStyle('quotes', ctrl.stylesValue.quotes)\" wb-title=quotes wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.tabSize ng-change=\"ctrl.setStyle('tabSize', ctrl.stylesValue.tabSize)\" wb-title=\"Tab Size\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.verticalAlign ng-change=\"ctrl.setStyle('verticalAlign', ctrl.stylesValue.verticalAlign)\" wb-title=verticalAlign wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.whiteSpace ng-change=\"ctrl.setStyle('whiteSpace', ctrl.stylesValue.whiteSpace)\" wb-title=\"White Space\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.wordBreak ng-change=\"ctrl.setStyle('wordBreak', ctrl.stylesValue.wordBreak)\" wb-title=\"Word Break\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.wordSpacing ng-change=\"ctrl.setStyle('wordSpacing', ctrl.stylesValue.wordSpacing)\" wb-title=wordSpacing wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.wordWrap ng-change=\"ctrl.setStyle('wordWrap', ctrl.stylesValue.wordWrap)\" wb-title=\"Word Wrap\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.writingMode ng-change=\"ctrl.setStyle('writingMode', ctrl.stylesValue.writingMode)\" wb-title=writingMode wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.userSelect ng-change=\"ctrl.setStyle('userSelect', ctrl.stylesValue.userSelect)\" wb-title=\"User Select\" wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Local</legend> <wb-ui-setting-text ng-model=ctrl.stylesValue.direction ng-change=\"ctrl.setStyle('direction', ctrl.stylesValue.direction)\" wb-title=Direction wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.stylesValue.unicodeBidi ng-change=\"ctrl.setStyle('unicodeBidi', ctrl.stylesValue.unicodeBidi)\" wb-title=\"Unicode Bidi\" wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-a.html',
    "<fieldset layout=column> <legend translate>Link</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.download ng-change=\"ctrl.setAttribute('download', ctrl.attributesValue.download)\" wb-title=Download wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.href ng-change=\"ctrl.setAttribute('href', ctrl.attributesValue.href)\" wb-title=Refrence wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.download ng-change=\"ctrl.setAttribute('download', ctrl.attributesValue.download)\" wb-title=Align wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.hreflang ng-change=\"ctrl.setAttribute('hreflang', ctrl.attributesValue.hreflang)\" wb-title=Language wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.media ng-change=\"ctrl.setAttribute('media', ctrl.attributesValue.media)\" wb-title=Media wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.ping ng-change=\"ctrl.setAttribute('ping', ctrl.attributesValue.ping)\" wb-title=Ping wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.referrerpolicy ng-change=\"ctrl.setAttribute('referrerpolicy', ctrl.attributesValue.referrerpolicy)\" wb-title=referrerpolicy wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.rel ng-change=\"ctrl.setAttribute('rel', ctrl.attributesValue.rel)\" wb-title=rel wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.target ng-change=\"ctrl.setAttribute('target', ctrl.attributesValue.target)\" wb-title=target wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.aType ng-change=\"ctrl.setAttribute('aType', ctrl.attributesValue.aType)\" wb-title=type wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-audio.html',
    "<fieldset layout=column> <legend translate>audio</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.autoplay ng-change=\"ctrl.setAttribute('autoplay', ctrl.attributesValue.autoplay)\" wb-title=autoplay wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.controls ng-change=\"ctrl.setAttribute('controls', ctrl.attributesValue.controls)\" wb-title=controls wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.loop ng-change=\"ctrl.setAttribute('loop', ctrl.attributesValue.loop)\" wb-title=loop wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.muted ng-change=\"ctrl.setAttribute('muted', ctrl.attributesValue.muted)\" wb-title=muted wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.preload ng-change=\"ctrl.setAttribute('preload', ctrl.attributesValue.preload)\" wb-title=preload wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.src ng-change=\"ctrl.setAttribute('src', ctrl.attributesValue.src)\" wb-title=src wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-form.html',
    "<fieldset layout=column> <legend translate>Form</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.acceptCharset ng-change=\"ctrl.setAttribute('acceptCharset', ctrl.attributesValue.acceptCharset)\" wb-title=acceptCharset wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.action ng-change=\"ctrl.setAttribute('action', ctrl.attributesValue.action)\" wb-title=action wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.autocomplete ng-change=\"ctrl.setAttribute('autocomplete', ctrl.attributesValue.autocomplete)\" wb-title=autocomplete wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.off ng-change=\"ctrl.setAttribute('off', ctrl.attributesValue.off)\" wb-title=off wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.enctype ng-change=\"ctrl.setAttribute('enctype', ctrl.attributesValue.enctype)\" wb-title=enctype wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.method ng-change=\"ctrl.setAttribute('method', ctrl.attributesValue.method)\" wb-title=method wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.name ng-change=\"ctrl.setAttribute('name', ctrl.attributesValue.name)\" wb-title=name wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.novalidate ng-change=\"ctrl.setAttribute('novalidate', ctrl.attributesValue.novalidate)\" wb-title=novalidate wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.target ng-change=\"ctrl.setAttribute('target', ctrl.attributesValue.target)\" wb-title=target wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-general.html',
    "<fieldset layout=column> <legend translate>Access</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.id ng-change=\"ctrl.setAttribute('id', ctrl.attributesValue.id)\" wb-title=ID wb-description=\"Specifies a unique id for an element\" wb-action-clean wb-action-more=ctrl.generateRandomId()> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.accesskey ng-change=\"ctrl.setAttribute('accesskey', ctrl.attributesValue.accesskey)\" wb-title=Accesskey wb-icon=\"\" wb-description=\"Specifies a shortcut key to activate/focus an element\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.title ng-change=\"ctrl.setAttribute('title', ctrl.attributesValue.title)\" wb-title=Title wb-description=\"Specifies extra information about an element\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.name ng-change=\"ctrl.setAttribute('name', ctrl.attributesValue.name)\" wb-title=Name wb-description=\"Specifies a unique id for an element\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-number ng-model=ctrl.attributesValue.tabindex ng-change=\"ctrl.setAttribute('tabindex', ctrl.attributesValue.tabindex)\" wb-title=\"Tab Index\" wb-description=\"Specifies the tabbing order of an element\" wb-action-clean wb-min=1> </wb-ui-setting-number> </fieldset> <fieldset layout=column> <legend translate>Edit</legend> <wb-ui-setting-boolean ng-model=ctrl.attributesValue.contenteditable ng-change=\"ctrl.setAttribute('contenteditable', ctrl.attributesValue.contenteditable)\" wb-title=\"Content Is Editable\" wb-description=\"Specifies whether the content of an element is editable or not\" wb-action-clean> </wb-ui-setting-boolean> <wb-ui-setting-boolean ng-model=ctrl.attributesValue.draggable ng-change=\"ctrl.setAttribute('draggable', ctrl.attributesValue.draggable)\" wb-title=\"Content Is Editable\" wb-description=\"Specifies whether an element is draggable or not\" wb-action-clean> </wb-ui-setting-boolean> <wb-ui-setting-select ng-model=ctrl.attributesValue.dropzone ng-change=\"ctrl.setAttribute('dropzone', ctrl.attributesValue.dropzone)\" wb-title=\"Drop Zone\" wb-description=\"Specifies whether the dragged data is copied, moved, or linked, when dropped\" wb-action-clean wb-items=\"[{\n" +
    "\t\t\tvalue: 'copy',\n" +
    "\t\t\ttitle: 'Copy',\n" +
    "\t\t\ticon: 'copy'\n" +
    "\t\t}, {\n" +
    "\t\t\tvalue: 'move',\n" +
    "\t\t\ttitle: 'Move',\n" +
    "\t\t\ticon: 'move'\n" +
    "\t\t}, {\n" +
    "\t\t\tvalue: 'link',\n" +
    "\t\t\ttitle: 'Link',\n" +
    "\t\t\ticon: 'link'\n" +
    "\t\t}]\"> </wb-ui-setting-select> </fieldset> <fieldset layout=column> <legend translate>Local</legend> <wb-ui-setting-select ng-model=ctrl.attributesValue.dir ng-change=\"ctrl.setAttribute('dropzone', ctrl.attributesValue.dropzone)\" wb-title=\"Text Direction\" wb-description=\"Specifies the text direction for the content in an element\" wb-action-clean wb-items=\"[{\n" +
    "\t\t\tvalue: 'ltr',\n" +
    "\t\t\ttitle: 'Default. Left-to-right text direction',\n" +
    "\t\t\ticon: 'ltr'\n" +
    "\t\t}, {\n" +
    "\t\t\tvalue: 'rtl',\n" +
    "\t\t\ttitle: 'Right-to-left text direction',\n" +
    "\t\t\ticon: 'rtl'\n" +
    "\t\t}, {\n" +
    "\t\t\tvalue: 'auto',\n" +
    "\t\t\ttitle: 'Let the browser figure out the text direction, based on the content',\n" +
    "\t\t}]\"> </wb-ui-setting-select> <wb-ui-setting-text ng-model=ctrl.attributesValue.lang ng-change=\"ctrl.setAttribute('lang', ctrl.attributesValue.lang)\" wb-title=Language wb-description=\"Specifies the language of the element's content\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-boolean ng-model=ctrl.attributesValue.spellcheck ng-change=\"ctrl.setAttribute('spellcheck', ctrl.attributesValue.spellcheck)\" wb-title=Spellcheck wb-description=\"Specifies whether the element is to have its spelling and grammar checked or not\" wb-action-clean> </wb-ui-setting-boolean> <wb-ui-setting-boolean ng-model=ctrl.attributesValue.translate ng-change=\"ctrl.setAttribute('translate', ctrl.attributesValue.translate)\" wb-title=Translate wb-description=\"Specifies whether the content of an element should be translated or not\" wb-action-clean> </wb-ui-setting-boolean> </fieldset> <fieldset layout=column> <legend translate>View</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.class ng-change=\"ctrl.setAttribute('class', ctrl.attributesValue.class)\" wb-title=Class wb-description=\"Specifies one or more classnames for an element (refers to a class in a style sheet)\" wb-action-clean> </wb-ui-setting-text> <wb-ui-setting-boolean ng-model=ctrl.attributesValue.hidden ng-change=\"ctrl.setAttribute('hidden', ctrl.attributesValue.hidden)\" wb-title=Hidden wb-description=\"Specifies that an element is not yet, or is no longer, relevant\" wb-action-clean> </wb-ui-setting-boolean> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-iframe.html',
    "<fieldset layout=column> <legend translate>Frame</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.name ng-change=\"ctrl.setAttribute('name', ctrl.attributesValue.name)\" wb-title=name wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.src ng-change=\"ctrl.setAttribute('src', ctrl.attributesValue.src)\" wb-title=src wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.srcdoc ng-change=\"ctrl.setAttribute('srcdoc', ctrl.attributesValue.srcdoc)\" wb-title=srcdoc wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.sandbox ng-change=\"ctrl.setAttribute('sandbox', ctrl.attributesValue.sandbox)\" wb-title=sandbox wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-img.html',
    "<fieldset layout=column> <legend translate>Image</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.alt ng-change=\"ctrl.setAttribute('alt', ctrl.attributesValue.alt)\" wb-title=Alternative wb-description=\"Defines an alternative text description of the image.\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.crossorigin ng-change=\"ctrl.setAttribute('crossorigin', ctrl.attributesValue.crossorigin)\" wb-title=\"Cross Oorigin\" wb-description=\"Indicates if the fetching of the image must be done using CORS.\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.longdesc ng-change=\"ctrl.setAttribute('longdesc', ctrl.attributesValue.longdesc)\" wb-title=\"Long Description\" wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Source</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.src ng-change=\"ctrl.setAttribute('src', ctrl.attributesValue.src)\" wb-title=Source wb-description=\"Path of the image\" wb-resource-type=image-url> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.srcset ng-change=\"ctrl.setAttribute('srcset', ctrl.attributesValue.srcset)\" wb-title=\"Source Set\" wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Map</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.ismap ng-change=\"ctrl.setAttribute('ismap', ctrl.attributesValue.ismap)\" wb-title=\"Is Map\" wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.usemap ng-change=\"ctrl.setAttribute('usemap', ctrl.attributesValue.usemap)\" wb-title=\"Use Map\" wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Size</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.size ng-change=\"ctrl.setAttribute('size', ctrl.attributesValue.size)\" wb-title=Size wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.height ng-change=\"ctrl.setAttribute('height', ctrl.attributesValue.height)\" wb-title=Height wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.width ng-change=\"ctrl.setAttribute('width', ctrl.attributesValue.width)\" wb-title=Width wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-input.html',
    "<fieldset layout=column> <legend translate>Data Validation</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.accept ng-change=\"ctrl.setAttribute('accept', ctrl.attributesValue.accept)\" wb-title=accept wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.alt ng-change=\"ctrl.setAttribute('alt', ctrl.attributesValue.alt)\" wb-title=alt wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.autocomplete ng-change=\"ctrl.setAttribute('autocomplete', ctrl.attributesValue.autocomplete)\" wb-title=autocomplete wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.autofocus ng-change=\"ctrl.setAttribute('autofocus', ctrl.attributesValue.autofocus)\" wb-title=autofocus wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.checked ng-change=\"ctrl.setAttribute('checked', ctrl.attributesValue.checked)\" wb-title=checked wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.dirname ng-change=\"ctrl.setAttribute('dirname', ctrl.attributesValue.dirname)\" wb-title=dirname wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.disabled ng-change=\"ctrl.setAttribute('disabled', ctrl.attributesValue.disabled)\" wb-title=disabled wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.list ng-change=\"ctrl.setAttribute('list', ctrl.attributesValue.list)\" wb-title=list wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.max ng-change=\"ctrl.setAttribute('max', ctrl.attributesValue.max)\" wb-title=max wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.maxlength ng-change=\"ctrl.setAttribute('maxlength', ctrl.attributesValue.maxlength)\" wb-title=maxlength wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.min ng-change=\"ctrl.setAttribute('min', ctrl.attributesValue.min)\" wb-title=min wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.multiple ng-change=\"ctrl.setAttribute('multiple', ctrl.attributesValue.multiple)\" wb-title=multiple wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.name ng-change=\"ctrl.setAttribute('name', ctrl.attributesValue.name)\" wb-title=name wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.pattern ng-change=\"ctrl.setAttribute('pattern', ctrl.attributesValue.pattern)\" wb-title=pattern wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.placeholder ng-change=\"ctrl.setAttribute('placeholder', ctrl.attributesValue.placeholder)\" wb-title=placeholder wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.readonly ng-change=\"ctrl.setAttribute('readonly', ctrl.attributesValue.readonly)\" wb-title=readonly wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.required ng-change=\"ctrl.setAttribute('required', ctrl.attributesValue.required)\" wb-title=required wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.src ng-change=\"ctrl.setAttribute('src', ctrl.attributesValue.src)\" wb-title=src wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.step ng-change=\"ctrl.setAttribute('step', ctrl.attributesValue.step)\" wb-title=step wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.inputType ng-change=\"ctrl.setAttribute('inputType', ctrl.attributesValue.inputType)\" wb-title=type wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.value ng-change=\"ctrl.setAttribute('value', ctrl.attributesValue.value)\" wb-title=value wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Form</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.form ng-change=\"ctrl.setAttribute('form', ctrl.attributesValue.form)\" wb-title=form wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.formaction ng-change=\"ctrl.setAttribute('formaction', ctrl.attributesValue.formaction)\" wb-title=formaction wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.formenctype ng-change=\"ctrl.setAttribute('formenctype', ctrl.attributesValue.formenctype)\" wb-title=formenctype wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.formmethod ng-change=\"ctrl.setAttribute('formmethod', ctrl.attributesValue.formmethod)\" wb-title=formmethod wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.formnovalidate ng-change=\"ctrl.setAttribute('formnovalidate', ctrl.attributesValue.formnovalidate)\" wb-title=formnovalidate wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.formtarget ng-change=\"ctrl.setAttribute('formtarget', ctrl.attributesValue.formtarget)\" wb-title=formtarget wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Size</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.size ng-change=\"ctrl.setAttribute('size', ctrl.attributesValue.size)\" wb-title=size wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.height ng-change=\"ctrl.setAttribute('height', ctrl.attributesValue.height)\" wb-title=height wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.width ng-change=\"ctrl.setAttribute('width', ctrl.attributesValue.width)\" wb-title=width wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-meta.html',
    "<fieldset layout=column style=\"padding: 0px\"> <legend translate=\"\">Meta</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.name ng-change=\"ctrl.setAttribute('name', ctrl.attributesValue.name)\" wb-title=Name wb-description=\"\"></wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.content ng-change=\"ctrl.setAttribute('content', ctrl.attributesValue.content)\" wb-title=Content wb-description=\"\"></wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.charset ng-change=\"ctrl.setAttribute('charset', ctrl.attributesValue.charset)\" wb-title=Charset wb-description=\"\"></wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.httpEquiv ng-change=\"ctrl.setAttribute('httpEquiv', ctrl.attributesValue.httpEquiv)\" wb-title=\"HTTP equiv\" wb-description=\"\"></wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-microdata.html',
    " <fieldset layout=column style=\"padding: 0px\"> <legend translate=\"\">Microdata</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.itemscope ng-change=\"ctrl.setAttribute('itemscope', ctrl.attributesValue.itemscope)\" wb-title=Scope wb-description=\"\"></wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.itemtype ng-change=\"ctrl.setAttribute('itemtype', ctrl.attributesValue.itemtype)\" wb-title=Type wb-description=\"\"></wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.itemprop ng-change=\"ctrl.setAttribute('itemprop', ctrl.attributesValue.itemprop)\" wb-title=Property wb-description=\"\"></wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.itemid ng-change=\"ctrl.setAttribute('itemid', ctrl.attributesValue.itemid)\" wb-title=ID wb-description=\"\"></wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.itemref ng-change=\"ctrl.setAttribute('itemref', ctrl.attributesValue.itemref)\" wb-title=Reference wb-description=\"\"></wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.value ng-change=\"ctrl.setAttribute('value', ctrl.attributesValue.value)\" wb-title=Value wb-description=\"\"></wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.content ng-change=\"ctrl.setAttribute('content', ctrl.attributesValue.content)\" wb-title=Content wb-description=\"\"></wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-picture.html',
    "<fieldset layout=column> <legend translate>Image</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.alt ng-change=\"ctrl.setAttribute('alt', ctrl.attributesValue.alt)\" wb-title=alt wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.crossorigin ng-change=\"ctrl.setAttribute('crossorigin', ctrl.attributesValue.crossorigin)\" wb-title=crossorigin wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.longdesc ng-change=\"ctrl.setAttribute('longdesc', ctrl.attributesValue.longdesc)\" wb-title=longdesc wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Source</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.src ng-change=\"ctrl.setAttribute('src', ctrl.attributesValue.src)\" wb-title=src wb-description=\"Path of the image\" wb-resource-type=image-url> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.hspace ng-change=\"ctrl.setAttribute('hspace', ctrl.attributesValue.hspace)\" wb-title=hspace wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Map</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.ismap ng-change=\"ctrl.setAttribute('ismap', ctrl.attributesValue.ismap)\" wb-title=ismap wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.usemap ng-change=\"ctrl.setAttribute('usemap', ctrl.attributesValue.usemap)\" wb-title=usemap wb-description=\"\"> </wb-ui-setting-text> </fieldset> <fieldset layout=column> <legend translate>Size</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.size ng-change=\"ctrl.setAttribute('size', ctrl.attributesValue.size)\" wb-title=size wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.height ng-change=\"ctrl.setAttribute('height', ctrl.attributesValue.height)\" wb-title=height wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.width ng-change=\"ctrl.setAttribute('width', ctrl.attributesValue.width)\" wb-title=width wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-source.html',
    "<fieldset layout=column> <legend translate>Source</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.src ng-change=\"ctrl.setAttribute('src', ctrl.attributesValue.src)\" wb-title=src wb-description=\"Path of the image\" wb-resource-type=image-url> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.srcset ng-change=\"ctrl.setAttribute('srcset', ctrl.attributesValue.srcset)\" wb-title=srcset wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.media ng-change=\"ctrl.setAttribute('media', ctrl.attributesValue.media)\" wb-title=media wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.sizes ng-change=\"ctrl.setAttribute('sizes', ctrl.attributesValue.sizes)\" wb-title=sizes wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.sourceType ng-change=\"ctrl.setAttribute('sourceType', ctrl.attributesValue.sourceType)\" wb-title=sourceType wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/settings/wb-widget-video.html',
    "<fieldset layout=column> <legend translate>audio</legend> <wb-ui-setting-text ng-model=ctrl.attributesValue.autoplay ng-change=\"ctrl.setAttribute('autoplay', ctrl.attributesValue.autoplay)\" wb-title=autoplay wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.controls ng-change=\"ctrl.setAttribute('controls', ctrl.attributesValue.controls)\" wb-title=controls wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.loop ng-change=\"ctrl.setAttribute('loop', ctrl.attributesValue.loop)\" wb-title=loop wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.muted ng-change=\"ctrl.setAttribute('muted', ctrl.attributesValue.muted)\" wb-title=muted wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.preload ng-change=\"ctrl.setAttribute('preload', ctrl.attributesValue.preload)\" wb-title=preload wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.src ng-change=\"ctrl.setAttribute('src', ctrl.attributesValue.src)\" wb-title=src wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.height ng-change=\"ctrl.setAttribute('height', ctrl.attributesValue.height)\" wb-title=height wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.poster ng-change=\"ctrl.setAttribute('poster', ctrl.attributesValue.poster)\" wb-title=poster wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.usemap ng-change=\"ctrl.setAttribute('usemap', ctrl.attributesValue.usemap)\" wb-title=usemap wb-description=\"\"> </wb-ui-setting-text> <wb-ui-setting-text ng-model=ctrl.attributesValue.width ng-change=\"ctrl.setAttribute('width', ctrl.attributesValue.width)\" wb-title=width wb-description=\"\"> </wb-ui-setting-text> </fieldset>"
  );


  $templateCache.put('views/sidenavs/amh-contents-list.html',
    "<div layout=column flex> <md-nav-bar md-no-ink-bar=disableInkBar md-selected-nav-item=currentNavItem nav-bar-aria-label=\"navigation links\"> <md-nav-item md-nav-click=\"goto('pages')\" name=pages> <span translate>Pages</span> </md-nav-item> <md-nav-item md-nav-click=\"goto('mypages')\" name=mypages> <span translate>My Pages</span> </md-nav-item> </md-nav-bar> <div ng-show=\"pageId === 'pages'\" ng-controller=\"AmhContentPagesCtrl as ctrl\" ng-init=\"ctrl.setDataQuery('{id, title, name, description, status, author{id, login}, metas{key, value}}')\" layout=column> <mb-pagination-bar mb-model=ctrl.queryParameter mb-properties=ctrl.properties mb-reload=ctrl.reload() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() mb-preloading=\"ctrl.state !== 'ideal'\" flex> <md-list flex> <md-list-item itemscope 8itemtype=http://schema.org/WebPage class=md-2-line ng-repeat=\"page in ctrl.items\" ng-href=\"{{ page.metasMap.link_canonical || 'content/' + page.name}}\"> <meta itemprop=url content=\"{{ page.metasMap.link_canonical || 'content/' + page.name}}\"> <img itemprop=image ng-src=\"{{page.metasMap.link_cover || 'resources/images/am-wb-common-broken-photo.svg'}}\" ng-src-error=resources/images/am-wb-common-broken-photo.svg class=md-avatar alt=\"{{page.title}}\"> <div class=md-list-item-text layout=column> <h3 itemprop=name>{{page.title}}</h3> <meta itemprop=description content={{page.description}}> <p itemscope itemtype=http://schema.org/Person itemprop=author> <span translate>Author</span> : <span itemprop=name>{{page.author.login}}</span> </p> </div> </md-list-item> </md-list> </md-content> </div> <div ng-show=\"pageId === 'mypages'\" ng-controller=\"AmhContentMyPagesCtrl as ctrl\" ng-init=\"ctrl.setDataQuery('{id, name, title, description, status, metas{key, value}}')\" layout=column> <mb-pagination-bar mb-model=ctrl.queryParameter mb-properties=ctrl.properties mb-reload=ctrl.reload() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() mb-preloading=\"ctrl.state !== 'ideal'\" flex> <md-list flex> <md-list-item itemscope itemtype=http://schema.org/WebPage class=md-2-line ng-repeat=\"page in ctrl.items\" ng-href=\"{{ page.metasMap.link_canonical || 'content/' + page.name}}\"> <meta itemprop=url content=\"{{ page.metasMap.link_canonical || 'content/' + page.name}}\"> <img itemprop=image ng-src=\"{{page.metasMap.link_cover || 'resources/images/am-wb-common-broken-photo.svg'}}\" ng-src-error=resources/images/am-wb-common-broken-photo.svg class=md-avatar alt=\"{{page.title}}\"> <div class=md-list-item-text layout=column> <h3 itemprop=name>{{page.title}}</h3> <meta itemprop=description content={{page.description}}> </div> </md-list-item> </md-list> </md-content> </div> </div>"
  );


  $templateCache.put('views/sidenavs/amh-contents-navigator.html',
    "<md-toolbar> </md-toolbar> <md-content ng-if=workbenchEditorLoaded layout-padding> <div ui-tree=treeOptions data-nodrop-enabled=false data-dropzone-enabled=false data-clone-enabled=false data-drag-enabled=false data-max-depth=0 data-drag-delay=0 data-empty-placeholder-enabled=true> <ol ui-tree-nodes=\"\" data-nodrop-enabled data-max-depth=0 ng-model=ctrl.widgets id=tree-root> <li ui-tree-node data-nodrag data-expand-on-hover=false ng-repeat=\"widget in ctrl.widgets\" ng-include=\"'views/templates/nodes_renderer.html'\"></li> </ol> </div> </md-content>"
  );


  $templateCache.put('views/sidenavs/amh-workbench-content.html',
    "<md-toolbar class=\"content-sidenavs-toolbar wb-layer-tool-bottom\" layout=row layout-align=\"space-around center\" ng-if=workbenchEditorLoaded> <span translate>Content settings</span> </md-toolbar> <md-content ng-if=workbenchEditorLoaded> <fieldset layout=column> <legend translate>Information</legend> <md-input-container class=md-block> <label translate>ID</label> <input ng-model=ctrl.content.id disabled> </md-input-container> <md-input-container class=md-block> <label translate>Downloads</label> <input ng-model=ctrl.content.downloads disabled> </md-input-container> <md-input-container class=md-block> <label translate>Creation date</label> <input ng-model=ctrl.content.creation_dtime disabled> </md-input-container> <md-input-container class=md-block> <label translate>Modification date</label> <input ng-model=ctrl.content.modif_dtime disabled> </md-input-container> </fieldset> <fieldset layout=column> <legend translate=\"\">General</legend> <md-input-container class=md-block> <label translate=\"\">Name</label> <input ng-model=ctrl.content.name ng-change=\"ctrl.contentDirty = true\"> </md-input-container> <md-input-container class=md-block> <label translate=\"\">File name</label> <input ng-model=ctrl.content.file_name ng-change=\"ctrl.contentDirty = true\"> </md-input-container> <md-input-container class=md-block> <label translate=\"\">Media type</label> <input ng-model=ctrl.content.media_type ng-change=\"ctrl.contentDirty = true\"> </md-input-container> <md-input-container class=md-block> <label translate=\"\">Mime type</label> <input ng-model=ctrl.content.mime_type ng-change=\"ctrl.contentDirty = true\"> </md-input-container> <md-input-container class=md-block> <label translate=\"\">Title</label> <input ng-model=ctrl.content.title ng-change=\"ctrl.contentDirty = true\"> </md-input-container> <md-input-container class=md-block> <label translate=\"\">Description</label> <textarea ng-model=ctrl.content.description ng-change=\"ctrl.contentDirty = true\"></textarea> </md-input-container> <div layout=column layout-align=\"center center\"> <md-button style=\"width: 90%\" class=\"md-raised md-primary\" ng-disabled=!ctrl.contentDirty ng-click=ctrl.saveChanges()> <span translate=\"\">Save changes</span> </md-button> </div> </fieldset> </md-content>"
  );


  $templateCache.put('views/sidenavs/amh-workbench-settings.html',
    "<md-tabs flex=\"\" ng-if=workbenchEditorLoaded> <md-tab id=content-setting-style> <md-tab-label> <wb-icon>color_lens</wb-icon> </md-tab-label> <md-tab-body> <md-content flex> <wb-setting-panel-group id=content-editor-setting ng-model=workbenchSelectedWidgets> </wb-setting-panel-group> </md-content> </md-tab-body> </md-tab> <md-tab id=content-setting-events> <md-tab-label> <wb-icon>flash_on</wb-icon> </md-tab-label> <md-tab-body> <md-content flex> <wb-event-panel ng-model=workbenchSelectedWidgets> </wb-event-panel> </md-content> </md-tab-body> </md-tab> <md-tab id=content-setting-widgets> <md-tab-label> <wb-icon>photo_album</wb-icon> </md-tab-label> <md-tab-body> <md-content flex> <amh-user-widgets id=content-editor-user-widgets> </amh-user-widgets> </md-content> </md-tab-body> </md-tab> </md-tabs>"
  );


  $templateCache.put('views/sidenavs/amh-workbench-template.html',
    "<div ng-if=workbenchEditorLoaded> <amh-template-contents id=amh-template-contents ng-model=template ng-change=ctrl.loadModelTemplate(template) flex> </amh-template-contents> </div>"
  );


  $templateCache.put('views/templates/nodes_renderer.html',
    "<md-menu mb-context-menu> <md-button ui-tree-handle=\"\" class=md-raised ng-class=\"{'md-accent': widget.isSelected()}\" style=\"margin:1px; width: 100%;  text-align: left\" ng-dblclick=\"dblclick(widget, this, $event)\" ng-click=\"click(widget, this, $event)\" layout=row> <wb-icon ng-show=\"widget.childWidgets && widget.childWidgets.length > 0\" class=\"md-icon-button launch\" ng-click=\"toggleItem(this, $event)\">{{ collapsed ? 'expand_more' : 'chevron_right'}}</wb-icon> <span flex>{{widget.getTitle() || widget.getId() || widget.getType()}}</span> </md-button> <md-menu-content width=4> <md-menu-item> <md-button ng-click=\"selectChildren(widget, $event)\"> <span translate>Select Children</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.clone()> <span translate>Clone</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.delete()> <span translate>Delete</span> </md-button> </md-menu-item> <md-divider></md-divider>  <md-menu-item> <md-button ng-click=widget.moveFirst()> <span translate>Move first</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.moveBefore()> <span translate>Move before</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.moveNext()> <span translate>Move next</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=widget.moveLast()> <span translate>Move last</span> </md-button> </md-menu-item> <md-divider></md-divider> <md-menu-item> <md-button ng-click=openHelp(widget)> <span translate>Widget help</span> </md-button> </md-menu-item> </md-menu-content> </md-menu> <ol ui-tree-nodes=\"\" ng-model=widget.childWidgets ng-show=collapsed> <li ng-repeat=\"widget in widget.childWidgets\" ui-tree-node ng-include=\"'views/templates/nodes_renderer.html'\"></li> </ol>"
  );


  $templateCache.put('views/toolbars/amh-owner-toolbar.html',
    "<md-toolbar class=\"wb-layer-tool-top amh-mini-toolbar\" md-theme=\"{{app.setting.theme || app.config.theme || 'default'}}\" md-theme-watch ng-show=_visible() layout=row>  <mb-mini-action-anchor mb-action-group=mb.toolbar.menu layout=row> </mb-mini-action-anchor>  <md-menu id=account md-offset=\"0 20\"> <div style=\"cursor: pointer\" layout=row layout-align=\"center center\" ng-click=$mdOpenMenu()> <img class=avatar ng-src=/api/v2/user/accounts/{{app.user.current.id}}/avatar ng-src-error=\"https://www.gravatar.com/avatar/{{app.user.current.id|wbmd5}}?d=identicon&size=22\"> <small style=\"margin-left: 3px; margin-right: 3px\"> {{app.user.profile.first_name}} {{app.user.profile.last_name}} </small> </div> <md-menu-content width=3> <md-menu-item ng-repeat=\"menu in userMenu.items | orderBy:['-priority']\"> <md-button ng-if=menu.visible() ng-click=menu.exec($event)> <wb-icon ng-if=menu.icon>{{::menu.icon}}</wb-icon> <span translate>{{::menu.title}}</span> </md-button> </md-menu-item> <md-menu-divider></md-menu-divider> <md-menu-item> <md-button ng-click=ctrl.showLocalSettings()> <span translate>Settings</span> </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=ctrl.logout();> <span translate>Logout</span> </md-button> </md-menu-item> </md-menu-content> </md-menu> <md-button aria-label=messages ng-show=ctrl.messageCount ng-click=ctrl.toggleMessageSidenav() style=\"overflow: visible\" class=\"md-icon-button md-mini\"> <md-tooltip md-delay=1500> <span translate>Display list of messages</span> </md-tooltip> <wb-icon size=16>notifications</wb-icon>{{ctrl.messageCount}} </md-button> <span flex></span> <mb-mini-action-anchor mb-action-group=amh.owner-toolbar.scope mb-divider=true layout=row> </mb-mini-action-anchor> <mb-mini-action-anchor mb-action-group=amh.owner-toolbar.public mb-divider=true layout=row> </mb-mini-action-anchor> </md-toolbar>"
  );

}]);
