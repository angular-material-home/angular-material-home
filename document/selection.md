# Selection
'Selection' is the rules of choosing border for different widgets in editing mode.
Here, we explain the main rules (in editing mode).
 
## Rules

1- All widgets in the page have a border of 'gray' color with width of '1px' 
2- When the mouse goes over a widget, the widget and it's parents get a border with the 'black' color of with '1px'
3- When a widget is selected, four small circles is added to the corner of the widget. 

## Next requirements

1- Making a mechanism for moving a widget up and down (if the parent layout is column) and left and right 
(if the parent layout is row) by using arrow keys or simulated ones.




