## start the module

crate a js file

past the following code


	(function() {
		alert('module is loaded');
	})();

Module is ready.

## Inject service and integerate to the application

To integerate

	(function() {
		/*
		 * Load the module
		 * @ngInject
		 */
		function loadModule(/* list of required service*/){
			// Load module
		}
		
		// load the module with angular injector
		angular
			.element(document)
			.injector()
			.invoke(loadModule);
	})();
	
## Load $basket service

	(function() {
		// 1- inject required service
		function loadModule($window, $dispatcher) {
			// 2- create service class
			function Bascket() {
				// 3- Load the service
			}
	
			// 4- Add service prototype
			Bascket.prototype.addItem = function() {};
			Bascket.prototype.removeItem = function() {};
			Bascket.prototype.getItem = function() {};
			Bascket.prototype.getItems = function() {};
			Bascket.prototype.getCount = function() {};
			Bascket.prototype.clear = function() {};
			
			// 5- Create an instance of the service
			var $basket = new Bascket();
			
			// 6- Load service as global JS object
			$window.$basket = $basket;
			
			// 7- Fire modules are loaded
			$dispatcher.dispatch('/app/modules', {
				type:'create',
				values: ['$basket'],
			});
		}
	
		/***************************************************************************
		 * Integeration code
		 * 
		 * To get access to the injector of a currently running
		 * AngularJS app from outside AngularJS, You can
		 * using the extra injector() added to JQuery/jqLite elements.
		 **************************************************************************/
		angular
			.element(document)
			.injector()
			.invoke(loadModule);
	})();