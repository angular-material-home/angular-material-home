# Toolbars


## Workbench

There are many toolbar in the module environment which are used for several gols.

- Workbench Shortcuts
- Workbench Tooolbar
- Workbench Editor Footer
- Workbench Editor Weburger Toolbar

### Shortcuts

This is always in access for a valid user

Is list of action to display as shortcuts.

The ID of the toolbar is:

    amh.workbench.shortcuts

### Tooolbar

This toolbar is accessable in edit mode.

The editor ID is:

    amh.workbench.toolbar

We use an advance toolbar:

    amh.workbench.toolbar#advance

### Editor Footer

This toolbar is accessable in edit mode.

The editor ID is:

    amh.workbench.footer

### Editor Weburger Toolbar

The editor ID is:

    amh.workbench.editor.weburger.toolbar
