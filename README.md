# Angular Material Home

![pipeline status](https://gitlab.com/angular-material-home/angular-material-home/badges/master/pipeline.svg)

![Codacy Badge](https://api.codacy.com/project/badge/Grade/a4badf8a47b4458eba1a315849216e9e)

In the other hand, a home page application is nothing expect an application to display contents and let customers choose site actions. So the AMH is a light application to manage and display contents to the customers.

This package adds an editor, viewer and a very fast light content management system UI. There are three main roles of CMS:

- Contributors
- Authors
- Editors


## Install

This is an bower package and you can install view the following command.

	bower install --save angular-material-home

It is recommanded to use a full path of the Git as follow:

	bower install --save https://gitlab.com/angular-material-home/angular-material-home.git

## Document

- [Wiki](https://gitlab.com/angular-material-home/angular-material-home/wikis/home)
- [API document](https://angular-material-home.gitlab.io/angular-material-home/doc/index.html)