# Content Modules

 A content module is a UI/Functional part of a document which is loaded by workbench. By default, these module dose not exist in the main WB core.
 
## How to load a module

 To load a module add following section to the root of your document:
 
	{
		...
		modules:[{
			title: 'module title',
			description: 'Module description',
			url: 'Path of the resource to load',
			logo: 'Path to the module logo',
			type: 'js|css|font'
		}]
	}

NOTE: There is no garanty to load module at an specific time.

NOTE: The usage of a module depend on the module itself.

## How to findout a module is loaded??

The JS module is very important and sensetive to load.

### Solution#1 load by $window

You can load module and then use it programatically. The $window manage loading.

	$window.loadLibrary('url/of/the/module')
	.then(function(){
		// Module is loaded :)
	}, function(){
		// fail to load the module?!
	});

### Solution#2 module fire the load

Some module fire the state of the module:

In module:

	$dispatcher.dispatch('/app/modules', {
		type: 'create',
		values: ['service', 'name']
	});

In widgets and document:

	$dispatcher.on('/app/modules', function(event){
		if(event.type === 'create' && _.includes(event.values, 'service)){
			// Service is loaded :)
		}
	});

This is not possible to handle failuares in the loading library.


## Custom module

### JS

To create a JS module create a js file.

Past the following code

	(function() {
		alert('module is loaded');
	})();

#### Integerate with application

Our base system is AngularJS.

To integerate the main application you must know about the system.

To load current application:

	
	(function() {
		function loadModule(){
			alert('module is loaded');
		}
		
		angular
			.element(document)
			.injector()
			.invoke(loadModule);
	})();

To use a service (for example $widnow)


	(function() {
		function loadModule($window){
			$window.alert('module is loaded');
		}
		
		angular
			.element(document)
			.injector()
			.invoke(loadModule);
	})();

